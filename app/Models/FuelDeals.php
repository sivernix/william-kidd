<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FuelDeals extends Model
{
    protected $table = 'fuel_deals';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'deal_id',
        'timestamp',
        'instrument_code',
        'price',
        'amount'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
    ];
}

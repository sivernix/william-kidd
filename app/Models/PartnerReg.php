<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Events\SettingUpdated;

class PartnerReg extends Model
{
    protected $table = 'partners_data';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'client_id',
        'company_name_short',
        'company_name_full',
        'ogrn',
        'kpp',
        'register_at',
        'ur_address',
        'main_fio',
        'position',
        'foundation',
        'partner_fio',
        'partner_phone',
        'partner_email',
        'okpo',
        'average_amount',
        'fact_address',
        'post_address',
        'signer1_fio',
        'signer1_position',
        'signer1_foundation',
        'signer2_fio',
        'signer2_position',
        'signer2_foundation',
        'bank_bik',
        'bank_account',
        'file_charter',
        'file_reg_cert',
        'file_decision_create',
        'file_order_head',
        'file_order_accountant'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
    ];
}

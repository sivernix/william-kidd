<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FuelAddress extends Model
{
    protected $table = 'fuel_address_index';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code',
        'address',
        'lat',
        'long',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
    ];
}

<?php

namespace App\Console\Commands;

use App\Http\Services\Calculator;
use Illuminate\Console\Command;


class SpimexLaunch extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'commands:spimexlaunch';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check spimex ftp';

    protected $service;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Calculator $calculator)
    {

        parent::__construct();

        $this->calculator = $calculator;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        try {
            $this->calculator->scheduleLaunch();
        } catch (\Exception $e) {
            Panic::scheduleLaunch($e->getMessage());
            $this->error($e->getMessage());

            return 1;
        }

        return 0;
    }
}

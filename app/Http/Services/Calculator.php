<?php

namespace App\Http\Services;

use App\Models\FuelAddress;
use App\Models\FuelDeals;
use App\Models\FuelIndex;
use Illuminate\Support\Facades\Log;

class Calculator
{
    private $host = '151.248.114.61';
    private $login = 'zzz_willkidd_test_ftp';
    private $password = 'V4j8J0h5';
    private $filename = 'deals.xml';

    public function selectFuelDeals() {
        $deals = FuelDeals::all();
        $dealsIndex = FuelIndex::all();

        $listIndex = [];

        foreach ($dealsIndex as $item) {
            $listIndex[$item->code] = $item->name;
        }

        $arCodeFuel = array_keys($listIndex);

        $list = [];
        foreach ($deals as $item) {
            $code = substr($item->instrument_code, 0, 4); // возвращает "abcd"
            $codeFuel = substr($item->instrument_code, -1);
            $sizeFuel = substr($item->instrument_code, -4, 3);
            $sizeFuel = intval($sizeFuel);
            $code = explode('-', $code);
            if($codeFuel == "F" && in_array($code[0], $arCodeFuel)) {
                $list[$code[0]] = [
                    'name' => $listIndex[$code[0]],
                    'code' => $code[0],
                    'size' => $sizeFuel
                ];
            }
        }

        return $list;
    }

    public function scheduleLaunch() {
        // Установка соединения
        $conn_id = ftp_connect($this->host);
        $login_result = ftp_login($conn_id, $this->login, $this->password);
        ftp_pasv($conn_id, true);

        // Прочитать файл в переменную
        $handle = fopen('php://temp', 'r+');

        if (ftp_fget($conn_id, $handle, $this->filename, FTP_BINARY, 0)) {
            $fstats = fstat($handle);
            fseek($handle, 0);
            $contents = fread($handle, $fstats['size']);

            $xml = simplexml_load_string($contents, "SimpleXMLElement", LIBXML_NOCDATA);
            $json = json_encode($xml);
            $array = json_decode($json,TRUE);

            Log::debug($array);

        } else {
            Log::debug('spimexlaunch: Произошла ошибка при чтении файла');
        }

        // Закрытие файла и соединения
        fclose($handle);
        ftp_close($conn_id);
    }
}

<?php

namespace App\Http\Services;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class Dadata
{

    public function getInfo($inn) {

        $token = "40cfcf8b9896de4168eabb0f6bc38dd486ea9abd";
        $secret = "073fb0c7340b7fb5be2cf24f3e1a40cb960de7af";

        $dadata = new \Dadata\DadataClient($token, $secret);

        $response = $dadata->findById("party", $inn);

        $arDate = [];

        foreach ($response as $company) {
            $arDate[] = [
                'name' => $company["value"],
                'inn' => $company["data"]["inn"],
                'kpp' => isset($company["data"]["kpp"]) ? $company["data"]["kpp"] : '',
                'full_name' => isset($company["data"]["name"]["full_with_opf"]) ? $company["data"]["name"]["full_with_opf"] : '',
                'ogrn' => isset($company["data"]["ogrn"]) ? $company["data"]["ogrn"] : '',
                'date' => isset($company["data"]["ogrn_date"]) ? $company["data"]["ogrn_date"] : '',
                'okpo' => isset($company["data"]["okpo"]) ? $company["data"]["okpo"] : '',
                'address' => $company["data"]["address"]["data"]["postal_code"] . ", " .$company["data"]["address"]['value'],
            ];
        }

        return $arDate[0];
    }
}

<?php

namespace App\Http\Services;

use App\Models\Events;
use App\Models\EventName;
use App\Models\FuelIndex;
use Illuminate\Support\Facades\Log;

class ServiceEvents
{

    private $pid;
    private $arEvent;

    public function __construct($pid)
    {
        $this->pid = $pid;
    }

    public function setEvent($code, $sid, $message='')
    {
        $obEvent = new Events;
        $obEvent->eid = $this->getEvent($code);
        $obEvent->sid = $sid;
        $obEvent->message = $this->constructMessage($message);
        $obEvent->save();
    }

    public function getEvents() {
        $rsEvent = EventName::where('pid', '=', $this->pid)->select('message')->get();
        $arEvents = $rsEvent->toarray();
        return $arEvents;
    }

    private function getEvent($code)
    {
        $rsEvent = EventName::where('code', '=', $code)->select('id', 'name', 'code')->get();
        $arEvent = $rsEvent->toarray();
        $this->arEvent = $arEvent[0];

        return $this->arEvent['id'];
    }

    private function constructMessage($message)
    {
        $code = $this->arEvent['code'];
        $expCode = explode('__', $code);
        $type = $expCode[0];
        $action = $expCode[0];

        if(empty($massage)) {
            if($type == 'contract') {
                $message = 'Поле "'. $this->arEvent['name'] .'" изменено';
            }
        } else {
            $message = $this->arEvent['name']. ': '.$message;
        }

        return $message;

    }
}

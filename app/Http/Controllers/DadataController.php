<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Services\Dadata;
use phpDocumentor\Reflection\Types\Integer;

class DadataController extends Controller
{
    public function getInfo($inn) {

        $errors = false;

        if(!is_numeric($inn) && strlen($inn) != 10) {
            $errors  = 'ИНН числовой код из 10 знаков';
        }

        if($errors === false) {

            $dadata = new Dadata();

            $response = $dadata->getInfo($inn);

            return ['status' => 'true', 'data' => json_encode($response)];
        } else {
            return ['status' => 'false', 'data' => $errors];
        }
    }
}

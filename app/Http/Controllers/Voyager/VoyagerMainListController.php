<?php

namespace App\Http\Controllers\Voyager;

//use TCG\Voyager\Http\Controllers as BaseController;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use League\Flysystem\Plugin\ListWith;
use TCG\Voyager\Events\MediaFileAdded;
use TCG\Voyager\Facades\Voyager;
use App\Models\PageFields;

class VoyagerMainListController extends VoyagerController
{
    public function index()
    {
        // Check permission
        //$this->authorize('browse', Voyager::model('MainList'));

        $data = PageFields::where('page_admin', 'main')->orderBy('order', 'ASC')->get();

        $page_fields = [];
        $page_fields[__('voyager::settings.group_general')] = [];
        foreach ($data as $d) {
            if ($d->group == '' || $d->group == __('voyager::settings.group_general')) {
                $page_fields[__('voyager::settings.group_general')][] = $d;
            } else {
                $page_fields[$d->group][] = $d;
            }
        }
        if (count($page_fields[__('voyager::settings.group_general')]) == 0) {
            unset($page_fields[__('voyager::settings.group_general')]);
        }

        $groups_data = PageFields::where('page_admin', 'main')->select('group')->distinct()->get();
        $groups = [];
        foreach ($groups_data as $group) {
            if ($group->group != '') {
                $groups[] = $group->group;
            }
        }

        $active = (request()->session()->has('setting_tab')) ? request()->session()->get('setting_tab') : old('setting_tab', key($page_fields));

        return Voyager::view('voyager::main-list.index', compact('page_fields', 'groups', 'active'));
    }

    public function store(Request $request)
    {
        // Check permission
        //$this->authorize('add', Voyager::model('PageFields'));

        $key = implode('.', [Str::slug($request->input('group')), $request->input('key')]);
        $key_check = PageFields::where('page_admin', 'main')->where('key', $key)->get()->count();

        if ($key_check > 0) {
            return back()->with([
                'message'    => __('voyager::settings.key_already_exists', ['key' => $key]),
                'alert-type' => 'error',
            ]);
        }

        $lastSetting = PageFields::where('page_admin', 'main')->orderBy('order', 'DESC')->first();

        if (is_null($lastSetting)) {
            $order = 0;
        } else {
            $order = intval($lastSetting->order) + 1;
        }

        $request->merge(['order' => $order]);
        $request->merge(['value' => '']);
        $request->merge(['key' => $key]);

        PageFields::where('page_admin', 'main')->create($request->except('setting_tab'));

        request()->flashOnly('setting_tab');

        return back()->with([
            'message'    => __('voyager::settings.successfully_created'),
            'alert-type' => 'success',
        ]);
    }

    public function update(Request $request)
    {
        //var_dump($request);
        // Check permission
        //$this->authorize('edit', Voyager::model('PageFields'));

        $settings = PageFields::where('page_admin', 'main')->get();

        foreach ($settings as $setting) {
            $content = $this->getContentBasedOnType($request, 'page_fields', (object) [
                'type'    => $setting->type,
                'field'   => str_replace('.', '_', $setting->key),
                'group'   => $setting->group,
            ], $setting->details);

            if ($setting->type == 'image' && $content == null) {
                continue;
            }

            if ($setting->type == 'file' && $content == null) {
                continue;
            }

            $key = preg_replace('/^'.Str::slug($setting->group).'./i', '', $setting->key);

            $setting->group = $request->input(str_replace('.', '_', $setting->key).'_group');
            $setting->key = implode('.', [Str::slug($setting->group), $key]);
            $setting->value = $content;
            $setting->save();
        }

        request()->flashOnly('setting_tab');

        return back()->with([
            'message'    => __('voyager::settings.successfully_saved'),
            'alert-type' => 'success',
        ]);
    }

    public function delete($id)
    {
        // Check permission
        //$this->authorize('delete', Voyager::model('PageFields'));

        $setting = PageFields::where('page_admin', 'main')->find($id);

        PageFields::where('page_admin', 'main')->destroy($id);

        request()->session()->flash('setting_tab', $setting->group);

        return back()->with([
            'message'    => __('voyager::settings.successfully_deleted'),
            'alert-type' => 'success',
        ]);
    }

    public function delete_value($id)
    {
        $setting = PageFields::where('page_admin', 'main')->find($id);

        // Check permission
        $this->authorize('delete', $setting);

        if (isset($setting->id)) {
            // If the type is an image... Then delete it
            if ($setting->type == 'image') {
                if (Storage::disk(config('voyager.storage.disk'))->exists($setting->value)) {
                    Storage::disk(config('voyager.storage.disk'))->delete($setting->value);
                }
            }
            $setting->value = '';
            $setting->save();
        }

        request()->session()->flash('setting_tab', $setting->group);

        return back()->with([
            'message'    => __('voyager::settings.successfully_removed', ['name' => $setting->display_name]),
            'alert-type' => 'success',
        ]);
    }
}

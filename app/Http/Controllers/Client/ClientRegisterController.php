<?php

namespace App\Http\Controllers\Client;

use App\Models\Client;
use App\Http\Controllers\Controller;
use App\Http\Services\Dadata;
use App\Models\VerificationClients;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Mail\VerificationClientEmail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;

class ClientRegisterController extends Controller
{
    public function index() {
        return view('cabinets.client.registration');
    }

    public function stepOne(Request $request) {


//        $validator = Validator::make($request->all(), [
//            'inn' => 'required|size:10',
//            'email' => 'required|email',
//        ]);
//
//        if ($validator->fails()) {
//
//            return redirect('/cabinet/client/registration')
//                ->withErrors($validator)
//                ->withInput();
//        }

        $inn = $request->get('inn');
        $email = $request->get('email');
        $error = '';

        $dadata = new Dadata();
        $response = $dadata->getInfo($inn);
        if(isset($response["inn"])) {
            if($response["inn"] == $inn) {
                $rsRecord = Client::where('inn', '=', $inn)->select('*')->get();
                $rsRecordEmail = Client::where('email', '=', $email)->select('*')->get();
                if ($rsRecord->count()) {
                    $error .= "Пользователь с таким ИНН уже существует \n";
                } else if($rsRecordEmail->count()) {
                    $error .= "Пользователь с таким E-mail уже существует \n";
                } else {
                    $obNewUser = new Client;
                    $obNewUser->email = $email;
                    $obNewUser->inn = $inn;
                    $obNewUser->name = '';
                    $obNewUser->password = 'empty';
                    $obNewUser->save();
                    $id = $obNewUser->id;

                    $date = date("Y-m-d H:i:s");
                    $code = md5($inn.$email.$date);

                    $this->setVerificationCode($id, $code);

                    $this->sendEmailVerification($email, $inn, $code);
                }
            }
        } else {
            $error .= "ИНН не найден \n";
        }

        if (!empty($error)) {
            return view('cabinets.client.registration', ['error' => $error, 'inn' => $inn, 'email' => $email]);
        } else {
            return view('cabinets.client.registration-next', ['inn' => $inn, 'email' => $email]);
        }
    }

    private function sendEmailVerification($email, $inn, $code) {
        $objSend = new \stdClass();

        $date = date("Y-m-d H:i:s");
        $objSend->inn = $inn;
        $objSend->email = $email;
        $objSend->code = 'http://affettapro.ru/cabinet/client/registration-password?code='.$code;

        $emails = [
            "ionov-dev@yandex.ru"
        ];

        //foreach ($emails as $email) {
            Mail::to($email)->send(new VerificationClientEmail($objSend));
        //}

        //return 'Cпасибо! Ваше сообщение отправлено, в ближайшее время с вами свяжутся.';
    }

    private function setVerificationCode($id, $code) {
        $obVClients = new VerificationClients;
        $obVClients->uid = $id;
        $obVClients->code = $code;
        $obVClients->used = 'false';
        $obVClients->save();
    }
}

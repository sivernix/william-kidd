<?php

namespace App\Http\Controllers\Client;

use App\Models\Client;
use App\Http\Controllers\Controller;
use App\Models\VerificationClients;
use App\Models\PartnerReg;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use App\Http\Services\Dadata;

class ClientVerificationController extends Controller
{
    public function index(Request $request) {
        $code = $request->get('code');
        $rsVC = VerificationClients::where('code', '=', $code)->where('used', '=', 'false')->select('*')->get();
        if($rsVC->count()) {
            return view('cabinets.client.registration-password', ['page' => 'ok', 'code' => $code, 'error' => '']);
        } else {
            return view('cabinets.client.registration-password', ['page' => 'error', 'code' => $code, 'error' => '']);
        }
    }

    public function setup(Request $request) {
        $password = $request->get('password');
        $passwordRepeat = $request->get('password-repeat');
        $code = $request->get('code');

        if ($password == $passwordRepeat) {
            //setup'password' =>

            $objVC = VerificationClients::where('code', '=', $code);
            $rsVC = $objVC->select('uid')->get();
            $arData = $rsVC->first();

            $uid = $arData['uid'];
            $obj = Client::where('id', '=', $uid)->first();
            $obj->password = Hash::make($password);
            $obj->email_verified_at = Carbon::now();
            $obj->save();

            //чоь торолпюсь(
            $objVC = VerificationClients::where('code', '=', $code)->first();
            $objVC->used = 'true';
            $objVC->save();

            $this->checkContract($uid);

            return redirect('/cabinet/client/login');
        } else {
            return view('cabinets.client.registration-password', ['page' => 'ok', 'code' => $code, 'error' => 'Пароли не совпадают']);
        }
    }

    private function checkContract($uid) {
        $rsRecord = PartnerReg::where('client_id', '=', $uid)->select('*')->get();
        if ($rsRecord->count() == 0) {
            $arValues = $this->getDataDadata($uid);
            $arValues['client_id'] = $uid;
            $obPartnerReg = new PartnerReg();
            $obPartnerReg->insert($arValues);
        }
    }

    private function getDataDadata($uid) {

        $obj = Client::where('id', '=', $uid);
        $rs = $obj -> select('inn') -> get();
        $arClient = $rs->first();
        $inn = $arClient['inn'];

        $dadata = new Dadata();
        $response = $dadata->getInfo($inn);

        return [
            'company_name_short' => $response['name'] ?? '' ,
            //'inn' => $response['inn'] ?? '',
            'kpp' => $response['kpp'] ?? '',
            'company_name_full' => $response['full_name'] ?? '',
            'ogrn' => $response['ogrn'] ?? '',
            'register_at' => Carbon::now() ?? '',
            'okpo' => $response['okpo'] ?? '',
            'ur_address' => $response['address'] ?? '',
            'main_fio' => '',
            'position' => '',
            'foundation' => '',
            'partner_fio' => '',
            'partner_phone' => '',
            'partner_email' => '',
            'average_amount' => '',
            'fact_address' => '',
            'post_address' => '',
            'signer1_fio' => '',
            'signer1_position' => '',
            'signer1_foundation' => '',
            'signer2_fio' => '',
            'signer2_position' => '',
            'signer2_foundation' => '',
            'bank_bik' => '',
            'bank_account' => '',
            'file_charter' => '',
            'file_reg_cert' => '',
            'file_decision_create' => '',
            'file_order_head' => '',
            'file_order_accountant' => '',
            'sid' => 1,
            'eid' => 0,
            'vid' => 0
        ];
    }
}

<?php

namespace App\Http\Controllers\Client;

use App\Models\Client;
use App\Http\Controllers\Controller;
use App\Models\PartnerReg;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class ClientProfileController extends Controller
{

    public function index(Request $request) {
        $user = auth()->user();

        $rsRecord = PartnerReg::where('client_id', '=', $user->id)->select('*')->get();

        $arData = $rsRecord->toarray()[0];

        //echo '<pre>'; var_dump($arData);

        return view('cabinets.client.profile', ['user' => $user, 'contract' => $arData]);
    }

    public function setup(Request $request) {

        $user = auth()->user();

        $arRequest = $request->all();

        $errors = [];

        $password = $arRequest['password'];
        $passwordRepeat = $arRequest['password-repeat'];
        $passwordNew = $arRequest['password-new'];
        $email = $arRequest['email'];
        //$inn = $request->get('inn');
        $userFlag = false;
        if (!empty($password) && !empty($passwordRepeat) && !empty($passwordNew)) {
            $oldPassword = Hash::make($password);
            if($oldPassword == $user->password) {
                if($passwordNew == $passwordRepeat) {
                    $user->password = Hash::make($passwordNew);
                    $userFlag = true;
                } else {
                    $errors['pswd-repeat'] = 'Пароли не совпадают';
                }
            } else {
                //ошибка пароля
                $errors['pswd'] = 'Неверный пароль';
            }
        }

        if(filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $user->email = $email;
            $userFlag = true;
        } else {
            //ошибка email
            $errors['email'] = 'Неверный email';
        }

        if(empty($errors)) {

            if($userFlag)
                $user->save();


            //echo '<pre>'; var_dump($arRequest);

            $objPR = PartnerReg::where('client_id', '=', $user->id)->first();
            $objPR->company_name_short = $arRequest["company_name_short"];
            $objPR->ogrn = $arRequest["ogrn"];
            $objPR->kpp = $arRequest["kpp"];
            $objPR->register_at = $arRequest["register_at"];
            $objPR->fact_address = $arRequest["fact_address"];
            $objPR->post_address = $arRequest["post_address"];
            $objPR->signer1_fio = $arRequest["signer1_fio"];
            $objPR->signer1_position = $arRequest["signer1_position"];
            $objPR->signer1_foundation = $arRequest["signer1_foundation"];
            $objPR->signer2_fio = $arRequest["signer2_fio"];
            $objPR->signer2_position = $arRequest["signer2_position"];
            $objPR->signer2_foundation = $arRequest["signer2_foundation"];
            $objPR->partner_fio = $arRequest["partner_fio"];
            $objPR->partner_phone = $arRequest["partner_phone"];
            $objPR->sid = 2;
            $objPR->save();
        }
        $rsRecord = PartnerReg::where('client_id', '=', $user->id)->select('*')->get();

        $arData = $rsRecord->toarray()[0];


        //echo '<pre>'; var_dump($arData);

        return view('cabinets.client.profile', ['user' => $user, 'contract' => $arData]);

    }

    public function getProfiles() {
        $user = auth()->user();

        $rsRecord = PartnerReg::where('sid', '!=', 1)->select('*')->get();
        $arData = $rsRecord->toarray();

        $rsClients = Client::where('email_verified_at', '!=','')->select('id','inn', 'email')->get();
        $arClients = $rsClients->toarray();
        $clientsList = [];
        $client = '';
        foreach ($arClients as $clients) {
            $clientsList[$clients['id']] = [
                'inn' => $clients['inn'],
                'email' => $clients['email']
            ];
        }


        $contractsList = [];
        $contract = [];
        foreach ($arData as $item) {
            $contractsList[] = [
                'company_name_short' => $item['company_name_short'],
                'register_at' => $item['register_at'],
                'id' => $item['id'],
                'sid' => $item['sid'],
                'active' => '',
                'inn' => $clientsList[$item['client_id']]['inn'],
                'email' => $clientsList[$item['client_id']]['email'],
                'cid' => $item['client_id']
            ];
        }
        $contractsList[0]['active'] = 'active';
        $contract['contract'] = $arData[0];
        $contract['user'] = $contractsList[0];

        //нужна обработка 404
        //echo '<pre>'; var_dump(['contract' => $contract, 'contractsList' => $contractsList, 'statusList' => $arStatusList]); die();
        return view('cabinets.operator.profile', ['contract' => $contract, 'contractsList' => $contractsList]);

    }

    public function getProfile($id) {
        $user = auth()->user();

        $rsRecord = PartnerReg::where('sid', '!=', 1)->select('*')->get();
        $arData = $rsRecord->toarray();

        $rsClients = Client::where('email_verified_at', '!=','')->select('id','inn', 'email')->get();
        $arClients = $rsClients->toarray();
        $clientsList = [];
        $client = '';
        foreach ($arClients as $clients) {
            $clientsList[$clients['id']] = [
                'inn' => $clients['inn'],
                'email' => $clients['email']
            ];
        }


        $contractsList = [];
        $contract = [];
        $contract1 = '';
        foreach ($arData as $item) {
            $contractsList[] = [
                'company_name_short' => $item['company_name_short'],
                'register_at' => $item['register_at'],
                'id' => $item['id'],
                'sid' => $item['sid'],
                'active' => $item['client_id'] == $id ? 'active' : '',
                'inn' => $clientsList[$item['client_id']]['inn'],
                'email' => $clientsList[$item['client_id']]['email'],
                'cid' => $item['client_id']
            ];

            if($item['client_id'] == $id) {
                $contract1 = $item;
            }
        }
        //$contractsList[0]['active'] = 'active';
        $contract['contract'] = $contract1;
        $contract['user'] = $contractsList[0];

        //нужна обработка 404
        //echo '<pre>'; var_dump(['contract' => $contract, 'contractsList' => $contractsList, 'statusList' => $arStatusList]); die();
        return view('cabinets.operator.profile', ['contract' => $contract, 'contractsList' => $contractsList]);
    }
}

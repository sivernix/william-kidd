<?php

namespace App\Http\Controllers\Client;

use App\Models\Client;
use App\Http\Controllers\Controller;
use App\Models\VerificationClients;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class ClientVerificationController extends Controller
{
    public function index(Request $request) {
        $code = $request->get('code');
        $rsVC = VerificationClients::where('code', '=', $code)->where('used', '=', 'true')->select('*')->get();
        if($rsVC->count()) {
            return view('cabinets.client.registration-password', ['page' => 'ok', 'code' => $code]);
        }

        return view('cabinets.client.registration-password', ['page' => 'error']);
    }
}

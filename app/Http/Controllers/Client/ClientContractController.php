<?php

namespace App\Http\Controllers\Client;

use App\Models\PartnerReg;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\PageFields;
use App\Models\FormAbout;
use App\Models\StatusesPartnerData;
use App\Mail\AboutEmail;
use App\Http\Services\ServiceEvent;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;

class ClientContractController extends Controller
{
    private $clientId;
    private $serviceEvents;

    public function index()
    {
        $user = auth()->user();

        $rsRecord = PartnerReg::where('sid', '!=', 1)->select('*')->get();

        $arData = $rsRecord->toarray();

        $contractsList = [];
        $contract = '';
        foreach ($arData as $item) {
            $contractsList[] = [
                'company_name_short' => $item['company_name_short'],
                'register_at' => $item['register_at'],
                'id' => $item['id'],
                'sid' => $item['sid'],
                'active' => ''
            ];
        }
        $contractsList[0]['active'] = 'active';
        $contract = $arData[0];

        $rsStatuses = StatusesPartnerData::where('id', '!=', 1)->select('*')->get();
        $arStatuses = $rsStatuses->toarray();

        $arStatusList = [];
        foreach ($arStatuses as $status) {
            $arStatusList[$status['id']] = $status;
        }

        return view('cabinets.operator.contract', ['contract' => $contract, 'contractsList' => $contractsList, 'statusList' => $arStatusList]);

    }

    public function completedByClient() {

        $user = auth()->user();

        $rsRecord = PartnerReg::where('client_id', '=', $user->id)->select('sid')->get();
        $arData = $rsRecord->first();
        if (!empty($arData)) {
            if($arData['sid']) {
                $obPartnerReg = new PartnerReg();
                $obPartnerReg->where('client_id', '=', $user->id)->update(['sid' => 2]);
            } else {
                return view('cabinets.client.partner-reg');
            }
        }

        return view('cabinets.client.partner-reg-complete');
    }

    public function checkStatusContract() {

        $user = auth()->user();

        $rsRecord = PartnerReg::where('client_id', '=', $user->id)->select('sid')->get();

        $arData = $rsRecord->first();
        if (!empty($arData)) {
            if($arData['sid'] == 2) {
                return view('cabinets.client.partner-reg-complete');
            } else {
                return view('cabinets.client.partner-reg');
            }
        }
    }

    public function getContracts($id) {
        $user = auth()->user();

        $rsRecord = PartnerReg::where('sid', '!=', 1)->select('*')->get();

        $arData = $rsRecord->toarray();

        $contractsList = [];

        foreach ($arData as $item) {
            $contractsList[] = [
                'company_name_short' => $item['company_name_short'],
                'register_at' => $item['register_at'],
                'id' => $item['id'],
                'sid' => $item['sid'],
                'active' => $item['id'] == $id ? 'active' : ''
            ];

            if($item['id'] == $id) {
                $contract = $item;
            }
        }

        $rsStatuses = StatusesPartnerData::where('id', '!=', 1)->select('*')->get();
        $arStatuses = $rsStatuses->toarray();

        $arStatusList = [];
        foreach ($arStatuses as $status) {
            $arStatusList[$status['id']] = $status;
        }

        return view('cabinets.operator.contract', ['contract' => $contract, 'contractsList' => $contractsList, 'statusList' => $arStatusList]);
    }

    public function setContractStatus(Request $request) {

        $user = auth()->user();
        $this->clientId = $user->id;

        $arRequest = $request->all();

        $this->serviceEvents = new ServiceEvent($arRequest['pid']);

        switch ($arRequest['status']) {
            case 'specified':
                $this->setSpecified();
                break;
            case 'wait':
                $this->setWait();
                break;
            case 'acting':
                $this->setActing();
                break;
            case 'expired':
                $this->setExpired();
                break;
            case 'refusal':
                $this->setRefusal();
                break;
            case 'canceled':
                $this->setCanceled();
                break;
            default :
                $this->setNew();
        }
    }

    private function setSpecified() {
        $sid = $this->setStatus('specified');
        $this->serviceEvents->setEvent('status__edit', $sid, $message='Уточняется');
    }

    private function setWait() {
        $sid = $this->setStatus('wait');
        $this->serviceEvents->setEvent('status__edit', $sid, $message='В обработке');
    }

    private function setActing() {
        $sid = $this->setStatus('acting');
        $this->serviceEvents->setEvent('status__edit', $sid, $message='Действующий');
    }

    private function setExpired() {
        $sid = $this->setStatus('expired');
        $this->serviceEvents->setEvent('status__edit', $sid, $message='Истек');
    }

    private function setRefusal() {
        $sid = $this->setStatus('refusal');
        $this->serviceEvents->setEvent('status__edit', $sid, $message='Отказ');
    }

    private function setCanceled() {
        $sid = $this->setStatus('canceled');
        $this->serviceEvents->setEvent('status__edit', $sid, $message='Отменен');
    }

    private function setNew() {
        $sid = $this->setStatus('new');
        $this->serviceEvents->setEvent('status__edit', $sid, $message='Новый');
    }

    private function setStatus($code) {

        $rsStatuses = StatusesPartnerData::where('id', '!=', 1000)->select('*')->get();
        $arStatuses = $rsStatuses->toarray();

        $arStatusList = [];
        foreach ($arStatuses as $status) {
            $arStatusList[$status['code']] = $status['id'];
        }

        PartnerReg::where('client_id', $this->clientId)->update(['sid' => $arStatusList[$code]]);
        return $arStatusList[$code];
    }
}

<?php

namespace App\Http\Controllers\Pub\Pages;

use App\Models\PageFields;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\AboutEmail;
use App\Models\FormAbout;
use App\Http\Services\Calculator;

class FuelController extends Controller
{
    public function index() {
        $setting = PageFields::where('page_admin', '=', 'fuel')->get();
        $settings = [];

        foreach ($setting as $item) {
            $settings[$item->key] = $item->value;
        }

        $obCalc = new Calculator();
        $fuelType = $obCalc->selectFuelDeals();

        $productList = FormAbout::all(); 

        return view('fuel', ['settings' => $settings, 'list' => $productList, 'fuelType' => $fuelType]);
    }

    public function sendProductInfo(Request $request) {

        $objSend = new \stdClass();

        $objSend->name = $request->get('name');
        $objSend->phone = $request->get('phone');
        $objSend->product = $request->get('product');

        $emails = [
            "open@affetta.ru",
            "office@tfkuk.ru"
        ];

        foreach ($emails as $email) {
            Mail::to($email)->send(new AboutEmail($objSend));
        }

        return 'Cпасибо! Ваше сообщение отправлено, в ближайшее время с вами свяжутся.';
    }
}

<?php

namespace App\Http\Controllers\Pub\Pages;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Http\Controllers\Controller;
use App\Mail\StonesEmail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use App\Models\PageFields;
use App\Models\FormAbout;

class InvestmentStonesController extends Controller
{
    public function index() {

        $setting = PageFields::where('page_admin', '=', 'stones')->get();
        $settings = [];

        foreach ($setting as $item) {
            $settings[$item->key] = $item->value;
        }

        $productList = FormAbout::all();

        return view('investment-stones', ['settings' => $settings, 'list' => $productList]);
    }

    public function calcSend(Request $request) {
        $objSend = new \stdClass();

        $objSend->name = $request->get('name');
        $objSend->phone = $request->get('phone');
        $objSend->email = $request->get('email');

        $objSend->form = $request->get("form"); //"изумруд",
        $objSend->color = $request->get("color"); //"D",
        $objSend->clear = $request->get("clear"); //"IF",
        $objSend->shape = $request->get("shape"); //"EXCELLENT",
        $objSend->flur = $request->get("flur"); //"NONE",
        $objSend->year = $request->get("year"); //"2013",
        $objSend->location = $request->get("location"); //"Якутия",
        $objSend->price_dia = $request->get("price_dia"); //"9 000 - 18 413 000",
        $objSend->carat = $request->get("carat"); //"0,08 - 4.30",
        $objSend->diameter = $request->get("diameter"); //:"2,85 - 11.20",
        $objSend->wight = $request->get("wight"); //"2,43 - 8,04",
        $objSend->price = $request->get("price"); //"59 500 - 4 282 100"

        $emails = [
            "open@affetta.ru",
            "office@tfkuk.ru"
        ];

        foreach ($emails as $email) {
            Mail::to("open@affetta.ru")->send(new StonesEmail($objSend));
        }

        return 'Cпасибо! Ваше сообщение отправлено, в ближайшее время с вами свяжутся.';
    }
}

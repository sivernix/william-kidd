<?php

namespace App\Http\Controllers\Pub\Pages;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\PageFields;
use App\Models\FormAbout;
use App\Mail\AboutEmail;
use Illuminate\Support\Facades\Mail;

class AboutController extends Controller
{
    public function index() {
        $setting = PageFields::where('page_admin', '=', 'about')->get();
        $settings = [];

        foreach ($setting as $item) {
            $settings[$item->key] = $item->value;
        }

        $productList = FormAbout::all();

        return view('about', ['settings' => $settings, 'list' => $productList]);
    }

    public function sendProductInfo(Request $request) {

        $objSend = new \stdClass();

        $objSend->name = $request->get('name');
        $objSend->phone = $request->get('phone');
        $objSend->product = $request->get('product');

        $emails = [
            "open@affetta.ru",
            "office@tfkuk.ru"
        ];

        if($request->get('check') == '') {
            foreach ($emails as $email) {
                Mail::to($email)->send(new AboutEmail($objSend));
            }
        }

        return 'Cпасибо! Ваше сообщение отправлено, в ближайшее время с вами свяжутся.';
    }
}

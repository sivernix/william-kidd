<?php

namespace App\Http\Controllers\Pub\Pages;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Http\Controllers\Controller;
use App\Models\News;
use App\Models\NewsGroups;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    public function index(Request $request) {

        $requestParam = $request->all();

        $keys = array_keys($requestParam);
        $desired_keys = array('group',  'q');

        foreach($desired_keys as $desired_key){
            if(in_array($desired_key, $keys)) continue;  // already set
            $requestParam[$desired_key] = '';
        }

        $group = '';
        $errorMsg = '';
        $group = $requestParam['group'] ? $requestParam['group'] : 'all';
        $q = $requestParam['q'];

        if($q) {
            $news = News::where ( 'title', 'LIKE', '%' . $q . '%' )->orWhere ( 'content', 'LIKE', '%' . $q . '%' )->paginate(8);
            if (count( $news ) == 0) {
                $errorMsg = "По вашему запросу нечего не найдено!";
            }
        } else {
            if(is_numeric($group)) {
                $news = News::where('group', '=', $group)->paginate(8);
            } else {
                $news = News::paginate(8);
            }
        }




        $newsTop = News::where('top', '=', 1)->get();

        $category = NewsGroups::all();

        foreach ($category as $item)
        {
            $arCategory[$item->id] = $item->name;
        }

        return view('news.index', [
            'news' => $news,
            'select_cat' => $group,
            'category' => $arCategory,
            'requestParam' => $requestParam,
            'newsTop' => $newsTop,
            'errorMsg' => $errorMsg
        ]);
    }

    public function detail($id) {

        $category = NewsGroups::all();

        foreach ($category as $item)
        {
            $arCategory[$item->id] = $item->name;
        }

        $news = News::where('id', '=', $id)->get();

        $groupName = '';
        foreach ($news as $item) {
            $groupName = $arCategory[$item->group];
        }

        $randomNews = News::inRandomOrder()->limit(3)->get();

        return view('news.detail', ["news"=>$news, 'category' => $groupName, 'randomNews'=>$randomNews]);
    }
}

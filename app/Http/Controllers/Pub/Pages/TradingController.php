<?php

namespace App\Http\Controllers\Public\Pages;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Http\Controllers\Controller;

class TradingController extends Controller
{
    public function index() {
        return view('trading', []);
    }
}

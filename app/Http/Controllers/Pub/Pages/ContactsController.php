<?php

namespace App\Http\Controllers\Pub\Pages;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Http\Controllers\Controller;
use App\Mail\ContactEmail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use App\Models\PageFields;

class ContactsController extends Controller
{
    public function index() {
        $setting = PageFields::where('page_admin', '=', 'contacts')->get();
        $settings = [];

        foreach ($setting as $item) {
            $settings[$item->key] = $item->value;
        }

        return view('contacts', ['settings' => $settings]);
    }

    public function sendFeedback(Request $request) {

        $objSend = new \stdClass();

        $objSend->name = $request->get('name');
        $objSend->phone = $request->get('phone');
        $objSend->message = $request->get('message');

        $emails = [
            "open@affetta.ru",
            "office@tfkuk.ru"
        ];

        if($request->get('check') == '') {
            foreach ($emails as $email) {
                Mail::to($email)->send(new ContactEmail($objSend));
            }
        }

        return 'Cпасибо! Ваше сообщение отправлено, в ближайшее время с вами свяжутся.';
    }

}

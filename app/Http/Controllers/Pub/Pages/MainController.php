<?php

namespace App\Http\Controllers\Pub\Pages;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Http\Controllers\Controller;
use App\Models\PageFields;

class MainController extends Controller
{
    public function index() {

        $setting = PageFields::where('page_admin', '=', 'main')->get();
        $settings = [];
        
        foreach ($setting as $item) {
            $settings[$item->key] = $item->value;
        }

        return view('main', ['settings' => $settings]);
    }
}

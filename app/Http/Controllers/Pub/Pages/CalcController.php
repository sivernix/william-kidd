<?php

namespace App\Http\Controllers\Pub\Pages;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\PageFields;
use App\Models\FuelIndex;
use App\Mail\CalculatorEmail;
use Illuminate\Support\Facades\Mail;
use App\Http\Services\Calculator;

class CalcController extends Controller
{
    public function index(Request $request) {

        $requestParam = $this->initParamRequestArray();

        $requestParam = $request->all() + $requestParam;

        $obCalc = new Calculator();
        $typeList = $obCalc->selectFuelDeals();

        return view('calculator.individual', ['requestParam' => $requestParam, 'typeList' => $typeList]);
    }

    public function indexEasy(Request $request) {

        $requestParam = $request->all();

        $obCalc = new Calculator();
        $typeList = $obCalc->selectFuelDeals();

        return view('calculator.easy', ['requestParam' => $requestParam, 'typeList' => $typeList]);
    }

    private function initParamRequestArray() {
        return [
            'lots' => '',
            'type' => '',
            'station' => '',
            'period' => '',
            'prepayment' => '',
            'dueDate' => '',
            'fact' => '',
        ];
    }

    public function sendCalcInfo(Request $request) {

        $objSend = new \stdClass();

        $objSend->name = $request->get('name');
        $objSend->phone = $request->get('phone');
        $objSend->email = $request->get('email');
        $objSend->period = $request->get('period');
        $objSend->inn = $request->get('inn');
        $objSend->type = $request->get('type');
        $objSend->number = $request->get('number');
        $objSend->size = $request->get('size');
        $objSend->station = $request->get('station');
        $objSend->prepayment = $request->get('prepayment');
        $objSend->dueDate = $request->get('dueDate');
        $objSend->gracePeriod = $request->get('gracePeriod');
        $objSend->fact = $request->get('fact');
        $objSend->comment = $request->get('comment');

        $emails = [
            "open@affetta.ru",
            "office@tfkuk.ru",
            "ionov-dev@yandex.ru"
        ];

        //if($request->get('check') == '') {
            foreach ($emails as $email) {
                Mail::to($email)->send(new CalculatorEmail($objSend));
           // }
        }

        return 'Cпасибо! Ваше сообщение отправлено, в ближайшее время с вами свяжутся.';
    }
}

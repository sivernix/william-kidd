<?php

namespace App\Http\Controllers\Pub\Pages;

use App\Models\PartnerReg;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\PageFields;
use App\Models\FormAbout;
use App\Mail\AboutEmail;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;

class PartnerRegController extends Controller
{
    private $fieldsGroups = [
        'partner-reg-confirm' => [
            'company_name_short',
            'company_name_full',
            'ogrn',
            'kpp',
            'register_at',
            'ur_address',
            'main_fio',
            'position',
            'foundation',
            'partner_fio',
            'partner_phone',
            'partner_email',
        ],
        'partner-reg-profile' => [
            'okpo',
            'average_amount',
            'fact_address',
            'post_address',
            'signer1_fio',
            'signer1_position',
            'signer1_foundation',
            'signer2_fio',
            'signer2_position',
            'signer2_foundation',
            'bank_bik',
            'bank_account',
        ],
        'partner-reg-doc' => [
            'file_charter',
            'file_reg_cert',
            'file_decision_create',
            'file_order_head',
            'file_order_accountant',
        ],
        'partner-reg-check' => [
            'company_name_short',
            'company_name_full',
            'ogrn',
            'kpp',
            'register_at',
            'ur_address',
            'main_fio',
            'position',
            'foundation',
            'partner_fio',
            'partner_phone',
            'partner_email',
            'okpo',
            'average_amount',
            'fact_address',
            'post_address',
            'signer1_fio',
            'signer1_position',
            'signer1_foundation',
            'signer2_fio',
            'signer2_position',
            'signer2_foundation',
            'bank_bik',
            'bank_account',
            'file_charter',
            'file_reg_cert',
            'file_decision_create',
            'file_order_head',
            'file_order_accountant',
        ]
    ];

    private $stepUrlChain = [
        'partner-reg-confirm' => '/cabinet/client/partner-reg-profile',
        'partner-reg-profile' => '/cabinet/client/partner-reg-doc',
        'partner-reg-doc' => '/cabinet/client/partner-reg-check',
        'partner-reg-check' => '/cabinet/client/partner-reg-complete',
    ];

    public function index()
    {

    }

    public function partnerReg(Request $request)
    {

        $sRedirectUrl = $this->stepUrlChain[$request->get('form')];

        $user = auth()->user();

        $arFieldList = $this->fieldsGroups[$request->get('form')];


        if (!empty($arFieldList) && $user->id) {
            $arValues = [];

            foreach ($arFieldList as $sFieldKey) {
                if ($request->get('form') == 'partner-reg-doc' && $request->file($sFieldKey)) {
                    $arValues[$sFieldKey] = $request->file($sFieldKey)->store('partner_docs/client' . $user->id);
                } elseif ($request->get('form') != 'partner-reg-doc') {
                    $arValues[$sFieldKey] = $request->get($sFieldKey);
                }
            }

            $arValues['sid'] = 1;
            $arValues['eid'] = 0;

            $this->createOrUpdateRecord($user->id, $arValues);
        }

        return redirect($sRedirectUrl);
    }

    public function createOrUpdateRecord($iClientId, $arValues) {
        $rsRecord = PartnerReg::where('client_id', '=', $iClientId)->select('*')->get();

        if ($rsRecord->count()) {
            $obPartnerReg = new PartnerReg();
            $obPartnerReg->where('client_id', '=', $iClientId)->update($arValues);
        } else {
            $arValues['client_id'] = $iClientId;
            $obPartnerReg = new PartnerReg();
            $obPartnerReg->insert($arValues);
        }
    }

    public function getFormData() {
        $user = auth()->user();

        $arFieldList = $this->fieldsGroups[Route::current()->getName()];


        $rsRecord = PartnerReg::where('client_id', '=', $user->id)->select($arFieldList)->get();

        $arData = $rsRecord->first();
        if (!empty($arData)) {
            if ($arData['register_at']) {
                $arData['register_at'] = date('Y-m-d', strtotime($arData['register_at']));
            }
        } else {
            foreach ($arFieldList as $key => $val) {
                $arData[$val] = '';
            }
        }

        return view('cabinets.client.'.Route::current()->getName(), ['form' => $arData]);
    }
}

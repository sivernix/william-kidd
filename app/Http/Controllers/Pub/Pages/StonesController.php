<?php

namespace App\Http\Controllers\Pub\Pages;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Http\Controllers\Controller;
use App\Mail\StonesEmail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;

class StonesController extends Controller
{
    public function index() {
        return view('contacts', []);
    }

    public function sendFeedback(Request $request) {

        $objSend = new \stdClass();

        $objSend->name = $request->get('name');
        $objSend->phone = $request->get('phone');
        $objSend->message = $request->get('message');

        $emails = [
            "open@affetta.ru",
            "office@tfkuk.ru"
        ];

        foreach ($emails as $email) {
            Mail::to($email)->send(new StonesEmail($objSend));
        }

        return 'Cпасибо! Ваше сообщение отправлено, в ближайшее время с вами свяжутся.';
    }

}

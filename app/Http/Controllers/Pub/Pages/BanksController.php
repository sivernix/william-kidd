<?php

namespace App\Http\Controllers\Pub\Pages;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Http\Controllers\Controller;
use App\Models\Banks;

class BanksController extends Controller
{
    public function index() {

        $banks = Banks::all();

        return view('banks', [
            'banks' => $banks
        ]);
    }
}

<?php

namespace App\Http\Controllers\Pub\Pages;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Http\Controllers\Controller;
use App\Models\FormAbout;

class FinancingController extends Controller
{
    public function index() {
        $productList = FormAbout::all();
        return view('financing', ['list' => $productList]);
    }
}

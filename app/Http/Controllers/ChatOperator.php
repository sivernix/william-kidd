<?php

namespace App\Http\Controllers;

use App\Models\Chat;
use App\Models\Client;
use App\Models\PartnerReg;
use App\Models\StatusesPartnerData;
use App\Models\User;
use Carbon\Carbon;
use Facade\Ignition\DumpRecorder\Dump;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class ChatOperator extends Controller
{
    public function index(Request $request)
    {

        $arRouterParams = $request->route()->parameters;

        $iChatId = 0;

        $user = auth()->user();
        $CHAT_SOCKETIO_URL = env('CHAT_SOCKETIO_URL');

        $userIdCode = '';

        if ($user->id) {
            $userIdCode = \App\Helpers\IntToShortCode::encode($user->id) . '_operator';
        }

        //выбираем всех клиентов, т.к. клиент это комната в чатах
        $arClientsData = Client::select('*')->get()->keyBy('id')->toarray();
        foreach ($arClientsData as $arClient) {
            $clientIdCode = \App\Helpers\IntToShortCode::encode($arClient['id']) . '_client';


            /*$arRecords = Chat::select('*')->whereIn('client1', [$clientIdCode, $userIdCode])
                ->orWhereIn('client2', [$clientIdCode, $userIdCode])->get()->toarray();*/


            $arRecords = DB::select('select * from chat where (client1 = ? AND client2 = ?) OR (client1 = ? AND client2 = ?)', array($clientIdCode, $userIdCode, $userIdCode,$clientIdCode));


            //если оператор и клиент ещё не взаимодействуют в чате, то добавляем чат

            if (empty($arRecords)) {
                DB::table('chat')->insert([
                    'client1' => $clientIdCode,
                    'client2' => $userIdCode,
                    'message' => '',
                    'readed' => '0',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]);
            }

        }

        //выбираем все чаты
        $rsRecords = Chat::select('*')->get();
        $arData = $rsRecords->toarray();


        $arChats = [];
        $arClientsID = [];

        foreach ($arData as $arMess) {
            if ($arMess['client1'] == $userIdCode) {
                $arChats[$arMess['client2']] = [
                    'chat_id' => $arMess['client2'],
                    'client_user_id' => \App\Helpers\IntToShortCode::decode(str_replace('_client', '',
                        $arMess['client2'])),
                ];
            }

            if ($arMess['client2'] == $userIdCode) {
                $arChats[$arMess['client1']] = [
                    'chat_id' => $arMess['client1'],
                    'client_user_id' => \App\Helpers\IntToShortCode::decode(str_replace('_client', '',
                        $arMess['client1'])),
                ];
            }
        }

        $arClientsID = array_column($arChats, 'client_user_id');

        $arPartners = PartnerReg::select('*')->whereIn('client_id', $arClientsID)->get()->keyBy('id')->toarray();
        $arClientsData = Client::select('*')->whereIn('id', $arClientsID)->get()->keyBy('id')->toarray();

        foreach ($arClientsData as $arClient) {
            $arChats[\App\Helpers\IntToShortCode::encode($arClient['id']) . '_client'] = [
                'partnerId' => '',
                'chatName' => '',
                'inn' => '',
                'message' => '',
            ];
        }

        foreach ($arPartners as $arPartner) {
            $arChats[\App\Helpers\IntToShortCode::encode($arPartner['client_id']) . '_client']['id'] = $arPartner['id'];
            $arChats[\App\Helpers\IntToShortCode::encode($arPartner['client_id']) . '_client']['partnerId'] = $arPartner['client_id'];
            $arChats[\App\Helpers\IntToShortCode::encode($arPartner['client_id']) . '_client']['clientCode'] = \App\Helpers\IntToShortCode::encode($arPartner['client_id']) . '_client';
            $arChats[\App\Helpers\IntToShortCode::encode($arPartner['client_id']) . '_client']['chatName'] = $arPartner['company_name_full'];
            $arChats[\App\Helpers\IntToShortCode::encode($arPartner['client_id']) . '_client']['inn'] = $arClientsData[$arPartner['client_id']]['inn'];
            $arChats[\App\Helpers\IntToShortCode::encode($arPartner['client_id']) . '_client']['message'] = $this->getLastMessage($arPartner['client_id']);
        }

        //если чат выбран, то добираем договор
        $arContract = [];
        $arStatusList = [];
        if (!empty($arRouterParams)) {
            $iChatId = \App\Helpers\IntToShortCode::encode($arRouterParams['id']) . '_client';
        } else {
            $firstChat = reset($arChats);
            $iChatId = \App\Helpers\IntToShortCode::encode($firstChat['partnerId']) . '_client';
        }

        $arContract = $arPartners[$arChats[$iChatId]['id']];

        $arStatusList = StatusesPartnerData::where('id', '!=', 1)
            ->select('*')
            ->get()
            ->keyBy('id')
            ->toarray();


        return view('cabinets.operator.chat', [
            'statusList' => $arStatusList,
            'contract' => $arContract,
            'curChat' => $iChatId,
            'user' => $user,
            'userIdCode' => $userIdCode,
            'chats' => $arChats,
            'CHAT_SOCKETIO_URL' => $CHAT_SOCKETIO_URL
        ]);
    }


    public function getLastMessage($iClientID)
    {
        $sCode = \App\Helpers\IntToShortCode::encode($iClientID) . '_client';
        //выбираем все чаты, в которых участвуем
        $arRecords = Chat::select('*')
            ->where('client1', '=', $sCode)
            ->orWhere('client2', '=', $sCode)
            ->orderBy('created_at', 'desc')
            ->limit(1)->get()->toarray();

        return $arRecords[0]['message'];
    }
}

<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AboutEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $about;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($about)
    {
        $this->about = $about;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $mail  = 'noreply@williamkidd.ru';
        return $this->from($mail, 'ТФК Уильям Кидд')->subject("Запрос продукта на сайте http://williamkidd.ru/")
            ->view('mails.about')->text('mails.about_plain');
    }
}

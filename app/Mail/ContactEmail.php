<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $contact;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($contact)
    {
        $this->contact = $contact;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $mail  = 'noreply@williamkidd.ru';
        return $this->from($mail, 'ТФК Уильям Кидд')->subject("Новая заявка на сайте williamkidd.ru")
            ->view('mails.contact')->text('mails.contacts_plain');
    }
}

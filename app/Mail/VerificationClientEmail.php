<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class VerificationClientEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $fields;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($fields)
    {
        $this->fields = $fields;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $mail  = 'noreply@williamkidd.ru';
        return $this->from($mail, 'ТФК Уильям Кидд')->subject("Подтверждение регистрации williamkidd.ru")
            ->view('mails.verifications_client')->text('mails.verifications_client_plain');
    }
}

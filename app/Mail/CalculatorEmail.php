<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CalculatorEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $calc;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($calc)
    {
        $this->calc = $calc;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $mail  = 'noreply@williamkidd.ru';
        return $this->from($mail, 'ТФК Уильям Кидд')->subject("Запрос на индивидуальный расчет топлива http://williamkidd.ru/")
            ->view('mails.calc')->text('mails.calc_plain');
    }
}

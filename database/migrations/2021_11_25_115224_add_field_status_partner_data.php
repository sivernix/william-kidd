<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldStatusPartnerData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('partners_data', function (Blueprint $table) {
            $table->integer('sid');
            $table->integer('eid'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('partners_data', function (Blueprint $table) {
            $table->dropColumn('sid');
            $table->dropColumn('eid'); 
        });
    }
}

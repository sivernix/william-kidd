<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class AddSeedEventName extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $arIndex = $this->getArrayIndex();
        foreach ($arIndex as $item) {
            DB::table('event_name')->insert([
                'code' => $item[0],
                'name' => $item[1],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }

    private function getArrayIndex()
    {
        return [
            [
                0 => 'contract__org_name',
                1 => 'Наименование организации'
            ],
            [
                0 => 'contract__ogrn',
                1 => 'ОГРН'
            ],
            [
                0 => 'contract__kpp',
                1 => 'КПП'
            ],
            [
                0 => 'contract__reg_data',
                1 => 'Дата регистрации'
            ],
            [
                0 => 'contract__legal_address',
                1 => 'Юридический адрес'
            ],
            [
                0 => 'contract__gen_dir',
                1 => 'Генеральный директор'
            ],
            [
                0 => 'contract__p1_fio',
                1 => 'ПОДПИСАНТ 1 - ФИО'
            ],
            [
                0 => 'contract__p1_position',
                1 => 'ПОДПИСАНТ 1 - Должность'
            ],
            [
                0 => 'contract__p2_fio',
                1 => 'ПОДПИСАНТ 2 - ФИО'
            ],
            [
                0 => 'contract__p2_position',
                1 => 'ПОДПИСАНТ 2 - Должность'
            ],
            [
                0 => 'contract__okpo',
                1 => 'ОКПО'
            ],
            [
                0 => 'contract__actual_address',
                1 => 'Фактический адрес'
            ],
            [
                0 => 'contract__post_address',
                1 => 'Почтовый адрес'
            ],
            [
                0 => 'contract__headcount',
                1 => 'Среднесписочная численность'
            ],
            [
                0 => 'user__edit',
                1 => 'Редактирование профиля'
            ],
            [
                0 => 'user__add',
                1 => 'Новый пользователь'
            ],
            [
                0 => 'user__add_contract',
                1 => 'Новый договор'
            ],
            [
                0 => 'user__change_password',
                1 => 'Изменен пароль'
            ],
            [
                0 => 'status__edit',
                1 => 'Изменен статус'
            ],
        ];
    }
}

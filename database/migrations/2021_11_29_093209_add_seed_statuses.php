<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSeedStatuses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $arIndex = $this->getArrayIndex();
        foreach ($arIndex as $item) {
            DB::table('statuses_partner_data')->insert([
                'code' => $item[0],
                'name' => $item[1]
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }

    private function getArrayIndex()
    {
        return [
            [
                0 => 'new',
                1 => 'Новый'
            ],
            [
                0 => 'in_process',
                1 => 'В обработке'
            ],
            [
                0 => 'valid',
                1 => 'Действующий'
            ],
            [
                0 => 'specified',
                1 => 'Уточняется'
            ],
            [
                0 => 'gone',
                1 => 'Истек'
            ],
            [
                0 => 'refusing',
                1 => 'Отказ'
            ],
            [
                0 => 'canceled',
                1 => 'Отменен'
            ]
        ];
    }
}

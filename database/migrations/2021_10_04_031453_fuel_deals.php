<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FuelDeals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fuel_deals', function ($table) {
            $table->string('deal_id')->primary();
            $table->string('timestamp');
            $table->string('instrument_code');
            $table->string('price');
            $table->string('amount');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fuel_deals', function (Blueprint $table) {
            Schema::dropIfExists('fuel_deals');
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePartnersDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partners_data', function (Blueprint $table) {
            $table->id();
            $table->integer('client_id');
            $table->string('company_name_short')->nullable();
            $table->string('company_name_full')->nullable();
            $table->string('ogrn')->nullable();
            $table->string('kpp')->nullable();
            $table->timestamp('register_at')->nullable();
            $table->string('ur_address')->nullable();
            $table->string('main_fio')->nullable();
            $table->string('position')->nullable();
            $table->string('foundation')->nullable();
            $table->string('partner_fio')->nullable();
            $table->string('partner_phone')->nullable();
            $table->string('partner_email')->nullable();
            $table->string('okpo')->nullable();
            $table->string('average_amount')->nullable();
            $table->string('fact_address')->nullable();
            $table->string('post_address')->nullable();
            $table->string('signer1_fio')->nullable();
            $table->string('signer1_position')->nullable();
            $table->string('signer1_foundation')->nullable();
            $table->string('signer2_fio')->nullable();
            $table->string('signer2_position')->nullable();
            $table->string('signer2_foundation')->nullable();
            $table->string('bank_bik')->nullable();
            $table->string('bank_account')->nullable();
            $table->string('file_charter')->nullable();
            $table->string('file_reg_cert')->nullable();
            $table->string('file_decision_create')->nullable();
            $table->string('file_order_head')->nullable();
            $table->string('file_order_accountant')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partners_data');
    }
}

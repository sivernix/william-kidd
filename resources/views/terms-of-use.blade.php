@include('header')
<div class="container content">
    <div class="row">
        <div class="col-xl-8 mx-auto">
            {!! setting('site.terms_of_use') !!}
        </div>
    </div>
</div>
@include('footer')

@include('cabinets/operator/header')

@php

//echo "<pre>";var_dump($chats);echo "</pre>";die;
@endphp


<div class="container-fluid">
    <div class="row align-items-center">
        <div class="col-3">
            <div class="header__page">
                <h1>Чаты</h1>
            </div>
        </div>
        <div class="col-9" style="display: none;">
            <div class="header__page">
                <input class="search" type="text" placeholder="Поиск по наименованию покупателя или тексту события"><a class="filter visible js-filter" href="#"><img src="/cabinets/operator/assets/images/filter.svg" /> Фильтр</a>
                <div class="filter__wrapper">
                    <h3>Фильтр</h3>
                    <form class="filter__wrapper-form">
                        <fieldset>
                            <label>Показать за период</label>
                            <input type="date">
                        </fieldset>
                        <fieldset>
                            <label>Раздел</label>
                            <select>
                                <option>Показать все</option>
                            </select>
                        </fieldset>
                        <fieldset>
                            <label>ПМ</label>
                            <select>
                                <option>Показать все</option>
                            </select>
                        </fieldset><a class="btn btn-primary" href="#">Применить</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-3 pr-0">
            <div class="sidebar sidebar-chat js-scroller">
                @foreach($chats as $chat)
                    @if($chat['clientCode'] == $curChat)
                    <a class="sidebar__item active" href="/cabinet/operator/chat/{{$chat['partnerId']}}">
                        <p>{{$chat['chatName']}} <i style="display: none;">11:32</i></p>
                        <span>ИНН: {{$chat['inn']}}</span>
                        <div class="msg">
                            <p>{{\Illuminate\Support\Str::limit($chat['message'], 10, $end='...')}}</p><i style="display:none;">3</i>
                        </div>
                    </a>
                    @else
                        <a class="sidebar__item" href="/cabinet/operator/chat/{{$chat['partnerId']}}">
                            <p>{{$chat['chatName']}} <i style="display: none;">11:32</i></p>
                            <span>ИНН: {{$chat['inn']}}</span>
                            <div class="msg">
                                <p>{{\Illuminate\Support\Str::limit($chat['message'], 10, $end='...')}}</p><i style="display:none;">3</i>
                            </div>
                        </a>
                    @endif

                @endforeach
            </div>
        </div>

        <div class="col-6 pl-0" style="">
            <div class="body__page js-scroller pt-0">
                <div class="chat__wrapper">
                    <span class="date"></span>
                    <div class="js-message-list">

                    </div>

                </div>
            </div>
            <form class="chat__form" id="lk__chat-form">
                <label style="display: none;">
                    <input class="attach" type="file">
                    <p></p>
                </label>
                <input type="hidden" id="ucode" value="{{$userIdCode}}">
                <input type="hidden" id="curChat" value="{{$curChat}}">
                <input type="hidden" id="uname" value="{{$curChat}}">
                <textarea id="lk__chat-input" placeholder="Напишите ваш комментарий..."></textarea>
                <a class="send js-lk__chat-send" href="#"></a>
            </form>
        </div>

        @if(!empty($curChat))
        <div class="col-3">
            <div class="chatbar js-scroller">
                <div class="chatbar__title">
                    <h3>Договор №{{$contract['id']}} от {{\Carbon\Carbon::parse($contract['register_at'])->format('d.m.Y')}} <a href="/cabinet/operator/contract/{{$contract['id']}}"></a></h3>


                        <div class="status {{$statusList[$contract['sid']]['code']}}">{{$statusList[$contract['sid']]['name']}}</div>

                </div>
                <div class="chatbar__order" style="display: none;">
                    <h4>Активные заказы</h4>
                    <div class="chatbar__order-item"><strong>№ 102 от 15.02.2020</strong><span>бензин (АИ-100-К5) СТО 78689379-32-2018 120 тн, 2 000 000 ₽</span>
                        <p>Гуково, Северо-Кавказская ЖД</p>
                        <div>
                            <div class="status expired">отгружен</div><span>с 27.02.2019 по 26.02.2020</span>
                        </div>
                    </div>
                    <div class="chatbar__order-item"><strong>№ 102 от 15.02.2020</strong><span>бензин (АИ-100-К5) СТО 78689379-32-2018 120 тн, 2 000 000 ₽</span>
                        <p>Гуково, Северо-Кавказская ЖД</p>
                        <div>
                            <div class="status wait">в обработке</div><span>с 27.02.2019 по 26.02.2020</span>
                        </div>
                    </div>
                </div><a class="btn btn-secondary w-100" style="display:none;" href="#">Все заказы</a>
            </div>
        </div>
        @endif
    </div>
</div>


<script src="{{$CHAT_SOCKETIO_URL}}/socket.io/socket.io.js"></script>

<script>
    var socket;
    var form;
    var input;
    var user = document.getElementById('ucode').value;
    var curChat = document.getElementById('curChat').value;
    $(() => {
        socket = new io.connect('{{$CHAT_SOCKETIO_URL}}', { query: 'ucode=' + user });

        socket.on('ping', function(data){
            socket.emit('pong', {beat: 1});
        });

        socket.on('chat message', (data) => {
            console.log('message: ' + data.message);
            console.log('socketId: ' + data.socketId);
            console.log(socket.id == data.socketId);

            let isMyMess = socket.id == data.socketId;

            let date = new Date;

            let seconds = date.getSeconds();
            let minutes = date.getMinutes();
            let hour = date.getHours();

            let html_mess = '<div class="chat__bubble ' + (isMyMess ? 'mine' : '') + '"> <p>' + data.message + '</p><span>' + hour + ':' + minutes + '</span></div>';

            $('.js-message-list').append(html_mess);
        });


        form = document.getElementById('lk__chat-form');
        input = document.getElementById('lk__chat-input');


        $(form).on('submit', (e) => {
            e.preventDefault();
            if (input.value) {
                socket.emit('chat message', {message: input.value, socketId: socket.id, ucode: user, curChat: curChat});
                input.value = '';
            }
        });

        $('.js-lk__chat-send').on('click', (e) => {
            e.preventDefault();
            $(form).trigger('submit');
        });
    })
</script>



@include('cabinets/operator/footer')

@include('cabinets/operator/header')
<div class="container-fluid">
    <div class="row align-items-center">
        <div class="col-3">
            <div class="header__page">
                <h1>Профили</h1><a class="excel" href="#" data-toggle="modal" data-target="export">Экспорт в Excel</a>
            </div>
        </div>
        <div class="col-9">
            <div class="header__page">
                <input class="search" type="text" placeholder="Поиск по наименованию покупателя или тексту события"><a class="filter visible js-filter" href="#"><img src="/cabinets/operator/assets/images/filter.svg" /> Фильтр</a>
                <div class="filter__wrapper">
                    <h3>Фильтр</h3>
                    <form class="filter__wrapper-form">
                        <fieldset>
                            <label>Показать за период</label>
                            <input type="date">
                        </fieldset>
                        <fieldset>
                            <label>Раздел</label>
                            <select>
                                <option>Показать все</option>
                            </select>
                        </fieldset>
                        <fieldset>
                            <label>ПМ</label>
                            <select>
                                <option>Показать все</option>
                            </select>
                        </fieldset><a class="btn btn-primary" href="#">Применить</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-3 pr-0">
            <div class="sidebar js-scroller">
                @foreach($contractsList as $item)
                <a class="sidebar__item {{$item['active']}}" href="/cabinet/operator/profile/{{$item['cid']}}">
                    <p>{{$item['company_name_short']}}</p><span>ИНН: {{$item['inn']}}</span>
                </a>
                @endforeach
            </div>
        </div>
        <div class="col-9 pl-0">
            <div class="body__page js-scroller">
                <div class="body__page-title align-items-start">
                    <div>
                        <h3>{{$contract['contract']['company_name_short']}}</h3><span>Персональный менеджер Фамилия Имя Отчество <br/>Дата регистрации 12.01.2020</span><a href="#">Неподтвержденные изменения</a>
                    </div>
                    <ul>
                        <li><a href="/cabinet/operator/chat">Чат</a></li>
                        <li><a href="/cabinet/operator/order">Заказы</a></li>
                        <li><a href="/cabinet/operator/contract">Договоры</a></li>
                    </ul>
                </div>
                <div class="body__page-container">
                    <div class="body__page-form">
                        <form>
                            <fieldset>
                                <h3>Данные</h3>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="text-field">
                                            <div><span>E-mail</span>
                                                <p>{{$contract['user']['email']}}</p>
                                            </div><a href="#">Сбросить пароль</a>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <label><span>Контактный номер телефона</span>
                                            <input type="text" value="{{$contract['contract']['partner_phone']}}">
                                        </label>
                                    </div>
                                    <div class="col-6">
                                        <label><span>Имя контактного лица</span>
                                            <input type="text" value="{{$contract['contract']['partner_fio']}}">
                                        </label>
                                    </div>
                                    <div class="col-6">
                                        <div class="row">
                                            <div class="col-6">
                                                <label><span>Наименование организации</span>
                                                    <input type="text" value="{{$contract['contract']['company_name_full']}}">
                                                </label>
                                            </div>
                                            <div class="col-6">
                                                <label><span>ОГРН</span>
                                                    <input type="text" value="{{$contract['contract']['ogrn']}}">
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="row">
                                            <div class="col-6">
                                                <label><span>КПП</span>
                                                    <input type="text" value="{{$contract['contract']['kpp']}}">
                                                </label>
                                            </div>
                                            <div class="col-6">
                                                <label><span>Дата регистрации</span>
                                                    <input type="text" value="{{$contract['contract']['register_at']}}">
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <label><span>Юридический адрес</span>
                                            <input type="text" value="{{$contract['contract']['ur_address']}}">
                                        </label>
                                    </div>
                                    <div class="col-6">
                                        <label><span>Генеральный директор</span>
                                            <input type="text" value="{{$contract['contract']['main_fio']}}">
                                        </label>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <h3>Профиль</h3>
                                <div class="row">
                                    <div class="col-6">
                                        <div class="row">
                                            <div class="col-6">
                                                <label><span>ОКПО</span>
                                                    <input type="text" value="{{$contract['contract']['okpo']}}">
                                                </label>
                                            </div>
                                            <div class="col-6">
                                                <label><span>Среднесписочная численность</span>
                                                    <input type="text" value="{{$contract['contract']['average_amount']}}">
                                                </label>
                                            </div>
                                            <div class="col-12">
                                                <label><span>Почтовый адрес</span>
                                                    <input type="text" value="{{$contract['contract']['post_address']}}">
                                                    <label class="checkbox">
                                                        <input type="checkbox">
                                                        <p>Совпадает с юридическим</p>
                                                    </label>
                                                </label>
                                            </div>
                                            <div class="col-6">
                                                <label><span>ФИО подписанта 1</span>
                                                    <input type="text">
                                                </label>
                                            </div>
                                            <div class="col-6">
                                                <label><span>Должность</span>
                                                    <input type="text">
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <label><span>Фактический адрес</span>
                                            <input type="text"  value="{{$contract['contract']['fact_address']}}">
                                            <label class="checkbox">
                                                <input type="checkbox">
                                                <p>Совпадает с юридическим</p>
                                            </label>
                                        </label>
                                        <div class="row">
                                            <div class="col-6">
                                                <label><span>БИК Банка</span>
                                                    <input type="text">
                                                </label>
                                            </div>
                                            <div class="col-6">
                                                <label><span>Номер расчетного счета</span>
                                                    <input type="text">
                                                </label>
                                            </div>
                                            <div class="col-6">
                                                <label><span>ФИО подписанта 2</span>
                                                    <input type="text">
                                                </label>
                                            </div>
                                            <div class="col-6">
                                                <label><span>Должность</span>
                                                    <input type="text">
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <h3>Подтверждающий документ</h3>
                                <div class="row">
                                    <div class="col-6">
                                        <label>
                                            <input type="file">
                                        </label>
                                    </div>
                                </div>
                                <div class="row align-items-end">
                                    <div class="col-6">
                                        <div class="file">
                                            <p>Устав организации</p><a href="#"></a>
                                        </div>
                                        <div class="file">
                                            <p>Устав организации</p><a href="#"></a>
                                        </div>
                                    </div>
                                    <div class="col-6 text-right"><a class="btn btn-primary" href="#">Сохранить изменения</a><a class="btn btn-secondary" href="#">Отменить</a></div>
                                </div>
                            </fieldset>
                        </form>
                        <div class="body__page-spoiler">
                            <div class="title">
                                <h3>Дополнительно</h3>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <ul class="tab-link">
                                        <li class="tab active"><a href="#doc">Документы</a></li>
                                        <li class="tab"><a href="#his">История изменений</a></li>
                                    </ul>
                                </div>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="doc">
                                        <div class="col-6">
                                            <div class="file">
                                                <p><a href="/storage/app/{{$contract['contract']['file_reg_cert']}}">Свидетельство о регистрации</a></p><a href="#"></a>
                                            </div>
                                            <div class="file">
                                                <p><a href="#">Решение о создании / Протокол собрания</a></p><a href="#"></a>
                                            </div>
                                            <div class="file">
                                                <p><a href="/storage/app/{{$contract['contract']['file_order_head']}}">Приказ о назначении руководителя</a></p><a href="#"></a>
                                            </div>
                                            <div class="file">
                                                <p><a href="#">Приказ о назначении главного бухгалтера</a></p><a href="#"></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="his">
                                        <div class="col-12">
                                            <div class="change__item"><strong>Юридический адрес</strong>
                                                <p>22.01.2019 09:55, Фамилия Имя Отчество</p>
                                                <div class="change__item-line">
                                                    <p>111141, г. Москва, ул. Плеханова, д 13 стр 5</p>
                                                    <p>111141, г. Москва, ул. Плеханова, д 13 стр 5</p>
                                                </div><a href="#">Договор аренды.pdf</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('cabinets/operator/footer')

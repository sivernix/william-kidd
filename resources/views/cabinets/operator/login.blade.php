<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="HandheldFriendly" content="true">
    <meta http-equiv="cleartype" content="on">
    <meta http-equiv="msthemecompatible" content="no">
    <meta name="format-detection" content="telephone=no">
    <link rel="shortcut icon" type="image/x-icon" href="cabinets/operator/favicon.ico">
    <title>William Kidd - Поставщик углеводородов</title>
    <link href="{{ asset('cabinets/operator/vendors/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('cabinets/operator/vendors/malihu-scrollbar/jquery.mCustomScrollbar.css') }}" rel="stylesheet">
    <link href="{{ asset('cabinets/operator/assets/css/app.css') }}" rel="stylesheet">
    <script type="text/javascript" src="{{ asset('cabinets/operator/vendors/jquery/dist/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('cabinets/operator/vendors/jquery-ui/jquery-ui.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('cabinets/operator/vendors/malihu-scrollbar/jquery.mCustomScrollbar.concat.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('cabinets/operator/assets/js/app.js') }}"></script>
</head>
<body>
<div class="page__wrapper">
    <div class="page__wrapper-cover" style="background-image: url('{{ asset('cabinets/operator/assets/images/banner.jpg') }}')">
        <div class="logo"><img src="{{ asset('cabinets/operator/assets/images/logo.svg') }}"></div>
    </div>
    <div class="page__wrapper-container">
        <div class="container-fluid h-100">
            <div class="page__wrapper-flex">
                <div class="page__wrapper-phone text-right"><a href="tel:+74951207337">+7 (495) 120-73-37</a><br><span>Горячая линия</span></div>
                <div class="page__wrapper-form">
                    <div class="col-7 mx-auto">
                        <form class="page__wrapper-form__login form" method="POST" action="{{ route('login') }}">
                            @csrf
                            <div class="form__title">
                                <h1>Вход в Личный кабинет</h1>
                                <p>У вас еще нет аккаунта? <a href="#">Зарегистрируйтесь</a></p>
                            </div>
                            <fieldset>
                                <div class="form__label">
                                    <label>E-mail оператора</label>
                                </div>
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </fieldset>
                            <fieldset>
                                <div class="form__label">
                                    <label>Пароль</label><a href="#">Забыли пароль?</a>
                                </div>
                                <div class="form__label-pwd js-pwd hide"></div>
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </fieldset>
                            <fieldset>
                                <div class="form__label-check">
                                    <label>
                                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                        <p>Запомнить пароль </p>
                                    </label>
                                </div>
                            </fieldset><button class="btn btn-primary js__btn-submit">Отправить</button>
                        </form>
                    </div>
                </div>
                <div class="page__wrapper-copyright text-right">
                    <p>© William Kidd LLC, 2008-2021</p>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>

@include('cabinets/operator/header')

<div class="container-fluid">
    <div class="row align-items-center">
        <div class="col-3">
            <div class="header__page">
                <h1>Договоры</h1><a class="excel" href="#" data-toggle="modal" data-target="contract">Экспорт в Excel</a>
            </div>
        </div>
        <div class="col-9">
            <div class="header__page">
                <input class="search" type="text" placeholder="Поиск по наименованию покупателя или тексту события"><a class="filter visible js-filter" href="#"><img src="/cabinets/operator/assets/images/filter.svg" /> Фильтр</a>
                <div class="filter__wrapper">
                    <h3>Фильтр</h3>
                    <form class="filter__wrapper-form">
                        <fieldset>
                            <label>Показать за период</label>
                            <input type="date">
                        </fieldset>
                        <fieldset>
                            <label>Раздел</label>
                            <select>
                                <option>Показать все</option>
                            </select>
                        </fieldset>
                        <fieldset>
                            <label>ПМ</label>
                            <select>
                                <option>Показать все</option>
                            </select>
                        </fieldset><a class="btn btn-primary" href="#">Применить</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-3 pr-0">
            <div class="sidebar js-scroller">
                @foreach($contractsList as $contracts)
                    <a class="sidebar__item {{$contracts['active']}}" href="/cabinet/operator/contract/{{$contracts['id']}}">
                        <p>{{$contracts['company_name_short']}}</p><span>№ {{$contracts['id']}} от {{$contracts['register_at']}}</span><span class="status {{$statusList[$contracts['sid']]['code']}}">{{$statusList[$contracts['sid']]['name']}}</span>
                    </a>
                @endforeach
            </div>
        </div>
        <div class="col-9 pl-0">
            <div class="body__page js-scroller">
                <div class="body__page-title align-items-start">
                    <div>
                        <h3>{{$contract['company_name_short']}}</h3><span>ИНН: 77123456789012</span>
                    </div>
                    <ul>
                        <li><a href="#">Чат</a></li>
                        <li><a href="/cabinet/operator/order">Заказы</a></li>
                        <li><a href="/cabinet/operator/documents">Документы</a></li>
                    </ul>
                </div>
                <div class="body__page-container">
                    <div class="body__page-form">
                        <div class="body__page-header"><strong>Договор № {{$contract['id']}} от {{$contract['register_at']}}</strong>
                            <div>
                                <div class="status {{$statusList[$contract['sid']]['code']}}">{{$statusList[$contract['sid']]['name']}}</div><span>с {{$contract['register_at']}} по 26.02.2022</span>
                            </div><a href="#">Оригинал договора.pgf</a>
                        </div>
                        <div class="fieldset">
                            <h3>Реквизиты</h3>
                            <div class="row">
                                <div class="col-5">
                                    <div class="block"><span>Наименование организации</span>
                                        <p>{{$contract['company_name_full']}}</p>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="block"><span>Расчетный счет № 1234567890123456789</span>
                                        <p>123456789, ПАО Банк, 30101810400000000336</p>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="block"><span>ОГРН</span>
                                        <p>{{$contract['ogrn']}}</p>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="block"><span>ОКПО</span>
                                        <p>{{$contract['okpo']}}</p>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="block"><span>КПП</span>
                                        <p>{{$contract['kpp']}}</p>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="block"><span>Фактический адрес</span>
                                        <p>{{$contract['fact_address']}}</p>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="block"><span>Дата регистрации</span>
                                        <p>13.01.2020</p>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="block"><span>Почтовый адрес</span>
                                        <p>{{$contract['post_address']}}</p>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="block"><span>Юридический адрес:</span>
                                        <p>{{$contract['ur_address']}}</p>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="block"><span>Среднесписочная численность:</span>
                                        <p>{{$contract['average_amount']}}</p>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="block"><span>{{$contract['position']}}</span>
                                        <p>{{$contract['partner_fio']}}</p>
                                    </div>
                                </div>
                                <div class="col-5"></div>
                                <div class="col-5">
                                    <div class="block"><strong>Подписант 1</strong><span>ФИО</span>
                                        <p>{{$contract['signer1_fio']}}</p>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="block"><strong>Подписант 2</strong><span>ФИО</span>
                                        <p>{{$contract['signer2_fio']}}</p>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="block"><span>Должность</span>
                                        <p>{{$contract['signer1_position']}}</p>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="block"><span>Должность</span>
                                        <p>{{$contract['signer2_position']}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="fieldset">
                            <h3 class="mb-0">Прикрепленные документы</h3>
                            <div class="row">
                                <div class="col-6">
                                    <div class="file">
                                        <p><a href="#">Свидетельство о регистрации</a></p>
                                    </div>
                                    <div class="file">
                                        <p><a href="#">Решение о создании / Протокол собрания</a></p>
                                    </div>
                                    <div class="file">
                                        <p><a href="#">Приказ о назначении руководителя</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="pid" value="{{$contract['id']}}">
                        @csrf
                        <!-- Действующий  -->
                        @if($statusList[$contract['sid']]['code'] == 'acting')
                        <div class="fieldset">
                            <div class="row">
                                <div class="col-12 ml"><button class="btn btn-primary edit-agree" type="button" value="specified">Изменить данные</button><a class="btn btn-secondary" href="#" data-toggle="modal" data-target="closed">Аннулировать договор</a></div>
                            </div>
                        </div>
                        @endif
                        <!-- В обработке  -->
                        @if($statusList[$contract['sid']]['code'] == 'wait')
                        <div class="body__page-form">
                            <form class="form">
                                <fieldset>
                                    <h3>Подписанный договор</h3>
                                    <div class="row">
                                        <div class="col-6">
                                            <label>
                                                <input type="file" name="agree">
                                            </label>
                                        </div>
                                    </div>
{{--                                    <div class="row align-items-end">--}}
{{--                                        <div class="col-6">--}}
{{--                                            <div class="file mt-0">--}}
{{--                                                <p>Устав организации</p><a href="#"></a>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
                                </fieldset>
                                <fieldset class="mt-5">
                                    <h3>Комментарий</h3>
                                    <div class="row">
                                        <div class="col-6">
                                            <label><span>Дополнительная информация</span>
                                                <textarea name=""></textarea>
                                            </label>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                        <div class="fieldset">
                            <div class="row">
                                <div class="col-12 ml"><button class="btn btn-primary edit-agree" type="button" value="acting">Подтвердить</button><button class="btn btn-secondary edit-agree" type="button" value="specified">Уточнить</button><a class="btn btn-secondary" href="#">Отменить</a></div>
                            </div>
                        </div>
                        @endif
                        <!-- уточняется specified-->
                        @if($statusList[$contract['sid']]['code'] == 'specified')

                        <div class="fieldset">
                            <div class="row">
                                <div class="col-12 ml"><button class="btn btn-primary edit-agree" type="button" value="wait">Взять в обработку</button><button class="btn btn-secondary edit-agree" type="button" value="canceled">Отменить</button></div>
                            </div>
                        </div>
                        @endif
                        @if($statusList[$contract['sid']]['code'] == 'refusal')
                        <!-- отказ -->
                        <div class="fieldset">
                            <h3>Комментарий оператора</h3>
                            <div class="row">
                                <div class="col-6">
                                    <div class="block">
                                        <p>Решение о создании частично не читается (загнулся уголок). Пожалуйста,&nbsp;загрузите&nbsp;полностью&nbsp;читаемый документ.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="fieldset">
                            <div class="row">
                                <div class="col-12 ml"><button class="btn btn-primary edit-agree" type="button" value="wait">возобновить</button></div>
                            </div>
                        </div>
                        @endif
                        <!-- отменен -->
                        @if($statusList[$contract['sid']]['code'] == 'canceled')
                        <div class="fieldset">
                            <h3>Комментарий оператора</h3>
                            <div class="row">
                                <div class="col-6">
                                    <div class="block">
                                        <p>Решение о создании частично не читается (загнулся уголок). Пожалуйста,&nbsp;загрузите&nbsp;полностью&nbsp;читаемый документ.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="fieldset">
                            <div class="row">
                                <div class="col-12 ml"><button class="btn btn-primary edit-agree" type="button" value="wait">возобновить</button></div>
                            </div>
                        </div>
                        @endif
                        <div class="body__page-spoiler">
                            <div class="title">
                                <h3>История событий</h3>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="history__item"><span>22.01.2019 09:55</span>
                                        <p>Фамилия Имя Отчество</p>
                                        <p>Договор отправлен на обработку</p>
                                    </div>
                                    <div class="history__item"><span>22.01.2019 09:55</span>
                                        <p>Наименование организации</p>
                                        <p>Требуются уточнения: В скан-копии брак сканирования на странице 15</p>
                                    </div>
                                    <div class="history__item"><span>22.01.2019 09:55</span>
                                        <p>Фамилия Имя Отчество</p>
                                        <p>Договор отправлен на обработку</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('cabinets/operator/footer')

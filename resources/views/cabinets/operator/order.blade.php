@include('cabinets/operator/header')
<div class="container-fluid">
    <div class="row align-items-center">
        <div class="col-3">
            <div class="header__page">
                <h1>Заказы</h1><a class="add-new" href="/cabinet/operator/order/new" >Создать заказ</a>
            </div>
        </div>
        <div class="col-9">
            <div class="header__page">
                <input class="search" type="text" placeholder="Поиск по наименованию покупателя или тексту события"><a class="filter visible js-filter" href="#"><img src="/cabinets/operator/assets/images/filter.svg" /> Фильтр</a>
                <div class="filter__wrapper">
                    <h3>Фильтр</h3>
                    <form class="filter__wrapper-form">
                        <fieldset>
                            <label>Показать за период</label>
                            <input type="date">
                        </fieldset>
                        <fieldset>
                            <label>Раздел</label>
                            <select>
                                <option>Показать все</option>
                            </select>
                        </fieldset>
                        <fieldset>
                            <label>ПМ</label>
                            <select>
                                <option>Показать все</option>
                            </select>
                        </fieldset><a class="btn btn-primary" href="#">Применить</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-3 pr-0">
            <div class="sidebar js-scroller"><a class="sidebar__item active" href="#">
                    <p>№ 123 от 12.01.2020</p><span>Наименование организации</span><span class="status wait">в обработке</span></a><a class="sidebar__item" href="#">
                    <p>№ 123 от 12.01.2020</p><span>Наименование организации</span><span class="status return">Возвращен на доработку</span></a><a class="sidebar__item" href="#">
                    <p>№ 123 от 12.01.2020</p><span>Наименование организации</span><span class="status acting">Доставлен</span></a><a class="sidebar__item" href="#">
                    <p>№ 123 от 12.01.2020</p><span>Наименование организации</span><span class="status specified">Подтвержден</span></a><a class="sidebar__item" href="#">
                    <p>№ 123 от 12.01.2020</p><span>Наименование организации</span><span class="status expired">Ожидает оплаты</span></a><a class="sidebar__item" href="#">
                    <p>№ 123 от 12.01.2020</p><span>Наименование организации</span><span class="status refusal">Отгружен 1/2</span></a><a class="sidebar__item" href="#">
                    <p>№ 123 от 12.01.2020</p><span>Наименование организации</span><span class="status canceled">отменен</span></a></div>
        </div>
        <div class="col-9 pl-0">
            <div class="body__page js-scroller">
                <div class="body__page-title align-items-start">
                    <div>
                        <h3>Заказ № 123 от 12.02.2020</h3><span>Наименование организации</span>
                    </div>
                    <ul>
                        <li><a href="#">Чат</a></li>
                        <li><a href="/cabinet/operator/contract">Договор</a></li>
                        <li><a href="/cabinet/operator/documents">Документы</a></li>
                    </ul>
                </div>
                <div class="body__page-container">
                    <div class="body__page-form">
                        <div class="body__page-header">
                            <div>
                                <div class="status wait">в обработке</div><span>с 27.02.2019 по 26.02.2020</span>
                            </div>
                        </div>
                        <div class="fieldset">
                            <h3>Основные данные</h3>
                            <div class="row">
                                <div class="col-5">
                                    <div class="block"><span>Вид топлива</span>
                                        <p>Бензин</p>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="block"><span>Количество лотов</span>
                                        <p>2 (120 тонн)</p>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="block"><span>Станция назначения</span>
                                        <p>Асино, Западно-Сибирская ЖД</p>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="block"><span>Подъездной путь / ветка</span>
                                        <p>не указано</p>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="block"><span>Отсрочка</span>
                                        <p class="editable">5</p>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="block"><span>Предоплата</span>
                                        <p class="editable">0%</p>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="block"><span>Стоимость:</span>
                                        <p class="ditable">2 000 000 ₽</p>
                                    </div>
                                </div>
                                <div class="col-10">
                                    <div class="block"><span>Требуется предоставление банковской гарантии или иного обеспечения</span>
                                        <label>
                                            <input type="checkbox">
                                            <p>Обеспечение предоставлено</p>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="fieldset">
                            <h3>Реквизиты</h3>
                            <div class="row">
                                <div class="col-5">
                                    <div class="block"><span>Грузополучатель</span>
                                        <p>Общество с ограниченной ответственностью «Наименование грузополучателя»</p>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="block"><span>Код ОКПО</span>
                                        <p>1234567890</p>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="block"><span>Код ТНГЛ</span>
                                        <p>1234</p>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="block"><span>Номер телефона / факса</span>
                                        <p>+7 (495) 123-45-67</p>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="block"><span>№ р/с:</span>
                                        <p>40702810121234567890</p>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="block"><span>БИК банка-получателя</span>
                                        <p>123456897</p>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="block"><span>Наименование банка</span>
                                        <p>ПАО "Наименование банка"</p>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="block"><span>К/с</span>
                                        <p>40702810121234567890</p>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="block"><span>Адрес грузополучателя</span>
                                        <p>457676, г. Старица, ул. Красной Сосны 13-я линия, дом 31</p>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="block"><span>ИНН / КПП получателя – структурного подразделения:</span>
                                        <p>1234569870</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="fieldset">
                            <h3>Данные об отгрузке</h3>
                            <div class="row">
                                <div class="col-5">
                                    <div class="block"><span>Период отгрузки</span>
                                        <p>20.03.2020 – 30.03.2020</p>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="block"><span>Для кого</span>
                                        <p>Фамилия Имя Отчество</p>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="block"><span>Особые отметки</span>
                                        <p>отсутствуют</p>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="block"><span>Дополнительная информация</span>
                                        <p>отсутствует</p>
                                    </div>
                                </div>
                                <div class="col-12"><a class="plus" href="#">+ Сформировать спецификацию (353 КБ)</a></div>
                            </div>
                        </div>
                        <div class="body__page-form">
                            <form class="form">
                                <fieldset>
                                    <h3>Подписанный договор</h3>
                                    <div class="row">
                                        <div class="col-6">
                                            <label>
                                                <input type="file">
                                            </label>
                                        </div>
                                    </div>
                                    <div class="row align-items-end">
                                        <div class="col-6">
                                            <div class="file mt-0">
                                                <p>Устав организации</p><a href="#"></a>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <fieldset class="mt-5">
                                    <h3>Комментарий</h3>
                                    <div class="row">
                                        <div class="col-6">
                                            <label><span>Дополнительная информация</span>
                                                <textarea></textarea>
                                            </label>
                                        </div>
                                        <div class="col-12">
                                            <p class="wrong">Не получен оригинал договора <a href="#">№ 12345 от 01.02.2020</a></p>
                                            <p class="wrong">Не получен оригинал договора <a href="#">№ 12345 от 01.02.2020</a></p>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                        <div class="fieldset">
                            <div class="row">
                                <div class="col-12 ml"><a class="btn btn-primary" href="#">Подтвердить</a><a class="btn btn-secondary" href="#">Вернуть на доработку</a><a class="btn btn-secondary" href="#">Отменить</a></div>
                            </div>
                        </div>
                        <div class="body__page-spoiler closed">
                            <div class="title">
                                <h3>История событий</h3>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="history__item"><span>22.01.2019 09:55</span>
                                        <p>Фамилия Имя Отчество</p>
                                        <p>Договор отправлен на обработку</p>
                                    </div>
                                    <div class="history__item"><span>22.01.2019 09:55</span>
                                        <p>Наименование организации</p>
                                        <p>Требуются уточнения: В скан-копии брак сканирования на странице 15</p>
                                    </div>
                                    <div class="history__item"><span>22.01.2019 09:55</span>
                                        <p>Фамилия Имя Отчество</p>
                                        <p>Договор отправлен на обработку</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('cabinets/operator/footer')

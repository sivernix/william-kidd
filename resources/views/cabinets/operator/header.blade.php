@php
    $userCurrent = auth()->user();
@endphp
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="HandheldFriendly" content="true">
    <meta http-equiv="cleartype" content="on">
    <meta http-equiv="msthemecompatible" content="no">
    <meta name="format-detection" content="telephone=no">
    <link rel="shortcut icon" type="image/x-icon" href="cabinets/operator/favicon.ico">
    <title>William Kidd - Поставщик углеводородов</title>
    <link href="{{ asset('cabinets/operator/vendors/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('cabinets/operator/vendors/malihu-scrollbar/jquery.mCustomScrollbar.css') }}" rel="stylesheet">
    <link href="{{ asset('cabinets/operator/assets/css/app.css') }}" rel="stylesheet">
    <script type="text/javascript" src="{{ asset('cabinets/operator/vendors/jquery/dist/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('cabinets/operator/vendors/jquery-ui/jquery-ui.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('cabinets/operator/vendors/malihu-scrollbar/jquery.mCustomScrollbar.concat.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('cabinets/operator/assets/js/app.js') }}"></script>
</head>
<body class="body__flex">
<header>
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="col-7">
                <div class="d-flex align-items-center">
                    <div class="logo"><img src="{{ asset('cabinets/operator/assets/images/logo.svg') }}"></div>
                    <ul class="nav">
                        <li><a href="/cabinet/operator/contract">Договоры</a></li>
                        <li><a href="/cabinet/operator/order">Заказы</a></li>
                        <li><a href="/cabinet/operator/chat">Чаты <i style="display: none;">14</i></a></li>
                        <li><a href="/cabinet/operator/profile">Профили</a></li>
                        <li><a class="active" href="/cabinet/operator/documents">Документы</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-2 text-right"><a style="display: none;" class="notify" href="/cabinet/operator/history">История событий <i>124</i></a></div>
            <div class="col-3"><a class="user" href="#">
                    <p>{{$userCurrent->name}}</p><span>ID: {{$userCurrent->id}}</span></a>
                <div class="dd">
                    <ul>
                        <li><a href="/cabinet/operator/password">Изменить пароль</a></li>
                        <li><a href="/logout">Выйти</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>
<main>

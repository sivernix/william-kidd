<footer>© William Kidd LLC, 2008-2021</footer>
<div class="modal modal-side" id="contract">
    <div class="modal-body"><a class="close" href="#" data-dismiss>&#215;</a>
        <form>
            <h2>Выгрузка сводной таблицы профилей</h2>
            <fieldset>
                <div class="row">
                    <div class="col-12">
                        <h4>Сформировать за период <i title="gjlcrfprf">?</i></h4>
                    </div>
                    <div class="col-6">
                        <label>
                            <input type="date">
                        </label>
                    </div>
                    <div class="col-6">
                        <label>
                            <input type="date">
                        </label>
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <div class="row">
                    <div class="col-12">
                        <h4>Данные для выгрузки</h4>
                    </div>
                    <div class="col-6">
                        <label>
                            <input type="checkbox" disabled checked>
                            <p>Номер заказа</p>
                        </label>
                    </div>
                    <div class="col-6">
                        <label>
                            <input type="checkbox" disabled checked>
                            <p>Дата заказа</p>
                        </label>
                    </div>
                    <div class="col-6">
                        <label>
                            <input type="checkbox" disabled checked>
                            <p>Покупатель</p>
                        </label>
                    </div>
                    <div class="col-6">
                        <label>
                            <input type="checkbox">
                            <p>Вид топлива</p>
                        </label>
                    </div>
                    <div class="col-6">
                        <label>
                            <input type="checkbox">
                            <p>Количество лотов</p>
                        </label>
                    </div>
                    <div class="col-6">
                        <label>
                            <input type="checkbox">
                            <p>Станция назначения</p>
                        </label>
                    </div>
                    <div class="col-6">
                        <label>
                            <input type="checkbox">
                            <p>Общая стоимость</p>
                        </label>
                    </div>
                    <div class="col-6">
                        <label>
                            <input type="checkbox">
                            <p>Отсрочка</p>
                        </label>
                    </div>
                    <div class="col-6">
                        <label>
                            <input type="checkbox">
                            <p>Предоплата</p>
                        </label>
                    </div>
                    <div class="col-6">
                        <label>
                            <input type="checkbox">
                            <p>Дата отгрузки</p>
                        </label>
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <div class="row">
                    <div class="col-12">
                        <h4>Статусы</h4>
                    </div>
                    <div class="col-6">
                        <label>
                            <input type="checkbox" disabled checked>
                            <p>В обработке</p>
                        </label>
                    </div>
                    <div class="col-6">
                        <label>
                            <input type="checkbox" disabled checked>
                            <p>В пути</p>
                        </label>
                    </div>
                    <div class="col-6">
                        <label>
                            <input type="checkbox" disabled checked>
                            <p>Возвращен</p>
                        </label>
                    </div>
                    <div class="col-6">
                        <label>
                            <input type="checkbox">
                            <p>Частично отгружен</p>
                        </label>
                    </div>
                    <div class="col-6">
                        <label>
                            <input type="checkbox">
                            <p>Оплачен</p>
                        </label>
                    </div>
                    <div class="col-6">
                        <label>
                            <input type="checkbox">
                            <p>Доставлен</p>
                        </label>
                    </div>
                    <div class="col-6">
                        <label>
                            <input type="checkbox">
                            <p>Подтвержден</p>
                        </label>
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <div class="row">
                    <div class="col-12">
                        <label><span>Оригинал спецификации</span>
                            <select>
                                <option>Выгрузить все</option>
                            </select>
                        </label>
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <div class="row">
                    <div class="col-12">
                        <label><span>Полная оплата</span>
                            <select>
                                <option>Выгрузить все</option>
                            </select>
                        </label>
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <div class="row">
                    <div class="col-12"><a class="btn btn-primary" href="#">скачать</a><a class="btn btn-secondary" href="#" data-dismiss>Отменить</a></div>
                </div>
            </fieldset>
        </form>
    </div>
</div>
<div class="modal modal-side" id="export">
    <div class="modal-body"><a class="close" href="#" data-dismiss>&#215;</a>
        <form>
            <h2>Выгрузка сводной таблицы договоров</h2>
            <fieldset>
                <div class="row">
                    <div class="col-12">
                        <h4>Сформировать за период <i title="gjlcrfprf">?</i></h4>
                    </div>
                    <div class="col-6">
                        <label>
                            <input type="date">
                        </label>
                    </div>
                    <div class="col-6">
                        <label>
                            <input type="date">
                        </label>
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <div class="row">
                    <div class="col-12">
                        <h4>Данные для выгрузки</h4>
                    </div>
                    <div class="col-6">
                        <label>
                            <input type="checkbox" disabled checked>
                            <p>ИНН</p>
                        </label>
                    </div>
                    <div class="col-6">
                        <label>
                            <input type="checkbox" disabled checked>
                            <p>Наименование</p>
                        </label>
                    </div>
                    <div class="col-6">
                        <label>
                            <input type="checkbox" disabled checked>
                            <p>E-mail</p>
                        </label>
                    </div>
                    <div class="col-6">
                        <label>
                            <input type="checkbox">
                            <p>ОГРН</p>
                        </label>
                    </div>
                    <div class="col-6">
                        <label>
                            <input type="checkbox">
                            <p>КПП</p>
                        </label>
                    </div>
                    <div class="col-6">
                        <label>
                            <input type="checkbox">
                            <p>Дата регистрации</p>
                        </label>
                    </div>
                    <div class="col-6">
                        <label>
                            <input type="checkbox">
                            <p>Юридический адрес</p>
                        </label>
                    </div>
                    <div class="col-6">
                        <label>
                            <input type="checkbox">
                            <p>Фактический адрес</p>
                        </label>
                    </div>
                    <div class="col-6">
                        <label>
                            <input type="checkbox">
                            <p>Почтовый адрес</p>
                        </label>
                    </div>
                    <div class="col-6">
                        <label>
                            <input type="checkbox">
                            <p>ЕИО</p>
                        </label>
                    </div>
                    <div class="col-6">
                        <label>
                            <input type="checkbox">
                            <p>ОКПО</p>
                        </label>
                    </div>
                    <div class="col-6">
                        <label>
                            <input type="checkbox">
                            <p>Среднесписочная численность</p>
                        </label>
                    </div>
                    <div class="col-6">
                        <label>
                            <input type="checkbox">
                            <p>Подписанты</p>
                        </label>
                    </div>
                    <div class="col-6">
                        <label>
                            <input type="checkbox">
                            <p>Банк</p>
                        </label>
                    </div>
                    <div class="col-6">
                        <label>
                            <input type="checkbox">
                            <p>БИК банка</p>
                        </label>
                    </div>
                    <div class="col-6">
                        <label>
                            <input type="checkbox">
                            <p>Номер к/с</p>
                        </label>
                    </div>
                    <div class="col-6">
                        <label>
                            <input type="checkbox">
                            <p>Номер р/с</p>
                        </label>
                    </div>
                    <div class="col-6">
                        <label>
                            <input type="checkbox">
                            <p>Контактный номер телефона</p>
                        </label>
                    </div>
                    <div class="col-6">
                        <label>
                            <input type="checkbox">
                            <p>Имя контактного лица</p>
                        </label>
                    </div>
                    <div class="col-6">
                        <label>
                            <input type="checkbox">
                            <p>Персональный менеджер</p>
                        </label>
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <div class="row">
                    <div class="col-12">
                        <label><span>Статус договора</span>
                            <select>
                                <option>Выгрузить все</option>
                            </select>
                        </label>
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <div class="row">
                    <div class="col-12">
                        <label><span>Статус изменений</span>
                            <select>
                                <option>Выгрузить все</option>
                            </select>
                        </label>
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <div class="row">
                    <div class="col-12"><a class="btn btn-primary" href="#">скачать</a><a class="btn btn-secondary" href="#" data-dismiss>Отменить</a></div>
                </div>
            </fieldset>
        </form>
    </div>
</div>
<div class="modal modal-popup" id="closed">
    <div class="modal-container">
        <div class="modal-body"><a class="close" href="#" data-dismiss>&#215;</a>
            <div class="title">
                <h3>Аннулирование договора</h3>
            </div>
            <div class="form">
                <fieldset>
                    <label><span>Укажите причину</span>
                        <textarea placeholder="Например: преращена работа с данной организацией"></textarea>
                    </label>
                </fieldset>
                <fieldset><a class="btn btn-primary" href="#">аннулировать</a><a class="btn btn-secondary" href="#" data-dismiss>отменить</a></fieldset>
            </div>
        </div>
    </div>
</div>
<div class="modal modal-popup" id="sure">
    <div class="modal-container">
        <div class="modal-body"><a class="close" href="#" data-dismiss>&#215;</a>
            <div class="title">
                <h3>Уверены, что хотите покинуть страницу?</h3>
            </div>
            <div class="form">
                <fieldset><a class="btn btn-primary" href="#">Да</a><a class="btn btn-secondary" href="#" data-dismiss>отменить</a></fieldset>
            </div>
        </div>
    </div>
</div>
</main>
</body>
</html>

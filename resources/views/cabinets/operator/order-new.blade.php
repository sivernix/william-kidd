@include('cabinets/operator/header')
<div class="container-fluid">
    <div class="row align-items-center">
        <div class="col-3">
            <div class="header__page">
                <h1>Заказы</h1><a class="add-new" href="/cabinet/operator/order/new">Создать заказ</a>
            </div>
        </div>
        <div class="col-9">
            <div class="header__page">
                <input class="search" type="text" placeholder="Поиск по наименованию покупателя или тексту события"><a class="filter visible js-filter" href="#"><img src="assets/images/filter.svg" /> Фильтр</a>
                <div class="filter__wrapper">
                    <h3>Фильтр</h3>
                    <form class="filter__wrapper-form">
                        <fieldset>
                            <label>Показать за период</label>
                            <input type="date">
                        </fieldset>
                        <fieldset>
                            <label>Раздел</label>
                            <select>
                                <option>Показать все</option>
                            </select>
                        </fieldset>
                        <fieldset>
                            <label>ПМ</label>
                            <select>
                                <option>Показать все</option>
                            </select>
                        </fieldset><a class="btn btn-primary" href="#">Применить</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-3 pr-0">
            <div class="sidebar js-scroller"><a class="sidebar__item" href="#">
                    <p>№ 123 от 12.01.2020</p><span>Наименование организации</span><span class="status wait">в обработке</span></a><a class="sidebar__item" href="#">
                    <p>№ 123 от 12.01.2020</p><span>Наименование организации</span><span class="status return">Возвращен на доработку</span></a><a class="sidebar__item" href="#">
                    <p>№ 123 от 12.01.2020</p><span>Наименование организации</span><span class="status acting">Доставлен</span></a><a class="sidebar__item" href="#">
                    <p>№ 123 от 12.01.2020</p><span>Наименование организации</span><span class="status specified">Подтвержден</span></a><a class="sidebar__item" href="#">
                    <p>№ 123 от 12.01.2020</p><span>Наименование организации</span><span class="status expired">Ожидает оплаты</span></a><a class="sidebar__item" href="#">
                    <p>№ 123 от 12.01.2020</p><span>Наименование организации</span><span class="status refusal">Отгружен 1/2</span></a><a class="sidebar__item" href="#">
                    <p>№ 123 от 12.01.2020</p><span>Наименование организации</span><span class="status canceled">отменен</span></a></div>
        </div>
        <div class="col-9 pl-0">
            <div class="body__page js-scroller">
                <div class="body__page-title">
                    <div>
                        <h3>Заказ №–</h3>
                    </div>
                </div>
                <div class="body__page-container">
                    <div class="body__page-form">
                        <form class="form">
                            <fieldset>
                                <h3>Основные данные</h3>
                                <div class="row">
                                    <div class="col-6">
                                        <label><span>ИНН Заказчика</span>
                                            <input type="text">
                                        </label>
                                    </div>
                                    <div class="col-6">
                                        <label><span>Вид топлива</span>
                                            <input type="text">
                                        </label>
                                    </div>
                                    <div class="col-6">
                                        <div class="row">
                                            <div class="col-6">
                                                <label><span>Лотов</span>
                                                    <input type="text">
                                                </label>
                                            </div>
                                            <div class="col-6">
                                                <label><span>Тонн <i title="gjlcrfprf">?</i></span>
                                                    <input type="text">
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <label><span>Станция назначения</span>
                                            <input type="text">
                                        </label>
                                    </div>
                                    <div class="col-6">
                                        <div class="row">
                                            <div class="col-6">
                                                <label><span>Период отгрузки</span>
                                                    <input type="date">
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <h3>Условия оплаты</h3>
                                <div class="row">
                                    <div class="col-3">
                                        <label><span>Предоплата</span>
                                            <select>
                                                <option>100%</option>
                                            </select>
                                        </label>
                                    </div>
                                    <div class="col-3">
                                        <label><span>Срок оплаты</span>
                                            <input type="text">
                                        </label>
                                    </div>
                                    <div class="col-3">
                                        <label><span>Отсрочка</span>
                                            <input type="text">
                                        </label>
                                    </div>
                                </div>
                                <div class="row align-items-end">
                                    <div class="col-2">
                                        <label><span>Стоимость за тонну, ₽</span>
                                            <input type="text">
                                        </label>
                                    </div>
                                    <div class="col-2">
                                        <label><span>Стоимость партии, ₽</span>
                                            <input type="text">
                                        </label>
                                    </div>
                                    <div class="col-2">
                                        <label><span>Сумма для предоплаты, ₽</span>
                                            <input type="text">
                                        </label>
                                    </div>
                                    <div class="col-3">
                                        <label>
                                            <input type="checkbox">
                                            <p>Оплата по факту прибытия</p>
                                        </label>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset><a class="btn btn-primary ml-0" href="#">сохранить как черновик</a><a class="btn btn-secondary" href="#" data-toggle="modal" data-target="sure">Отменить</a></fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('cabinets/operator/footer')

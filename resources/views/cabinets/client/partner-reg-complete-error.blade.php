@include('cabinets/client/header')

<div class="container-fluid">
    <div class="row">
        <div class="col-9 lk__wrapper-col">
            <div class="lk__wrapper-title">
                <h1>Регистрация нового партнера</h1>
                <p>Для осуществления заказов в нашей Системе пройдите все шаги регистрации</p>
            </div>
            <div class="lk__wrapper-error">
                <p>Обнаружены ошибки</p><a href="#"><img src="{{ asset('assets/images/times.svg') }}"></a>
            </div>
            <div class="lk__wrapper-wrapper js-scroller mCustomScrollbar _mCS_1"><div id="mCSB_1" class="mCustomScrollBox mCS-light mCSB_vertical mCSB_inside" style="max-height: 580px;" tabindex="0"><div id="mCSB_1_container" class="mCSB_container mCS_y_hidden mCS_no_scrollbar_y" style="position:relative; top:0; left:0;" dir="ltr">
                        <div class="row">
                            <div class="col-3">
                                <div class="tline">
                                    <div class="tline-item done"><span>1</span>
                                        <p>Подтверждение данных</p>
                                    </div>
                                    <div class="tline-item done"><span>2</span>
                                        <p>Заполнение профиля</p>
                                    </div>
                                    <div class="tline-item done"><span>3</span>
                                        <p>Загрузка документов</p>
                                    </div>
                                    <div class="tline-item done"><span>4</span>
                                        <p>Проверка данных</p>
                                    </div>
                                    <div class="tline-item active"><span>5</span>
                                        <p>Завершено</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-9">
                                <form>
                                    <div class="title title-error">
                                        <h3>Требуется доработка договора</h3>
                                        <p>Просим вас исправить ошибки в разделах:</p>
                                        <p class="mb-1"><b>Подтверждение данных</b></p>
                                        <p>Допущена ошибка в полном наименовании организации. Не заполнено поле “Должность”</p>
                                        <p class="mb-1"><b>Заполнение профиля</b></p>
                                        <p>Не соответствует БИК банка</p>
                                        <p class="mb-1"><b>Загрузка документов</b></p>
                                        <p>Отсутствует страница Устава с печатью ФНС</p>
                                        <div class="d-flex"><a class="btn btn-primary" href="#">Начать исправлять</a><a class="btn btn-secondary" href="#">Отправить на проверку</a></div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div><div id="mCSB_1_scrollbar_vertical" class="mCSB_scrollTools mCSB_1_scrollbar mCS-light mCSB_scrollTools_vertical" style="display: none;"><div class="mCSB_draggerContainer"><div id="mCSB_1_dragger_vertical" class="mCSB_dragger" style="position: absolute; min-height: 30px; height: 0px; top: 0px;"><div class="mCSB_dragger_bar" style="line-height: 30px;"></div></div><div class="mCSB_draggerRail"></div></div></div></div></div>
        </div>
        <div class="col-3">
            @include('cabinets/client/chat/chat')
        </div>
    </div>
</div>

@include('cabinets/client/footer')

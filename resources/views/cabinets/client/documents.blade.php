@include('cabinets/client/header')
<div class="container-fluid">
    <div class="row align-items-center">
        <div class="col-3">
            <div class="header__page">
                <h1>Документы</h1>
            </div>
        </div>
        <div class="col-9">
            <div class="header__page">
                <input class="search" type="text" placeholder="Поиск по наименованию покупателя или тексту события"><a class="filter visible js-filter" href="#"><img src="/cabinets/operator/assets/images/filter.svg" /> Фильтр</a>
                <div class="filter__wrapper">
                    <h3>Фильтр</h3>
                    <form class="filter__wrapper-form">
                        <fieldset>
                            <label>Показать за период</label>
                            <input type="date">
                        </fieldset>
                        <fieldset>
                            <label>Раздел</label>
                            <select>
                                <option>Показать все</option>
                            </select>
                        </fieldset>
                        <fieldset>
                            <label>ПМ</label>
                            <select>
                                <option>Показать все</option>
                            </select>
                        </fieldset><a class="btn btn-primary" href="#">Применить</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-3 pr-0">
            <div class="sidebar js-scroller"><a class="sidebar__item" href="#">
                    <p>ИП Фамилия Имя Отчество</p><span>ИНН: 77123456789012</span></a><a class="sidebar__item active" href="#">
                    <p>ИП Фамилия Имя Отчество</p><span>ИНН: 77123456789012</span></a><a class="sidebar__item" href="#">
                    <p>ИП Фамилия Имя Отчество</p><span>ИНН: 77123456789012</span></a><a class="sidebar__item" href="#">
                    <p>ИП Фамилия Имя Отчество</p><span>ИНН: 77123456789012</span></a><a class="sidebar__item" href="#">
                    <p>ИП Фамилия Имя Отчество</p><span>ИНН: 77123456789012</span></a><a class="sidebar__item" href="#">
                    <p>ИП Фамилия Имя Отчество</p><span>ИНН: 77123456789012</span></a><a class="sidebar__item" href="#">
                    <p>ИП Фамилия Имя Отчество</p><span>ИНН: 77123456789012</span></a><a class="sidebar__item" href="#">
                    <p>ИП Фамилия Имя Отчество</p><span>ИНН: 77123456789012</span></a><a class="sidebar__item" href="#">
                    <p>ИП Фамилия Имя Отчество</p><span>ИНН: 77123456789012</span></a><a class="sidebar__item" href="#">
                    <p>ИП Фамилия Имя Отчество</p><span>ИНН: 77123456789012</span></a><a class="sidebar__item" href="#">
                    <p>ИП Фамилия Имя Отчество</p><span>ИНН: 77123456789012</span></a><a class="sidebar__item" href="#">
                    <p>ИП Фамилия Имя Отчество</p><span>ИНН: 77123456789012</span></a><a class="sidebar__item" href="#">
                    <p>ИП Фамилия Имя Отчество</p><span>ИНН: 77123456789012</span></a><a class="sidebar__item" href="#">
                    <p>ИП Фамилия Имя Отчество</p><span>ИНН: 77123456789012</span></a><a class="sidebar__item" href="#">
                    <p>ИП Фамилия Имя Отчество</p><span>ИНН: 77123456789012</span></a><a class="sidebar__item" href="#">
                    <p>ИП Фамилия Имя Отчество</p><span>ИНН: 77123456789012</span></a><a class="sidebar__item" href="#">
                    <p>ИП Фамилия Имя Отчество</p><span>ИНН: 77123456789012</span></a></div>
        </div>
        <div class="col-9 pl-0">
            <div class="body__page js-scroller">
                <div class="body__page-title">
                    <div>
                        <h3>ИП Фамилия Имя Отчество</h3>
                        <p>ИНН: 77123456789012</p>
                    </div>
                    <ul>
                        <li><a href="/cabinet/operator/chat">Чат</a></li>
                        <li><a href="/cabinet/operator/order">Заказы</a></li>
                        <li><a href="/cabinet/operator/profile">Профиль</a></li>
                        <li><a href="/cabinet/operator/contract">Договоры</a></li>
                    </ul>
                </div>
                <div class="body__page-container table__wrapper">
                    <table>
                        <thead>
                        <tr>
                            <th>тип документа</th>
                            <th>дата загрузки</th>
                            <th>Размер</th>
                            <th>действия</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td><b>Договор от 28.01.2020.zip</b></td>
                            <td>Загружен 28.01.2020 (ИП Фамилия Имя Отчество)</td>
                            <td>151 Кб</td>
                            <td>
                                <div class="action"><a href="#"><img src="/cabinets/operator/assets/images/download.svg" /> Скачать</a><a href="#"><img src="/cabinets/operator/assets/images/remove.svg" /> Удалить</a></div>
                            </td>
                        </tr>
                        <tr>
                            <td><b>Договор от 28.01.2020.zip</b></td>
                            <td>Загружен 28.01.2020 (ИП Фамилия Имя Отчество)</td>
                            <td>151 Кб</td>
                            <td>
                                <div class="action"><a href="#"><img src="/cabinets/operator/assets/images/download.svg" /> Скачать</a><a href="#"><img src="/cabinets/operator/assets/images/remove.svg" /> Удалить</a></div>
                            </td>
                        </tr>
                        <tr>
                            <td><b>Договор от 28.01.2020.zip</b></td>
                            <td>Загружен 28.01.2020 (ИП Фамилия Имя Отчество)</td>
                            <td>151 Кб</td>
                            <td>
                                <div class="action"><a href="#"><img src="/cabinets/operator/assets/images/download.svg" /> Скачать</a><a href="#"><img src="/cabinets/operator/assets/images/remove.svg" /> Удалить</a></div>
                            </td>
                        </tr>
                        <tr>
                            <td><b>Договор от 28.01.2020.zip</b></td>
                            <td>Загружен 28.01.2020 (ИП Фамилия Имя Отчество)</td>
                            <td>151 Кб</td>
                            <td>
                                <div class="action"><a href="#"><img src="/cabinets/operator/assets/images/download.svg" /> Скачать</a><a href="#"><img src="/cabinets/operator/assets/images/remove.svg" /> Удалить</a></div>
                            </td>
                        </tr>
                        <tr>
                            <td><b>Договор от 28.01.2020.zip</b></td>
                            <td>Загружен 28.01.2020 (ИП Фамилия Имя Отчество)</td>
                            <td>151 Кб</td>
                            <td>
                                <div class="action"><a href="#"><img src="/cabinets/operator/assets/images/download.svg" /> Скачать</a><a href="#"><img src="/cabinets/operator/assets/images/remove.svg" /> Удалить</a></div>
                            </td>
                        </tr>
                        <tr>
                            <td><b>Договор от 28.01.2020.zip</b></td>
                            <td>Загружен 28.01.2020 (ИП Фамилия Имя Отчество)</td>
                            <td>151 Кб</td>
                            <td>
                                <div class="action"><a href="#"><img src="/cabinets/operator/assets/images/download.svg" /> Скачать</a><a href="#"><img src="/cabinets/operator/assets/images/remove.svg" /> Удалить</a></div>
                            </td>
                        </tr>
                        <tr>
                            <td><b>Договор от 28.01.2020.zip</b></td>
                            <td>Загружен 28.01.2020 (ИП Фамилия Имя Отчество)</td>
                            <td>151 Кб</td>
                            <td>
                                <div class="action"><a href="#"><img src="/cabinets/operator/assets/images/download.svg" /> Скачать</a><a href="#"><img src="/cabinets/operator/assets/images/remove.svg" /> Удалить</a></div>
                            </td>
                        </tr>
                        <tr>
                            <td><b>Договор от 28.01.2020.zip</b></td>
                            <td>Загружен 28.01.2020 (ИП Фамилия Имя Отчество)</td>
                            <td>151 Кб</td>
                            <td>
                                <div class="action"><a href="#"><img src="/cabinets/operator/assets/images/download.svg" /> Скачать</a><a href="#"><img src="/cabinets/operator/assets/images/remove.svg" /> Удалить</a></div>
                            </td>
                        </tr>
                        <tr>
                            <td><b>Договор от 28.01.2020.zip</b></td>
                            <td>Загружен 28.01.2020 (ИП Фамилия Имя Отчество)</td>
                            <td>151 Кб</td>
                            <td>
                                <div class="action"><a href="#"><img src="/cabinets/operator/assets/images/download.svg" /> Скачать</a><a href="#"><img src="/cabinets/operator/assets/images/remove.svg" /> Удалить</a></div>
                            </td>
                        </tr>
                        <tr>
                            <td><b>Договор от 28.01.2020.zip</b></td>
                            <td>Загружен 28.01.2020 (ИП Фамилия Имя Отчество)</td>
                            <td>151 Кб</td>
                            <td>
                                <div class="action"><a href="#"><img src="/cabinets/operator/assets/images/download.svg" /> Скачать</a><a href="#"><img src="/cabinets/operator/assets/images/remove.svg" /> Удалить</a></div>
                            </td>
                        </tr>
                        <tr>
                            <td><b>Договор от 28.01.2020.zip</b></td>
                            <td>Загружен 28.01.2020 (ИП Фамилия Имя Отчество)</td>
                            <td>151 Кб</td>
                            <td>
                                <div class="action"><a href="#"><img src="/cabinets/operator/assets/images/download.svg" /> Скачать</a><a href="#"><img src="/cabinets/operator/assets/images/remove.svg" /> Удалить</a></div>
                            </td>
                        </tr>
                        <tr>
                            <td><b>Договор от 28.01.2020.zip</b></td>
                            <td>Загружен 28.01.2020 (ИП Фамилия Имя Отчество)</td>
                            <td>151 Кб</td>
                            <td>
                                <div class="action"><a href="#"><img src="/cabinets/operator/assets/images/download.svg" /> Скачать</a><a href="#"><img src="/cabinets/operator/assets/images/remove.svg" /> Удалить</a></div>
                            </td>
                        </tr>
                        <tr>
                            <td><b>Договор от 28.01.2020.zip</b></td>
                            <td>Загружен 28.01.2020 (ИП Фамилия Имя Отчество)</td>
                            <td>151 Кб</td>
                            <td>
                                <div class="action"><a href="#"><img src="/cabinets/operator/assets/images/download.svg" /> Скачать</a><a href="#"><img src="/cabinets/operator/assets/images/remove.svg" /> Удалить</a></div>
                            </td>
                        </tr>
                        <tr>
                            <td><b>Договор от 28.01.2020.zip</b></td>
                            <td>Загружен 28.01.2020 (ИП Фамилия Имя Отчество)</td>
                            <td>151 Кб</td>
                            <td>
                                <div class="action"><a href="#"><img src="/cabinets/operator/assets/images/download.svg" /> Скачать</a><a href="#"><img src="/cabinets/operator/assets/images/remove.svg" /> Удалить</a></div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@include('cabinets/client/footer')

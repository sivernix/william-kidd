@include('header')
<div class="body__wrapper body__wrapper-login">
    <div class="body__wrapper-image">
        <h3>С «William Kidd» удобно работать:</h3>
        <ul>
            <li>Заключайте договоры поставки прямо в <b>Личном кабинете</b></li>
            <li>Следите за всеми этапами доставки</li>
            <li>С любыми вопросами обращайтесь к менеджеру в&nbsp;персональном чате</li>
        </ul>
    </div>
    <div class="body__wrapper-form">
        @if(empty(app('request')->input('step')))
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xl-8 col-10 mx-auto">
                        <h1>Регистрация</h1>
                        <form class="form">
                            <fieldset>
                                <div class="form__label">
                                    <label>ИНН организации / ИП</label>
                                </div>
                                <input type="text">
                            </fieldset>
                            <fieldset>
                                <div class="form__label">
                                    <label>E-mail</label>
                                </div>
                                <input type="text">
                            </fieldset>
                            <a class="btn btn-primary js__btn-submit" href="/signup?step=1">Зарегистрироваться</a><a href="/login">У меня
                                уже заключен договор</a>
                            <fieldset>
                                <div class="form__label-check">
                                    <label>
                                        <input type="checkbox">
                                        <p>Подтверждаю согласие c <a href="#">Пользовательским соглашением и Политикой
                                                конфиденциальности.</a></p>
                                    </label>
                                </div>
                            </fieldset>
                        </form>
                        <div class="problem"><strong>Не можете войти в ЛК? </strong>
                            <p>Если у вас уже заключен договор, но нет доступа в личный кабинет, обратитесь к своему
                                персональному менеджеру или по общему телефону <a href="#">+7 (495) 120-73-37</a></p>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        @if(app('request')->input('step') == 1)
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xl-8 col-10 mx-auto">
                        <h1>Регистрация</h1>
                        <form class="form">
                            <div class="row">
                                <div class="col-5"><span>ИНН организации / ИП</span>
                                    <p>123467890</p>
                                </div>
                                <div class="col-7"><span>E-mail</span>
                                    <p>jonnkidd@mail.ru <a href="#">Изменить E-mail</a></p>
                                </div>
                            </div>
                            <p>Не получили письмо? <br>Проверьте папку «Спам» или повторите отправку.</p>
                            <a class="btn btn-primary js__btn-submit" href="/signup?step=2">Повторить через 04:59</a>
                            <fieldset class="mt-4">
                                <div class="form__label-check">
                                    <label>
                                        <input type="checkbox">
                                        <p>Подтверждаю согласие c <a href="#">Пользовательским соглашением и Политикой конфиденциальности.</a></p>
                                    </label>
                                </div>
                            </fieldset>
                        </form>
                        <div class="problem"><strong>Не можете войти в ЛК? </strong>
                            <p>Если у вас уже заключен договор, но нет доступа в личный кабинет, обратитесь к своему персональному менеджеру или по общему телефону <a href="#">+7 (495) 120-73-37</a></p>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        @if(app('request')->input('step') == 2)
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xl-8 col-10 mx-auto">
                        <h1>Регистрация</h1>
                        <div class="register">
                            <p>Придумайте пароль для учетной записи:</p>
                        </div>
                        <form class="form">
                            <fieldset>
                                <div class="form__label">
                                    <label>Новый пароль</label>
                                </div>
                                <input type="password">
                                <div class="form__label-pwd js-pwd hide"></div><span>Минимум 6 символов</span>
                            </fieldset>
                            <fieldset>
                                <div class="form__label">
                                    <label>Повторите пароль:</label>
                                </div>
                                <input type="password">
                                <div class="form__label-pwd js-pwd hide"></div>
                            </fieldset><a class="btn btn-primary js__btn-submit" href="#">Сохранить</a>
                        </form>
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>

@include('footer')

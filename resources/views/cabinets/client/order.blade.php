@include('cabinets/client/header')

<div class="container-fluid">
    <div class="row">
        <div class="col-9 lk__wrapper-col">
            <div class="lk__wrapper-title">
                <h1>Заказ №9576 <span>от 27.02.2020</span></h1>
            </div>
            <div class="row">
                <div class="col-4">
                    <div class="lk__wrapper-side">
                        <h3>Характеристики заказа</h3><span class="status wait">Ожидает оплаты</span>
                        <div class="row">
                            <div class="col-12">
                                <div><span>Вид топлива:</span>
                                    <p>бензин (АИ-100-К5) СТО 78689379-32-2018</p>
                                </div>
                            </div>
                            <div class="col-12">
                                <div><span>Количество лотов:</span>
                                    <p>2 (120 тонн)</p>
                                </div>
                            </div>
                            <div class="col-12">
                                <div><span>Станция назначения:</span>
                                    <p>Асино, Западно-Сибирская ЖД</p>
                                </div>
                            </div>
                            <div class="col-6">
                                <div><span>Отсрочка:</span>
                                    <p>0 дней</p>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="warning"><span>Предоплата: <i title="tint"></i></span>
                                    <p>100% (2 000 000 ₽)</p>
                                </div>
                            </div>
                            <div class="col-12">
                                <div><span>Стоимость</span>
                                    <p>2 000 000 ₽</p>
                                </div>
                            </div>
                        </div><a href="#"><span>Скачать спецификацию (115 КБ)</span></a>
                    </div><span>Если обновление статуса оплаты не произошло, свяжитесь с&nbsp;оператором в&nbsp;чате или персональным менеджером.</span>
                </div>
                <div class="col-8">
                    <div class="lk__wrapper-invoice">
                        <div class="lk__wrapper-invoice-item lk__wrapper-invoice-map">
                            <div><strong>Накладная № 150005</strong></div>
                            <div class="row">
                                <div class="col-4">
                                    <div id="map"><img src="{{ asset('assets/images/map.jpg') }}"></div>
                                </div>
                                <div class="col-8">
                                    <div class="row">
                                        <div class="col-6"><span>Фактическая дата отгрузки:</span>
                                            <p>13.01.2020</p>
                                        </div>
                                        <div class="col-6"><span>Прогноз прибытия: <i title="dfdsf"></i></span>
                                            <p>04.02.2020</p>
                                        </div>
                                        <div class="col-12"><span>Станция дислокации:</span>
                                            <p>Омск-Восточный, Западно-Сибирская ЖД</p>
                                        </div>
                                        <div class="col-4"><span>Номер вагона</span>
                                            <p>67845345</p>
                                        </div>
                                        <div class="col-4"><span>Вес:</span>
                                            <p>60,5 тонн</p>
                                        </div>
                                        <div class="col-4"><span>Фактический вес:</span>
                                            <p>60,5 тонн</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="lk__wrapper-invoice-item">
                            <div><strong>Накладная № 150005</strong>
                                <p>Доставлено: 04.02.2020</p>
                            </div><a href="#"><span>Скачать (428 КБ)</span></a>
                        </div>
                        <div class="lk__wrapper-invoice-item">
                            <div><strong>Накладная № 121255</strong>
                                <p>Доставлено: 04.02.2020</p>
                            </div><a href="#"><span>Скачать (428 КБ)</span></a>
                        </div>
                        <div class="lk__wrapper-invoice-item">
                            <div><strong class="mb-0">Ожидает отгрузки: 1 лот</strong></div>
                        </div>
                    </div>
                    <div class="lk__wrapper-order">
                        <h3>Данные заказа</h3>
                        <div class="row">
                            <div class="col-12">
                                <div><span>Грузополучатель (полное наименование):</span>
                                    <p>Общество с ограниченной ответственностью "Автосекрет"</p>
                                </div>
                            </div>
                            <div class="col-12">
                                <div><span>ИНН / КПП получателя - структурного подразделения:</span>
                                    <p>1234567890</p>
                                </div>
                            </div>
                            <div class="col-4">
                                <div><span>Код ОКПО:</span>
                                    <p>1234567890123456</p>
                                </div>
                            </div>
                            <div class="col-4">
                                <div><span>Код ТГНЛ:</span>
                                    <p>1234</p>
                                </div>
                            </div>
                            <div class="col-12">
                                <div><span>Адрес грузополучателя:</span>
                                    <p>186207, г. Заречный, ул. На дачу Долгорукова дор, дом 56</p>
                                </div>
                            </div>
                            <div class="col-12">
                                <div><span>Номер телефона / факса:</span>
                                    <p>+7 (951) 491-96-61</p>
                                </div>
                            </div>
                            <div class="col-12">
                                <div><span>Для кого (ФИО):</span>
                                    <p>Войтова Лисоведа Игоревна</p>
                                </div>
                            </div>
                            <div class="col-12">
                                <div><span>Особые отметки:</span>
                                    <p>Отсуствуют</p>
                                </div>
                            </div>
                            <div class="col-12">
                                <div><span>№ р/с:</span>
                                    <p>12345678901234567890</p>
                                </div>
                            </div>
                            <div class="col-4">
                                <div><span>БИК Банка</span>
                                    <p>123456789</p>
                                </div>
                            </div>
                            <div class="col-4">
                                <div><span>Наименование Банка</span>
                                    <p>ПАО "Банк Фламинго"</p>
                                </div>
                            </div>
                            <div class="col-4">
                                <div><span>К/с Банка</span>
                                    <p>12345678901234567890</p>
                                </div>
                            </div>
                            <div class="col-8">
                                <div><span>Период отгрузки</span>
                                    <p>16.03.2020 - 19.03.2020</p>
                                </div>
                            </div>
                            <div class="col-4">
                                <div><span>Подъездная ветка</span>
                                    <p>не указана</p>
                                </div>
                            </div>
                            <div class="col-12"><strong>Загруженные документы:</strong>
                                <p>отсутствуют</p>
                            </div>
                            <div class="col-12"><strong>Доолнительная информация</strong>
                                <p>ООО "Наменование организации": Получится ли быстро оформить заказ, продукт нужен очень срочно</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-3">
            @include('cabinets/client/chat/chat')
        </div>
    </div>
</div>

@include('cabinets/client/footer')

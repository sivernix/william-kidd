@include('cabinets/client/header')
<div class="container-fluid">
    <div class="row">

        <div class="col-9 lk__wrapper-col">
            <div class="lk__wrapper-title">
                <h1>Новый заказ</h1>
            </div>
            <div class="row">
                <div class="col-4">
                    <div class="lk__wrapper-side">
                        <h3>Характеристики заказа</h3>
                        <div class="row">
                            <div class="col-12">
                                <div><span>Вид топлива:</span>
                                    <p>бензин (АИ-100-К5) СТО 78689379-32-2018</p>
                                </div>
                            </div>
                            <div class="col-12">
                                <div><span>Количество лотов:</span>
                                    <p>2 (120 тонн)</p>
                                </div>
                            </div>
                            <div class="col-12">
                                <div><span>Станция назначения:</span>
                                    <p>Асино, Западно-Сибирская ЖД</p>
                                </div>
                            </div>
                            <div class="col-6">
                                <div><span>Отсрочка:</span>
                                    <p>0 дней</p>
                                </div>
                            </div>
                            <div class="col-6">
                                <div><span>Предоплата:</span>
                                    <p>100% (2 000 000 ₽)</p>
                                </div>
                            </div>
                            <div class="col-12">
                                <div><span>Стоимость</span>
                                    <p>2 000 000 ₽</p>
                                </div>
                            </div>
                        </div><a class="clear" href="#"><span>Изменить условия расчета</span></a>
                    </div>
                </div>
                <div class="col-8">
                    <div class="lk__wrapper-wrapper">
                        <h3>Для оформления заказа укажите следующие данные</h3>
                        <p>* - поля, обязательные для заполнения</p>
                        <form>
                            <div class="row">
                                <div class="col-12">
                                    <label>
                                        <input type="checkbox">
                                        <p>Заполнить из профиля</p>
                                    </label>
                                </div>
                            </div>
                            <fieldset>
                                <div class="row">
                                    <div class="col-10">
                                        <label><span>Грузополучатель (полное наименование)*</span>
                                            <input type="text">
                                        </label>
                                    </div>
                                    <div class="col-10">
                                        <label><span>ИНН / КПП получателя - структурного подразделения*</span>
                                            <input type="text">
                                        </label>
                                    </div>
                                    <div class="col-5">
                                        <label><span>Код ОКПО*</span>
                                            <input type="text">
                                        </label>
                                    </div>
                                    <div class="col-5">
                                        <label><span>Код ТГНЛ* <i title="erfwerwe"></i></span>
                                            <input type="text">
                                        </label>
                                    </div>
                                    <div class="col-10">
                                        <label><span>Адрес грузополучателя:*</span>
                                            <input type="text">
                                        </label>
                                    </div>
                                    <div class="col-5">
                                        <label><span>Номер телефона / факса*</span>
                                            <input type="text">
                                        </label>
                                    </div>
                                    <div class="col-10">
                                        <label><span>Для кого (ФИО)*</span>
                                            <input type="text">
                                        </label>
                                    </div>
                                    <div class="col-10">
                                        <label><span>Особые отметки</span>
                                            <textarea></textarea>
                                        </label>
                                    </div>
                                    <div class="col-10">
                                        <div class="row">
                                            <div class="col-4">
                                                <label><span>БИК банка*</span>
                                                    <input type="text">
                                                </label>
                                            </div>
                                            <div class="col-4">
                                                <label><span>Наименование банка*</span>
                                                    <input type="text">
                                                </label>
                                            </div>
                                            <div class="col-4">
                                                <label><span>К/с банка*</span>
                                                    <input type="text">
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-10">
                                        <div class="row">
                                            <div class="col-4">
                                                <label><span>Период отгрузки*</span>
                                                    <input type="date">
                                                </label>
                                            </div>
                                            <div class="col-4">
                                                <label><span></span>
                                                    <input type="date">
                                                </label>
                                            </div>
                                            <div class="col-4">
                                                <label><span>Подъездная ветка</span>
                                                    <input type="text">
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <div class="row">
                                    <div class="col-10"><strong>Загрузить документ (при наличии)</strong></div>
                                    <div class="col-10">
                                        <label>
                                            <input type="file">
                                        </label>
                                    </div>
                                    <div class="col-10">
                                        <label><span>Дополнительная информация:</span>
                                            <textarea></textarea>
                                        </label>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <div class="d-flex gap-20"><a class="btn btn-primary" href="#">Оформить заказ</a><a class="btn btn-secondary" href="#">Сохранить в черновик</a><a class="btn btn-secondary" href="#">Отменить</a></div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-3">
            @include('cabinets/client/chat/chat')
        </div>
    </div>
</div>

@include('cabinets/client/footer')

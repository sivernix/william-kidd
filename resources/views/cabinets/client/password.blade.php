<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="HandheldFriendly" content="true">
    <meta http-equiv="cleartype" content="on">
    <meta http-equiv="msthemecompatible" content="no">
    <meta name="format-detection" content="telephone=no">
    <link rel="shortcut icon" type="image/x-icon" href="cabinets/client/favicon.ico">
    <title>William Kidd - Поставщик углеводородов</title>
    <link href="{{ asset('cabinets/client/vendors/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('cabinets/client/vendors/malihu-scrollbar/jquery.mCustomScrollbar.css') }}" rel="stylesheet">
    <link href="{{ asset('cabinets/client/assets/css/app.css') }}" rel="stylesheet">
    <script type="text/javascript" src="{{ asset('cabinets/client/vendors/jquery/dist/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('cabinets/client/vendors/jquery-ui/jquery-ui.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('cabinets/client/vendors/malihu-scrollbar/jquery.mCustomScrollbar.concat.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('cabinets/client/assets/js/app.js') }}"></script>
</head>
<body>
<div class="page__wrapper">
    <div class="page__wrapper-cover" style="background-image: url('cabinets/client/assets/images/banner.jpg')">
        <div class="logo"><img src="cabinets/client/assets/images/logo.svg"></div>
    </div>
    <div class="page__wrapper-container">
        <div class="container-fluid h-100">
            <div class="page__wrapper-flex">
                <div class="page__wrapper-phone text-right"><a href="tel:+74951207337">+7 (495) 120-73-37</a><br><span>Горячая линия</span></div>
                <div class="page__wrapper-form">
                    <div class="col-7 mx-auto">
                        <form class="page__wrapper-form__login form" method="POST" action="{{ route('user-password.update') }}">
                            @csrf
                            @method('PUT')

                            <div class="form__title">
                                <h1>Изменить пароль</h1>
                            </div>
                            <fieldset>
                                <div class="form__label">
                                    <label>Действующий пароль</label><a href="#">Забыли пароль?</a>
                                </div>
                                <div class="form__label-pwd js-pwd hide"></div>
                                <input id="current_password" type="password" class="form-control @error('current_password') is-invalid @enderror" name="current_password" required autofocus>

                                @error('current_password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </fieldset>
                            <fieldset>
                                <div class="form__label">
                                    <label>Новый пароль</label>
                                </div>
                                <div class="form__label-pwd js-pwd hide"></div>
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required>

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </fieldset>
                            <fieldset>
                                <div class="form__label">
                                    <label>Новый пароль еще раз</label>
                                </div>
                                <div class="form__label-pwd js-pwd hide"></div>
                                <input id="password-confirm" type="password" class="form-control" name="password-confirm" required>
                            </fieldset><button class="btn btn-primary js__btn-submit">Сохранить</button>
                        </form>@dd($errors)
                    </div>
                </div>
                <div class="page__wrapper-copyright text-right">
                    <p>© William Kidd LLC, 2008-2021</p>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>

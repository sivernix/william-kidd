@include('header')
<div class="body__wrapper body__wrapper-login">
    <div class="body__wrapper-image">
        <h3>С «William Kidd» удобно работать:</h3>
        <ul>
            <li>Заключайте договоры поставки прямо в <b>Личном кабинете</b></li>
            <li>Следите за всеми этапами доставки</li>
            <li>С любыми вопросами обращайтесь к менеджеру в персональном чате</li>
        </ul>
    </div>
    <div class="body__wrapper-form">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-8 col-10 mx-auto">
                    @if($page =='ok')
                    <h1>Регистрация</h1>
                    <div class="register">
                        <p>Придумайте пароль для учетной записи:</p>
                        <p style="color: red">{{$error}}</p>
                    </div>
                    <form class="form" method="POST" action="/cabinet/client/registration-password">
                        @csrf
                        <fieldset>
                            <div class="form__label">
                                <label>Новый пароль</label>
                            </div>
                            <input type="password" name="password" minlength="6">
                            <div class="form__label-pwd js-pwd hide"></div><span>Минимум 6 символов</span>
                        </fieldset>
                        <fieldset>
                            <div class="form__label">
                                <label>Повторите пароль:</label>
                            </div>
                            <input type="password" name="password-repeat">
                            <div class="form__label-pwd js-pwd hide"></div>
                        </fieldset>
                        <input type="hidden" name="code" value="{{$code}}">
                        <button class="btn btn-primary js__btn-submit" type="submit">Сохранить</button>
                    </form>
                    @else
                        <div class="register">
                            <p>Неверная контрольная строка</p>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>

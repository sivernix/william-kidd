@include('cabinets/client/header')

<div class="container-fluid">
    <div class="row">
        <div class="col-9 lk__wrapper-col">
            <div class="row">
                <div class="col-3"><img class="cover" src="{{ asset('assets/images/lk-reg.jpg') }}"></div>
                <div class="col-8 ml-auto">
                    <div class="entry order-empty">
                        <h1>Регистрация нового партнера</h1>
                        <p>Для осуществления заказов в нашей Системе<br> необходимо пройти 4 шага регистрации договора</p>
                        <ol>
                            <li>Подтверждение данных</li>
                            <li>Заполнение профиля</li>
                            <li>Загрузка документов</li>
                            <li>Проверка данных</li>
                            <li>Завершено</li>
                        </ol><a class="btn btn-primary" href="/cabinet/client/partner-reg-confirm">Начать</a>
                    </div>
                    <div class="contact">
                        <p>Если у вас есть вопросы, вы можете связаться с нами по телефону  <a href="#">+7 495 120 73-37</a></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-3">
            @include('cabinets/client/chat/chat')
        </div>
    </div>
</div>

@include('cabinets/client/footer')

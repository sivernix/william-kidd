@include('header')
<div class="body__wrapper body__wrapper-login">
    <div class="body__wrapper-image">
        <h3>С «William Kidd» удобно работать:</h3>
        <ul>
            <li>Заключайте договоры поставки прямо в <b>Личном кабинете</b></li>
            <li>Следите за всеми этапами доставки</li>
            <li>С любыми вопросами обращайтесь к менеджеру в&nbsp;персональном чате</li>
        </ul>
    </div>
    <div class="body__wrapper-form">
        <div class="container-fluid">
            <div class="row">
                <div class="col-8 mx-auto"> 
                    <h1>Вход в Личный кабинет</h1>
                    <div class="register">
                        <p>У вас еще нет аккаунта?</p><a href="/cabinet/client/registration">Зарегистрироваться</a>
                    </div>

                    <form class="page__wrapper-form__login form" method="POST" action="{{ route('login.client') }}">
                        @csrf
                        <fieldset>
                            <div class="form__label">
                                <label>ИНН / E-mail</label>
                            </div>
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </fieldset>
                        <fieldset>
                            <div class="form__label">
                                <label>Пароль</label>
                                <a href="#">Забыли пароль?</a>
                            </div>

                            <input id="password" type="password"
                                   class="form-control @error('password') is-invalid @enderror" name="password" required
                                   autocomplete="current-password">
                            <div class="form__label-pwd js-pwd hide"></div>

                            @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror


                        </fieldset>
                        <fieldset>
                            <div class="form__label-check">
                                <label>
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                    <p>Запомнить пароль </p>
                                </label>
                            </div>
                        </fieldset><button class="btn btn-primary js__btn-submit" type="submit">Отправить</button>
                    </form>
                    <div class="problem"><strong>Не можете войти в ЛК? </strong>
                        <p>Если у вас уже заключен договор, но нет доступа в личный кабинет, обратитесь к своему персональному менеджеру или по общему телефону <a href="#">+7 (495) 120-73-37</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('footer')

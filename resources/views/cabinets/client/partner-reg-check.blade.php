@include('cabinets/client/header')

<div class="container-fluid">
    <div class="row">
        <div class="col-9 lk__wrapper-col">
            <div class="lk__wrapper-title">
                <h1>Регистрация нового партнера</h1>
                <p>Для осуществления заказов в нашей Системе пройдите все шаги регистрации</p>
            </div>
            <div class="lk__wrapper-wrapper js-scroller mCustomScrollbar _mCS_1"><div id="mCSB_1" class="mCustomScrollBox mCS-light mCSB_vertical mCSB_inside" style="max-height: none;" tabindex="0"><div id="mCSB_1_container" class="mCSB_container mCS_y_hidden mCS_no_scrollbar_y" style="position:relative; top:0; left:0;" dir="ltr">
                        <div class="row">
                            <div class="col-3">
                                <div class="tline">
                                    <div class="tline-item done"><span>1</span>
                                        <p>Подтверждение данных</p>
                                    </div>
                                    <div class="tline-item done"><span>2</span>
                                        <p>Заполнение профиля</p>
                                    </div>
                                    <div class="tline-item done"><span>3</span>
                                        <p>Загрузка документов</p>
                                    </div>
                                    <div class="tline-item active"><span>4</span>
                                        <p>Проверка данных</p>
                                    </div>
                                    <div class="tline-item"><span>5</span>
                                        <p>Завершено</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-7">
                                <form id="partner-reg-check" action="/cabinet/client/partner-reg-complete">
                                    <div class="title">
                                        <h3>Перепроверьте свои данные</h3>
                                        <p>После нажатия кнопки «Сохранить» договор будет отправлен на проверку</p>
                                    </div>
                                    <fieldset class="data">
                                        <h4>Данные <a href="/cabinet/client/partner-reg-confirm">Изменить</a></h4>
                                        <div class="row">
                                            <div class="col-12"><span>Наименование организации (сокращенное):</span>
                                                <p>{{ $form['company_name_short'] }}</p>
                                            </div>
                                            <div class="col-12"><span>Наименование организации (полное):</span>
                                                <p>{{ $form['company_name_full'] }}</p>
                                            </div>
                                            <div class="col-12"><span>ОГРН:</span>
                                                <p>{{ $form['ogrn'] }}</p>
                                            </div>
                                            <div class="col-6"><span>Должность:</span>
                                                <p>{{ $form['position'] }}</p>
                                            </div>
                                            <div class="col-6"><span>Действует на основании:</span>
                                                <p>{{ $form['foundation'] }}</p>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset class="data"><strong>Контактное лицо</strong>
                                        <div class="row">
                                            <div class="col-12"><span>ФИО:</span>
                                                <p>{{ $form['partner_fio'] }}</p>
                                            </div>
                                            <div class="col-12"><span>Телефон:</span>
                                                <p>{{ $form['partner_phone'] }}</p>
                                            </div>
                                            <div class="col-12"><span>E-mail:</span>
                                                <p>{{ $form['partner_email'] }}</p>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <hr>
                                    <fieldset class="data">
                                        <h4>Профиль <a href="/cabinet/client/partner-reg-profile">Изменить</a></h4>
                                        <div class="row">
                                            <div class="col-12"><span>ФИО:</span>
                                                <p>{{ $form['partner_fio'] }}</p>
                                            </div>
                                            <div class="col-12"><span>Телефон:</span>
                                                <p>{{ $form['partner_phone'] }}</p>
                                            </div>
                                            <div class="col-12"><span>E-mail:</span>
                                                <p>{{ $form['partner_email'] }}</p>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <hr>
                                    <fieldset class="data">
                                        <h4>Документы <a href="/cabinet/client/partner-reg-doc">Изменить</a></h4>
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="d-flex flex-column">
                                                    <a class="doc" href="{{ url($form['file_charter']) }}">Устав организации</a>
                                                    <a class="doc" href="{{ url($form['file_reg_cert']) }}">Свидетельство о регистрации</a>
                                                    <a class="doc" href="{{ url($form['file_decision_create']) }}">Устав организации</a>
                                                    <a class="doc" href="{{ url($form['file_order_head']) }}">Свидетельство о регистрации</a>
                                                    <a class="doc" href="{{ url($form['file_order_accountant']) }}">Устав организации</a>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <a href="javascript:void(0);" class="btn btn-primary js-btn-send-form" data-formid="partner-reg-check">СОХРАНИТЬ</a>
                                </form>
                            </div>
                        </div>
                    </div><div id="mCSB_1_scrollbar_vertical" class="mCSB_scrollTools mCSB_1_scrollbar mCS-light mCSB_scrollTools_vertical" style="display: none;"><div class="mCSB_draggerContainer"><div id="mCSB_1_dragger_vertical" class="mCSB_dragger" style="position: absolute; min-height: 30px; height: 0px; top: 0px;"><div class="mCSB_dragger_bar" style="line-height: 30px;"></div></div><div class="mCSB_draggerRail"></div></div></div></div></div>
        </div>
        <div class="col-3">
            @include('cabinets/client/chat/chat')
        </div>
    </div>
</div>

@include('cabinets/client/footer')

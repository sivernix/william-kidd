@php
    $userIdCode = '';
    $user = auth()->user();
    if ($user->id) {
        $userIdCode = \App\Helpers\IntToShortCode::encode($user->id) . '_client';
    }

    $CHAT_SOCKETIO_URL = env('CHAT_SOCKETIO_URL');
@endphp

<div class="lk__wrapper-chat">
    <div class="lk__chat-body">
        <div class="js-scroller">
            <div class="js-message-list">

            </div>
        </div>
    </div>
    <form class="lk__chat-form" id="lk__chat-form">
        <a style="display: none;" class="attach" href="#"><img src="{{ asset('assets/images/attach.svg') }}"></a>
        <input type="hidden" id="ucode" value="{{$userIdCode}}">
        <textarea id="lk__chat-input" placeholder="Напишите ваш комментарий..."></textarea>
        <a class="send js-lk__chat-send" href="#"><img
                src="{{ asset('assets/images/send.svg') }}"></a>
    </form>
</div>

<script src="{{$CHAT_SOCKETIO_URL}}/socket.io/socket.io.js"></script>

<script>
    var socket;
    var form;
    var input;
    var user = document.getElementById('ucode').value;
    $(() => {
        socket = new io.connect('{{$CHAT_SOCKETIO_URL}}', { query: 'ucode=' + user });

        socket.on('ping', function(data){
            socket.emit('pong', {beat: 1});
        });

        socket.on('chat message', (data) => {
            console.log('message: ' + data.message);
            console.log('socketId: ' + data.socketId);
            console.log(socket.id == data.socketId);

            let isMyMess = socket.id == data.socketId;

            let html_mess = '<div class="bubble ' + (isMyMess ? 'bubble__user' : 'bubble__admin') + '"><p>' + data.message + '</p><span>' + hour + ':' + minutes + '</span></div>';
            $('.js-message-list').append(html_mess);
        });


        form = document.getElementById('lk__chat-form');
        input = document.getElementById('lk__chat-input');
        let date = new Date;

        let seconds = date.getSeconds();
        let minutes = date.getMinutes();
        let hour = date.getHours();

        $(form).on('submit', (e) => {
            e.preventDefault();
            if (input.value) {
                socket.emit('chat message', {message: input.value, curChat: user, socketId: socket.id, ucode: user});
                input.value = '';
            }
        });

        $('.js-lk__chat-send').on('click', (e) => {
            e.preventDefault();
            $(form).trigger('submit');
        });
    })
</script>

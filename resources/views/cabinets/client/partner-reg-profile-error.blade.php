@include('cabinets/client/header')

<div class="container-fluid">
    <div class="row">
        <div class="col-9 lk__wrapper-col">
            <div class="lk__wrapper-title">
                <h1>Регистрация нового партнера</h1>
                <p>Для осуществления заказов в нашей Системе пройдите все шаги регистрации</p>
            </div>
            <div class="lk__wrapper-error">
                <p>Не соответствует БИК банка</p><a href="#"><img src="assets/images/times.svg"></a>
            </div>
            <div class="lk__wrapper-wrapper js-scroller mCustomScrollbar _mCS_1"><div id="mCSB_1" class="mCustomScrollBox mCS-light mCSB_vertical mCSB_inside" style="max-height: none;" tabindex="0"><div id="mCSB_1_container" class="mCSB_container mCS_y_hidden mCS_no_scrollbar_y" style="position:relative; top:0; left:0;" dir="ltr">
                        <div class="row">
                            <div class="col-3">
                                <div class="tline">
                                    <div class="tline-item done error"><span>1</span>
                                        <p>Подтверждение данных</p>
                                    </div>
                                    <div class="tline-item active error"><span>2</span>
                                        <p>Заполнение профиля</p>
                                    </div>
                                    <div class="tline-item"><span>3</span>
                                        <p>Загрузка документов</p>
                                    </div>
                                    <div class="tline-item"><span>4</span>
                                        <p>Проверка данных</p>
                                    </div>
                                    <div class="tline-item"><span>5</span>
                                        <p>Завершено</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-7">
                                <form>
                                    <div class="title">
                                        <h3>Заполните данные профиля</h3>
                                    </div>
                                    <fieldset>
                                        <div class="row">
                                            <div class="col-6">
                                                <label><span>ОКПО</span>
                                                    <input type="text">
                                                </label>
                                            </div>
                                            <div class="col-6">
                                                <label><span>Среднесписочная численность</span>
                                                    <input type="text">
                                                </label>
                                            </div>
                                            <div class="col-12">
                                                <label><span>Фактический адрес</span>
                                                    <input type="text">
                                                </label>
                                                <label class="checkbox">
                                                    <input type="checkbox">
                                                    <p>Совпадает с юридическим</p>
                                                </label>
                                            </div>
                                            <div class="col-12">
                                                <label><span>Почтовый адрес</span>
                                                    <input type="text">
                                                </label>
                                                <label class="checkbox">
                                                    <input type="checkbox">
                                                    <p>Совпадает с юридическим</p>
                                                </label>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset><strong>Подписант 1</strong>
                                        <div class="row">
                                            <div class="col-12">
                                                <label><span>ФИО</span>
                                                    <input type="text">
                                                </label>
                                            </div>
                                            <div class="col-6">
                                                <label><span>Должность</span>
                                                    <input type="text">
                                                </label>
                                            </div>
                                            <div class="col-6">
                                                <label><span>Действует на основании</span>
                                                    <input type="text">
                                                </label>
                                            </div>
                                            <div class="col-12">
                                                <label class="checkbox">
                                                    <input type="checkbox">
                                                    <p>Единственный подписант</p>
                                                </label>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset><strong>Подписант 2</strong>
                                        <div class="row">
                                            <div class="col-12">
                                                <label><span>ФИО</span>
                                                    <input type="text">
                                                </label>
                                            </div>
                                            <div class="col-6">
                                                <label><span>Должность</span>
                                                    <input type="text">
                                                </label>
                                            </div>
                                            <div class="col-6">
                                                <label><span>Действует на основании</span>
                                                    <input type="text">
                                                </label>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset><strong>Банковские реквизиты</strong>
                                        <div class="row">
                                            <div class="col-6">
                                                <label><span>БИК Банка</span>
                                                    <input type="text">
                                                </label>
                                            </div>
                                            <div class="col-6">
                                                <label><span>Номер расчетного счета</span>
                                                    <input type="text">
                                                </label>
                                            </div>
                                            <div class="col-12">
                                                <label><span>ПАО Банк, 30101810400000000225</span></label>
                                            </div>
                                        </div>
                                    </fieldset><a class="btn btn-primary" href="#">сохранить</a>
                                </form>
                            </div>
                        </div>
                    </div><div id="mCSB_1_scrollbar_vertical" class="mCSB_scrollTools mCSB_1_scrollbar mCS-light mCSB_scrollTools_vertical" style="display: none;"><div class="mCSB_draggerContainer"><div id="mCSB_1_dragger_vertical" class="mCSB_dragger" style="position: absolute; min-height: 30px; height: 0px; top: 0px;"><div class="mCSB_dragger_bar" style="line-height: 30px;"></div></div><div class="mCSB_draggerRail"></div></div></div></div></div>
        </div>
        <div class="col-3">
            @include('cabinets/client/chat/chat')
        </div>
    </div>
</div>

@include('cabinets/client/footer')

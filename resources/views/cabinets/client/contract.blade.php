@include('cabinets/client/header')

<div class="container-fluid">
    <div class="row align-items-center">
        <div class="col-3">
            <div class="header__page">
                <h1>Договоры</h1><a class="excel" href="#" data-toggle="modal" data-target="contract">Экспорт в Excel</a>
            </div>
        </div>
        <div class="col-9">
            <div class="header__page">
                <input class="search" type="text" placeholder="Поиск по наименованию покупателя или тексту события"><a class="filter visible js-filter" href="#"><img src="/cabinets/operator/assets/images/filter.svg" /> Фильтр</a>
                <div class="filter__wrapper">
                    <h3>Фильтр</h3>
                    <form class="filter__wrapper-form">
                        <fieldset>
                            <label>Показать за период</label>
                            <input type="date">
                        </fieldset>
                        <fieldset>
                            <label>Раздел</label>
                            <select>
                                <option>Показать все</option>
                            </select>
                        </fieldset>
                        <fieldset>
                            <label>ПМ</label>
                            <select>
                                <option>Показать все</option>
                            </select>
                        </fieldset><a class="btn btn-primary" href="#">Применить</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-3 pr-0">
            <div class="sidebar js-scroller"><a class="sidebar__item" href="#">
                    <p>Наименование организации</p><span>№ 123 от 12.01.2020</span><span class="status wait">в обработке</span></a><a class="sidebar__item active" href="#">
                    <p>Наименование организации</p><span>№ 123 от 12.01.2020</span><span class="status acting">Действующий</span></a><a class="sidebar__item" href="#">
                    <p>Наименование организации</p><span>№ 123 от 12.01.2020</span><span class="status specified">уточняется</span></a><a class="sidebar__item" href="#">
                    <p>Наименование организации</p><span>№ 123 от 12.01.2020</span><span class="status expired">истек</span></a><a class="sidebar__item" href="#">
                    <p>Наименование организации</p><span>№ 123 от 12.01.2020</span><span class="status refusal">отказ</span></a><a class="sidebar__item" href="#">
                    <p>Наименование организации</p><span>№ 123 от 12.01.2020</span><span class="status canceled">отменен</span></a></div>
        </div>
        <div class="col-9 pl-0">
            <div class="body__page js-scroller">
                <div class="body__page-title align-items-start">
                    <div>
                        <h3>Наименование организации</h3><span>ИНН: 77123456789012</span>
                    </div>
                    <ul>
                        <li><a href="#">Чат</a></li>
                        <li><a href="/cabinet/operator/order">Заказы</a></li>
                        <li><a href="/cabinet/operator/documents">Документы</a></li>
                    </ul>
                </div>
                <div class="body__page-container">
                    <div class="body__page-form">
                        <div class="body__page-header"><strong>Договор № 12345 от 27.02.2019</strong>
                            <div>
                                <div class="status acting">Действующий</div><span>с 27.02.2019 по 26.02.2020</span>
                            </div><a href="#">Оригинал договора.pgf</a>
                        </div>
                        <div class="fieldset">
                            <h3>Реквизиты</h3>
                            <div class="row">
                                <div class="col-5">
                                    <div class="block"><span>Наименование организации</span>
                                        <p>ООО Организация покупателя</p>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="block"><span>Расчетный счет № 1234567890123456789</span>
                                        <p>123456789, ПАО Банк, 30101810400000000336</p>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="block"><span>ОГРН</span>
                                        <p>1234567890123456</p>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="block"><span>ОКПО</span>
                                        <p>1234567890</p>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="block"><span>КПП</span>
                                        <p>123456789</p>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="block"><span>Фактический адрес</span>
                                        <p>111141, г. Москва, ул. Плеханова, д 13 стр 6</p>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="block"><span>Дата регистрации</span>
                                        <p>13.01.2020</p>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="block"><span>Почтовый адрес</span>
                                        <p>111141, г. Москва, ул. Плеханова, д 13 стр 6</p>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="block"><span>Юридический адрес:</span>
                                        <p>111141, г. Москва, ул. Плеханова, д 13 стр 6</p>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="block"><span>Среднесписочная численность:</span>
                                        <p>500</p>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="block"><span>Генеральный директор</span>
                                        <p>Фамилия Имя Отчество</p>
                                    </div>
                                </div>
                                <div class="col-5"></div>
                                <div class="col-5">
                                    <div class="block"><strong>Подписант 1</strong><span>ФИО</span>
                                        <p>Богатырева Июстина Леонидовна</p>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="block"><strong>Подписант 2</strong><span>ФИО</span>
                                        <p>Волощук Абдуллагаджи Дмитриевич</p>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="block"><span>Должность</span>
                                        <p>Генеральный директор</p>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="block"><span>Должность</span>
                                        <p>Главный бухгалтер</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="fieldset">
                            <h3 class="mb-0">Прикрепленные документы</h3>
                            <div class="row">
                                <div class="col-6">
                                    <div class="file">
                                        <p><a href="#">Свидетельство о регистрации</a></p>
                                    </div>
                                    <div class="file">
                                        <p><a href="#">Решение о создании / Протокол собрания</a></p>
                                    </div>
                                    <div class="file">
                                        <p><a href="#">Приказ о назначении руководителя</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="fieldset">
                            <div class="row">
                                <div class="col-12 ml"><a class="btn btn-primary" href="#">Изменить данные</a><a class="btn btn-secondary" href="#" data-toggle="modal" data-target="closed">Аннулировать договор</a></div>
                            </div>
                        </div>
                        <div class="body__page-spoiler">
                            <div class="title">
                                <h3>История событий</h3>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="history__item"><span>22.01.2019 09:55</span>
                                        <p>Фамилия Имя Отчество</p>
                                        <p>Договор отправлен на обработку</p>
                                    </div>
                                    <div class="history__item"><span>22.01.2019 09:55</span>
                                        <p>Наименование организации</p>
                                        <p>Требуются уточнения: В скан-копии брак сканирования на странице 15</p>
                                    </div>
                                    <div class="history__item"><span>22.01.2019 09:55</span>
                                        <p>Фамилия Имя Отчество</p>
                                        <p>Договор отправлен на обработку</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('cabinets/client/footer')

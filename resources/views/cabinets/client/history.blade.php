@include('cabinets/client/header')
<div class="container-fluid">
    <div class="row refactor-col">
        <div class="col-9">
            <div class="header__page">
                <h1>История собыий</h1>
                <input class="search" type="text" placeholder="Поиск по наименованию покупателя или тексту события"><a class="filter js-filter" href="#"><img src="assets/images/filter.svg" /> Фильтр</a>
                <div class="filter__wrapper">
                    <h3>Фильтр</h3>
                    <form class="filter__wrapper-form">
                        <fieldset>
                            <label>Показать за период</label>
                            <input type="date">
                        </fieldset>
                        <fieldset>
                            <label>Раздел</label>
                            <select>
                                <option>Показать все</option>
                            </select>
                        </fieldset>
                        <fieldset>
                            <label>ПМ</label>
                            <select>
                                <option>Показать все</option>
                            </select>
                        </fieldset><a class="btn btn-primary" href="#">Применить</a>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-9">
            <div class="table__wrapper">
                <div class="table__wrapper-container">
                    <table>
                        <thead>
                        <tr>
                            <th>Дата и время</th>
                            <th>Раздел</th>
                            <th>Клиент</th>
                            <th>Событие</th>
                            <th>ПМ</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td><span class="new"></span> 24.01.2020 16:25</td>
                            <td><b>Договоры</b></td>
                            <td>ООО Наименование</td>
                            <td><b>Договор №- поступил на обработку</b></td>
                            <td>Фамилия Имя Отчество</td>
                        </tr>
                        <tr>
                            <td><span class="new"></span> 24.01.2020 16:25</td>
                            <td><b>Заказы</b></td>
                            <td>ООО Наименование</td>
                            <td><b>Заказ № 1234 подписан покупателем</b></td>
                            <td>Фамилия Имя Отчество</td>
                        </tr>
                        <tr>
                            <td><span class="new"></span> 24.01.2020 16:25</td>
                            <td><b>Чаты</b></td>
                            <td>ООО Наименование</td>
                            <td><b>Новое сообщение в чате</b></td>
                            <td>Фамилия Имя Отчество</td>
                        </tr>
                        <tr>
                            <td>24.01.2020 16:25</td>
                            <td><b>Заказы</b></td>
                            <td>ООО Наименование</td>
                            <td><b>Заказ № 1234 подписан покупателем</b></td>
                            <td>Фамилия Имя Отчество</td>
                        </tr>
                        <tr>
                            <td>24.01.2020 16:25</td>
                            <td><b>Договоры</b></td>
                            <td>ООО Наименование</td>
                            <td><b>Договор №- поступил на обработку</b></td>
                            <td>Фамилия Имя Отчество</td>
                        </tr>
                        <tr>
                            <td>24.01.2020 16:25</td>
                            <td><b>Договоры</b></td>
                            <td>ООО Наименование</td>
                            <td><b>Договор №- поступил на обработку</b></td>
                            <td>Фамилия Имя Отчество</td>
                        </tr>
                        <tr>
                            <td>24.01.2020 16:25</td>
                            <td><b>Заказы</b></td>
                            <td>ООО Наименование</td>
                            <td><b>Заказ № 1234 подписан покупателем</b></td>
                            <td>Фамилия Имя Отчество</td>
                        </tr>
                        <tr>
                            <td>24.01.2020 16:25</td>
                            <td><b>Чаты</b></td>
                            <td>ООО Наименование</td>
                            <td><b>Новое сообщение в чате</b></td>
                            <td>Фамилия Имя Отчество</td>
                        </tr>
                        <tr>
                            <td>24.01.2020 16:25</td>
                            <td><b>Договоры</b></td>
                            <td>ООО Наименование</td>
                            <td><b>Договор №- поступил на обработку</b></td>
                            <td>Фамилия Имя Отчество</td>
                        </tr>
                        <tr>
                            <td>24.01.2020 16:25</td>
                            <td><b>Договоры</b></td>
                            <td>ООО Наименование</td>
                            <td><b>Договор №- поступил на обработку</b></td>
                            <td>Фамилия Имя Отчество</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="table__wrapper-footer">
                    <div class="table__wrapper-pagination"><a class="prev" href="#"><img src="assets/images/prev.svg"></a>
                        <ul>
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a class="active" href="#">3</a></li>
                            <li><span>...</span></li>
                            <li><a href="#">24</a></li>
                        </ul><a class="next" href="#"><img src="assets/images/next.svg"></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-3">
            <div class="filter__wrapper">
                <h3>Фильтр</h3>
                <form class="filter__wrapper-form">
                    <fieldset>
                        <label>Показать за период</label>
                        <input type="date">
                    </fieldset>
                    <fieldset>
                        <label>Раздел</label>
                        <select>
                            <option>Показать все</option>
                        </select>
                    </fieldset>
                    <fieldset>
                        <label>ПМ</label>
                        <select>
                            <option>Показать все</option>
                        </select>
                    </fieldset><a class="btn btn-primary" href="#">Применить</a>
                </form>
            </div>
        </div>
    </div>
</div>
@include('cabinets/client/footer')

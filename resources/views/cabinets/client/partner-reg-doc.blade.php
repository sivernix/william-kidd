@include('cabinets/client/header')

<div class="container-fluid">
    <div class="row">

        <div class="col-9 lk__wrapper-col">
            <div class="lk__wrapper-title">
                <h1>Регистрация нового партнера</h1>
                <p>Для осуществления заказов в нашей Системе пройдите все шаги регистрации</p>
            </div>
            <div class="lk__wrapper-wrapper js-scroller mCustomScrollbar _mCS_1">
                <div id="mCSB_1" class="mCustomScrollBox mCS-light mCSB_vertical mCSB_inside" style="max-height: 580px;"
                     tabindex="0">
                    <div id="mCSB_1_container" class="mCSB_container mCS_y_hidden mCS_no_scrollbar_y"
                         style="position:relative; top:0; left:0;" dir="ltr">
                        <div class="row">
                            <div class="col-3">
                                <div class="tline">
                                    <div class="tline-item done"><span>1</span>
                                        <p>Подтверждение данных</p>
                                    </div>
                                    <div class="tline-item done"><span>2</span>
                                        <p>Заполнение профиля</p>
                                    </div>
                                    <div class="tline-item active"><span>3</span>
                                        <p>Загрузка документов</p>
                                    </div>
                                    <div class="tline-item"><span>4</span>
                                        <p>Проверка данных</p>
                                    </div>
                                    <div class="tline-item"><span>5</span>
                                        <p>Завершено</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-7">
                                <form enctype="multipart/form-data" id="partner-reg-doc" data-validate="partner-reg-doc"
                                      action="/cabinet/client/partner-reg-doc" method="post">
                                    <div class="title">
                                        <h3>Загрузите документы из списка:</h3>
                                    </div>
                                    <div class="list__doc">
                                        @if($form['file_charter'])
                                        <div class="list__doc-item loaded">
                                            <p>Устав организации</p>
                                            <a class="js-file_reset" href="#"><img src="{{ asset('assets/images/times.svg') }}" class="mCS_img_loaded"></a>
                                            <label style="display: none;">
                                                <input type="file" name="file_charter"><span>Загрузить</span>
                                            </label>
                                        </div>
                                        @else
                                        <div class="list__doc-item">
                                            <p>Устав организации</p>
                                            <label>
                                                <input type="file" name="file_charter"><span>Загрузить</span>
                                            </label>
                                        </div>
                                        @endif

                                        @if($form['file_reg_cert'])
                                            <div class="list__doc-item loaded">
                                                <p>Свидетельство о регистрации</p><a class="js-file_reset" href="#"><img
                                                        src="{{ asset('assets/images/times.svg') }}"
                                                        class="mCS_img_loaded"></a>
                                                <label style="display: none;">
                                                    <input type="file" name="file_charter"><span>Загрузить</span>
                                                </label>
                                            </div>
                                        @else
                                            <div class="list__doc-item">
                                                <p>Свидетельство о регистрации</p>
                                                <label>
                                                    <input type="file" name="file_reg_cert"><span>Загрузить</span>
                                                </label>
                                            </div>
                                        @endif

                                        @if($form['file_decision_create'])
                                            <div class="list__doc-item loaded">
                                                <p>Решение о создании / Протокол собрания</p><a class="js-file_reset" href="#"><img
                                                        src="{{ asset('assets/images/times.svg') }}"
                                                        class="mCS_img_loaded"></a>
                                                <label style="display: none;">
                                                    <input type="file" name="file_charter"><span>Загрузить</span>
                                                </label>
                                            </div>
                                        @else
                                            <div class="list__doc-item">
                                                <p>Решение о создании / Протокол собрания</p>
                                                <label>
                                                    <input type="file" name="file_decision_create"><span>Загрузить</span>
                                                </label>
                                            </div>
                                        @endif

                                        @if($form['file_order_head'])
                                            <div class="list__doc-item loaded">
                                                <p>Приказ о назначении руководителя</p><a class="js-file_reset" href="#"><img
                                                        src="{{ asset('assets/images/times.svg') }}"
                                                        class="mCS_img_loaded"></a>
                                                <label style="display: none;">
                                                    <input type="file" name="file_charter"><span>Загрузить</span>
                                                </label>
                                            </div>
                                        @else
                                                <div class="list__doc-item">
                                                    <p>Приказ о назначении руководителя</p>
                                                    <label>
                                                        <input type="file" name="file_order_head"><span>Загрузить</span>
                                                    </label>
                                                </div>
                                        @endif

                                        @if($form['file_order_accountant'])
                                            <div class="list__doc-item loaded">
                                                <p>Приказ о назначении главного бухгалтера</p><a class="js-file_reset" href="#"><img
                                                        src="{{ asset('assets/images/times.svg') }}"
                                                        class="mCS_img_loaded"></a>
                                                <label style="display: none;">
                                                    <input type="file" name="file_charter"><span>Загрузить</span>
                                                </label>
                                            </div>
                                        @else
                                                <div class="list__doc-item">
                                                    <p>Приказ о назначении главного бухгалтера</p>
                                                    <label>
                                                        <input type="file" name="file_order_accountant"><span>Загрузить</span>
                                                    </label>
                                                </div>
                                        @endif
                                    </div>
                                    @csrf
                                    <input type="hidden" name="form" value="partner-reg-doc">
                                    <a href="javascript:void(0);" class="btn btn-primary js-btn-send-form"
                                       data-formid="partner-reg-doc">СОХРАНИТЬ</a>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div id="mCSB_1_scrollbar_vertical"
                         class="mCSB_scrollTools mCSB_1_scrollbar mCS-light mCSB_scrollTools_vertical"
                         style="display: none;">
                        <div class="mCSB_draggerContainer">
                            <div id="mCSB_1_dragger_vertical" class="mCSB_dragger"
                                 style="position: absolute; min-height: 30px; height: 0px; top: 0px;">
                                <div class="mCSB_dragger_bar" style="line-height: 30px;"></div>
                            </div>
                            <div class="mCSB_draggerRail"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-3">
            @include('cabinets/client/chat/chat')
        </div>
    </div>
</div>

@include('cabinets/client/footer')

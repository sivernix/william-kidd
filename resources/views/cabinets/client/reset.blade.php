@include('header')
<div class="body__wrapper body__wrapper-login">
    <div class="body__wrapper-image">
        <h3>С «William Kidd» удобно работать:</h3>
        <ul>
            <li>Заключайте договоры поставки прямо в <b>Личном кабинете</b></li>
            <li>Следите за всеми этапами доставки</li>
            <li>С любыми вопросами обращайтесь к менеджеру в&nbsp;персональном чате</li>
        </ul>
    </div>
    <div class="body__wrapper-form">
        @if(empty(app('request')->input('step')))
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xl-8 col-10 mx-auto">
                        <h1>Восстановление пароля</h1>
                        <div class="register">
                            <p>Введите данные вашей учётной записи, и&nbsp;мы&nbsp;отправим вам&nbsp;ссылку для&nbsp;восстановления доступа к&nbsp;личному кабинету</p>
                        </div>
                        <form class="form">
                            <fieldset>
                                <div class="form__label">
                                    <label>ИНН организации / ИП</label>
                                </div>
                                <input type="text">
                            </fieldset>
                            <fieldset>
                                <div class="form__label">
                                    <label>E-mail</label>
                                </div>
                                <input type="text">
                            </fieldset><a class="btn btn-primary js__btn-submit" href="#">Восстановить</a>
                            <div class="btn__wrapper d-flex justify-content-between"><a href="/login">Войти</a><a href="/signup">Зарегистрироваться</a></div>
                        </form>
                    </div>
                </div>
            </div>
        @endif

        @if(app('request')->input('step') == 1)
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xl-8 col-10 mx-auto">
                        <h1>Восстановление пароля</h1>
                        <form class="form">
                            <div class="row">
                                <div class="col-5"><span>ИНН организации / ИП</span>
                                    <p>123467890</p>
                                </div>
                                <div class="col-7"><span>E-mail</span>
                                    <p>jonnkidd@mail.ru <a href="#">Изменить E-mail</a></p>
                                </div>
                            </div>
                            <p>Не получили письмо? <br>Проверьте папку «Спам» или повторите отправку.</p><a class="btn btn-primary js__btn-submit" href="#">Повторить через 04:59</a>
                            <div class="btn__wrapper d-flex justify-content-between"><a href="/login">Войти</a><a href="/signup">Зарегистрироваться</a></div>
                        </form>
                    </div>
                </div>
            </div>
        @endif

        @if(app('request')->input('step') == 2)
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xl-8 col-10 mx-auto">
                        <h1>Восстановление пароля</h1>
                        <div class="register">
                            <p>Придумайте пароль для учетной записи:</p>
                        </div>
                        <form class="form">
                            <fieldset>
                                <div class="form__label">
                                    <label>Новый пароль</label>
                                </div>
                                <input type="password">
                                <div class="form__label-pwd js-pwd hide"></div><span>Минимум 6 символов</span>
                            </fieldset>
                            <fieldset>
                                <div class="form__label">
                                    <label>Повторите пароль:</label>
                                </div>
                                <input type="password">
                                <div class="form__label-pwd js-pwd hide"></div>
                            </fieldset>
                            <a class="btn btn-primary js__btn-submit" href="#">Сохранить</a>
                        </form>
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>

@include('footer')

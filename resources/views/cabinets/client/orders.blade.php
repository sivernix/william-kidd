@include('cabinets/client/header')
<div class="container-fluid">
    <div class="row">

        @if(false)
        <div class="col-9 lk__wrapper-col">
            <div class="lk__wrapper-title">
                <h1 class="mb-4">Ваши заказы</h1><a class="btn btn-new" href="#" data-toggle="modal" data-target="order"><img src="{{ asset('assets/images/c-plus.svg') }}"> Новый заказ</a>
            </div>
            <div class="lk__wrapper-table">
                <form>
                    <input type="text" placeholder="Поиск">

                    <select>
                        <option value="Сначала новые">Сначала новые</option>
                        <option value="Сначала старые">Сначала старые</option>
                    </select>
                </form>
                <table>
                    <thead>
                    <tr>
                        <th>Заказ</th>
                        <th>Вид топлива</th>
                        <th>Кол-во лотов:</th>
                        <th>Стоимость</th>
                        <th>Станция</th>
                        <th>Статус</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td> <b>№ —</b><br><span>от 27.02.2020</span></td>
                        <td>бензин (АИ-100-К5) СТО 78689379-32- 2018689900</td>
                        <td>2 (120 тонн)</td>
                        <td>2 000 000 ₽</td>
                        <td><span>Гуково, Северо-Кавказская ЖД</span></td>
                        <td>
                            <div class="status draft">Черновик</div>
                        </td>
                    </tr>
                    <tr>
                        <td> <b>№ —</b><br><span>от 27.02.2020</span></td>
                        <td>бензин (АИ-100-К5) СТО 78689379-32- 2018689900</td>
                        <td>2 (120 тонн)</td>
                        <td>2 000 000 ₽</td>
                        <td><span>Гуково, Северо-Кавказская ЖД</span></td>
                        <td>
                            <div class="status progressed">В обработке</div>
                        </td>
                    </tr>
                    <tr>
                        <td> <b>№ —</b><br><span>от 27.02.2020</span></td>
                        <td>бензин (АИ-100-К5) СТО 78689379-32- 2018689900</td>
                        <td>2 (120 тонн)</td>
                        <td>2 000 000 ₽</td>
                        <td><span>Гуково, Северо-Кавказская ЖД</span></td>
                        <td>
                            <div class="status returned">Возвращен на доработку</div>
                        </td>
                    </tr>
                    <tr>
                        <td> <b>№ —</b><br><span>от 27.02.2020</span></td>
                        <td>бензин (АИ-100-К5) СТО 78689379-32- 2018689900</td>
                        <td>2 (120 тонн)</td>
                        <td>2 000 000 ₽</td>
                        <td><span>Гуково, Северо-Кавказская ЖД</span></td>
                        <td>
                            <div class="status wait">Ожидает оплаты</div>
                        </td>
                    </tr>
                    <tr>
                        <td> <b>№ —</b><br><span>от 27.02.2020</span></td>
                        <td>бензин (АИ-100-К5) СТО 78689379-32- 2018689900</td>
                        <td>2 (120 тонн)</td>
                        <td>2 000 000 ₽</td>
                        <td><span>Гуково, Северо-Кавказская ЖД</span></td>
                        <td>
                            <div class="status indone">Отгружен 4 из 5</div>
                        </td>
                    </tr>
                    <tr>
                        <td> <b>№ —</b><br><span>от 27.02.2020</span></td>
                        <td>бензин (АИ-100-К5) СТО 78689379-32- 2018689900</td>
                        <td>2 (120 тонн)</td>
                        <td>2 000 000 ₽</td>
                        <td><span>Гуково, Северо-Кавказская ЖД</span></td>
                        <td>
                            <div class="status complete">Доставлен</div>
                        </td>
                    </tr>
                    <tr>
                        <td> <b>№ —</b><br><span>от 27.02.2020</span></td>
                        <td>бензин (АИ-100-К5) СТО 78689379-32- 2018689900</td>
                        <td>2 (120 тонн)</td>
                        <td>2 000 000 ₽</td>
                        <td><span>Гуково, Северо-Кавказская ЖД</span></td>
                        <td>
                            <div class="status canceled">Отменен</div>
                        </td>
                    </tr>
                    <tr>
                        <td> <b>№ —</b><br><span>от 27.02.2020</span></td>
                        <td>бензин (АИ-100-К5) СТО 78689379-32- 2018689900</td>
                        <td>2 (120 тонн)</td>
                        <td>2 000 000 ₽</td>
                        <td><span>Гуково, Северо-Кавказская ЖД</span></td>
                        <td>
                            <div class="status done">Подтвержден</div>
                        </td>
                    </tr>
                    <tr>
                        <td> <b>№ —</b><br><span>от 27.02.2020</span></td>
                        <td>бензин (АИ-100-К5) СТО 78689379-32- 2018689900</td>
                        <td>2 (120 тонн)</td>
                        <td>2 000 000 ₽</td>
                        <td><span>Гуково, Северо-Кавказская ЖД</span></td>
                        <td>
                            <div class="status indone">Отгружен 4 из 5</div>
                        </td>
                    </tr>
                    <tr>
                        <td> <b>№ —</b><br><span>от 27.02.2020</span></td>
                        <td>бензин (АИ-100-К5) СТО 78689379-32- 2018689900</td>
                        <td>2 (120 тонн)</td>
                        <td>2 000 000 ₽</td>
                        <td><span>Гуково, Северо-Кавказская ЖД</span></td>
                        <td>
                            <div class="status complete">Доставлен</div>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <div class="col-12 text-center">
                    <div class="paginaton mb-5"><a class="prev" href="#">Предыдущая</a>
                        <ul>
                            <li><a class="active" href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><span>...</span></li>
                            <li><a href="#">22</a></li>
                        </ul><a class="next" href="#">Следующая</a>
                    </div>
                </div>
            </div>
        </div>
        @else
            <div class="col-9 lk__wrapper-col">
                <div class="row">
                    <div class="col-3"><img class="cover" src="{{ asset('assets/images/lk-order.jpg') }}"></div>
                    <div class="col-8 ml-auto">
                        <div class="entry order-empty">
                            <h1>Ваши заказы</h1><strong>Вы еще не оформили ни одного заказа</strong>
                            <p>Для создания заказа нажмите кнопку «Новый заказ»</p><a class="btn btn-new" href="#" data-toggle="modal" data-target="order"><img src="{{ asset('assets/images/c-plus.svg') }}"> Новый заказ</a>
                        </div>
                    </div>
                </div>
            </div>
        @endif


        <div class="col-3">
            @include('cabinets/client/chat/chat')
        </div>
    </div>
</div>



<div class="modal" id="order">
    <div class="modal-overlay"></div>
    <div class="modal-body"><a class="close" href="#">&#215;</a>
        <div class="modal-title">
            <h2>Новый заказ</h2>
            <p>рассчитать стоимость с учетом доставки</p>
        </div>
        <div class="modal-form">
            <fieldset>
                <div class="row">
                    <div class="col-12">
                        <label><span>Вид топлива</span>
                            <select>
                                <option value="бензин (АИ-100-К5) СТО 44905015-2017">бензин (АИ-100-К5) СТО 44905015-2017</option>
                            </select>
                        </label>
                    </div>
                    <div class="col-6">
                        <label><span>Лотов</span>
                            <input type="text">
                        </label>
                    </div>
                    <div class="col-6">
                        <label><span>Тонн <i title="rghrsj"></i></span>
                            <input type="text">
                        </label>
                    </div>
                    <div class="col-12">
                        <label><span>Станция назначения</span>
                            <select>
                                <option value="Асино, Западно-Сибирская ЖД">Асино, Западно-Сибирская ЖД</option>
                            </select>
                        </label>
                    </div>
                    <div class="col-12">
                        <label><span>Период отгрузки</span>
                            <select>
                                <option value="до 30 дней">до 30 дней</option>
                            </select>
                        </label>
                    </div>
                </div>
            </fieldset>
            <fieldset><strong>Условия оплаты</strong>
                <div class="row">
                    <div class="col-6">
                        <label><span>Предоплата</span>
                            <select>
                                <option value="до 30 дней">до 30 дней</option>
                            </select>
                        </label>
                    </div>
                    <div class="col-6">
                        <label><span>Срок оплаты</span>
                            <select>
                                <option value="до 30 дней">до 30 дней</option>
                            </select>
                        </label>
                    </div>
                    <div class="col-12">
                        <label class="disabled"><span>Отсрочка</span>
                            <select>
                                <option value="до 30 дней">до 30 дней</option>
                            </select>
                        </label>
                    </div>
                    <div class="col-12">
                        <label>
                            <input type="checkbox">
                            <p>Оплата по факту прибытия</p>
                        </label>
                    </div>
                </div>
                <blockquote>В случае отсрочки платежа необходимо предоставление банковской гарантии. <a herf="#">Список банков-партнеров</a></blockquote><a class="btn btn-primary" href="#">+ перейти к оформлению</a>
            </fieldset>
        </div>
    </div>
</div>

@include('cabinets/client/footer')

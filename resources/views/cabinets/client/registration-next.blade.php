@include('header')
<div class="body__wrapper body__wrapper-login">
    <div class="body__wrapper-image">
        <h3>С «William Kidd» удобно работать:</h3>
        <ul>
            <li>Заключайте договоры поставки прямо в <b>Личном кабинете</b></li>
            <li>Следите за всеми этапами доставки</li>
            <li>С любыми вопросами обращайтесь к менеджеру в персональном чате</li>
        </ul>
    </div>
    <div class="body__wrapper-form">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-8 col-10 mx-auto">
                    <h1>Регистрация</h1>
                    <form class="form">
                        <div class="row">
                            <div class="col-5"><span>ИНН организации / ИП</span>
                                <p>{{ $inn }}</p>
                            </div>
                            <div class="col-7"><span>E-mail</span>
                                <p>{{ $email }}</p>
                            </div>
                        </div>
                        <p>Не получили письмо? <br/>Проверьте папку «Спам»</p>
{{--                        <fieldset class="mt-4">--}}
{{--                            <div class="form__label-check">--}}
{{--                                <label>--}}
{{--                                    <input type="checkbox">--}}
{{--                                    <p>Подтверждаю согласие c <a href="#">Пользовательским соглашением и Политикой конфиденциальности.</a></p>--}}
{{--                                </label>--}}
{{--                            </div>--}}
{{--                        </fieldset>--}}
                    </form>
                    <div class="problem"><strong>Не можете войти в ЛК? </strong>
                        <p>Если у вас уже заключен договор, но нет доступа в личный кабинет, обратитесь к своему персональному менеджеру или по общему телефону <a href="#">+7 (495) 120-73-37</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>

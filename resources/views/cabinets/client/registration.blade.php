@include('header')
<div class="body__wrapper body__wrapper-login">
    <div class="body__wrapper-image">
        <h3>С «William Kidd» удобно работать:</h3>
        <ul>
            <li>Заключайте договоры поставки прямо в <b>Личном кабинете</b></li>
            <li>Следите за всеми этапами доставки</li>
            <li>С любыми вопросами обращайтесь к менеджеру в персональном чате</li>
        </ul>
    </div>
    <div class="body__wrapper-form">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-8 col-10 mx-auto">
                    <h1>Регистрация</h1>
                    <form class="form" method="POST" action="{{ route('reg-one') }}">
                        @csrf
                        <fieldset>
                            <div class="form__label">
                                <label>ИНН организации / ИП</label>
                            </div>
                            <input type="text" name="inn">
                        </fieldset>
                        <fieldset>
                            <div class="form__label">
                                <label>E-mail</label>
                            </div>
                            <input type="text" name="email">
                        </fieldset><button class="btn btn-primary js__btn-submit" type="submit">Зарегистрироваться</button><a href="/cabinet/client/login">У меня уже заключен договор</a>
                        <fieldset>
                            <div class="form__label-check">
                                <label>
                                    <input type="checkbox" name="agree">
                                    <p>Подтверждаю согласие c <a href="/terms-of-use">Пользовательским соглашением</a> <a href="/privacy-policy">и Политикой конфиденциальности.</a></p>
                                </label>
                            </div>
                        </fieldset>
                    </form>
                    <div class="problem"><strong>Не можете войти в ЛК? </strong>
                        <p>Если у вас уже заключен договор, но нет доступа в личный кабинет, обратитесь к своему персональному менеджеру или по общему телефону <a href="#">+7 (495) 120-73-37</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>

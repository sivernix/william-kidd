@include('cabinets/client/header')

<div class="container-fluid">
    <div class="row">

        <div class="col-9 lk__wrapper-col">
            <div class="lk__wrapper-title">
                <h1>Регистрация нового партнера</h1>
                <p>Для осуществления заказов в нашей Системе пройдите все шаги регистрации</p>
            </div>
            <div class="lk__wrapper-wrapper js-scroller mCustomScrollbar _mCS_1"><div id="mCSB_1" class="mCustomScrollBox mCS-light mCSB_vertical mCSB_inside" style="max-height: none;" tabindex="0"><div id="mCSB_1_container" class="mCSB_container mCS_y_hidden mCS_no_scrollbar_y" style="position:relative; top:0; left:0;" dir="ltr">
                        <div class="row">
                            <div class="col-3">
                                <div class="tline">
                                    <div class="tline-item active"><span>1</span>
                                        <p>Подтверждение данных</p>
                                    </div>
                                    <div class="tline-item"><span>2</span>
                                        <p>Заполнение профиля</p>
                                    </div>
                                    <div class="tline-item"><span>3</span>
                                        <p>Загрузка документов</p>
                                    </div>
                                    <div class="tline-item"><span>4</span>
                                        <p>Проверка данных</p>
                                    </div>
                                    <div class="tline-item"><span>5</span>
                                        <p>Завершено</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-7">
                                <form id="partner-reg-confirm" data-validate="partner-reg-confirm" action="/cabinet/client/partner-reg-confirm" method="post">
                                    <div class="title">
                                        <h3>Подтвердите корректность данных</h3>
                                        <p>Мы получили данные из ЕГРЮЛ в соответствии с Вашим ИНН 7712345678</p>
                                    </div>
                                    <fieldset>
                                        <div class="row">
                                            <div class="col-12">
                                                <label><span>Наименование организации (сокращенное)</span>
                                                    <input type="text" name="company_name_short" value="{{ $form['company_name_short'] }}">
                                                </label>
                                            </div>
                                            <div class="col-12">
                                                <label><span>Наименование организации (полное)</span>
                                                    <input type="text" name="company_name_full" value="{{ $form['company_name_full'] }}">
                                                </label>
                                            </div>
                                            <div class="col-6">
                                                <label><span>ОГРН</span>
                                                    <input type="text" name="ogrn" value="{{ $form['ogrn'] }}">
                                                </label>
                                            </div>
                                            <div class="col-6">
                                                <label><span>КПП</span>
                                                    <input type="text" name="kpp" value="{{ $form['kpp'] }}">
                                                </label>
                                            </div>
                                            <div class="col-6">
                                                <label><span>Дата регистрации</span>
                                                    <input type="date" name="register_at" value="{{ $form['register_at'] }}">
                                                </label>
                                            </div>
                                            <div class="col-12">
                                                <label><span>Юридический адрес:</span>
                                                    <input type="text" name="ur_address" name="ogrn" value="{{ $form['ur_address'] }}">
                                                </label>
                                            </div>
                                            <div class="col-12">
                                                <label><span>ФИО единоличного исполнительгого органа</span>
                                                    <input type="text" name="main_fio" value="{{ $form['main_fio'] }}">
                                                </label>
                                            </div>
                                            <div class="col-6">
                                                <label><span>Должность</span>
                                                    <input type="text" name="position" value="{{ $form['position'] }}">
                                                </label>
                                            </div>
                                            <div class="col-6">
                                                <label><span>Действует на основании</span>
                                                    <input type="text" name="foundation" value="{{ $form['foundation'] }}">
                                                </label>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset><strong>контактное лицо</strong>
                                        <div class="row">
                                            <div class="col-12">
                                                <label><span>ФИО</span>
                                                    <input type="text" name="partner_fio" value="{{ $form['partner_fio'] }}">
                                                </label>
                                            </div>
                                            <div class="col-6">
                                                <label><span>Телефон</span>
                                                    <input type="text" name="partner_phone" value="{{ $form['partner_phone'] }}">
                                                </label>
                                            </div>
                                            <div class="col-6">
                                                <label><span>E-mail</span>
                                                    <input type="text" name="partner_email" value="{{ $form['partner_email'] }}">
                                                </label>
                                            </div>
                                        </div>
                                    </fieldset>
                                    @csrf
                                    <input type="hidden" name="form" value="partner-reg-confirm">
                                    <a href="javascript:void(0);" class="btn btn-primary js-btn-send-form" data-formid="partner-reg-confirm">ПОДТВЕРДИТЬ</a>
                                </form>
                            </div>
                        </div>
                    </div><div id="mCSB_1_scrollbar_vertical" class="mCSB_scrollTools mCSB_1_scrollbar mCS-light mCSB_scrollTools_vertical" style="display: none;"><div class="mCSB_draggerContainer"><div id="mCSB_1_dragger_vertical" class="mCSB_dragger" style="position: absolute; min-height: 30px; height: 0px; top: 0px;"><div class="mCSB_dragger_bar" style="line-height: 30px;"></div></div><div class="mCSB_draggerRail"></div></div></div></div></div>
        </div>

        <div class="col-3">
            @include('cabinets/client/chat/chat')
        </div>
    </div>
</div>

@include('cabinets/client/footer')

@include('cabinets/client/header')

<div class="container-fluid">
    <div class="row">
        <div class="col-9 lk__wrapper-col">
            <div class="lk__wrapper-title">
                <h1>Профиль</h1>
                <p>Ваши персональные данные</p>
            </div>
            <div class="lk__wrapper-profile">
                <div class="lk__wrapper-profile-tab">
                    <ul class="tab__link">
                        <li><a class="active" href="#t1">данные для входа</a></li>
                        <li><a href="#t2" class="">Профиль</a></li>
                        <li><a href="#t3" class="">Контакты</a></li>
                    </ul>
                </div>
                <div class="lk__wrapper-profile-content">
                    <form class="form" method="post" action="{{route('client.profile')}}">
                        @csrf
                    <div class="tab__pane lk__tab-pane show" id="t1">
                        <div class="body__wrapper-form">
                            <div class="row">
                                <div class="col-7">

                                        <fieldset>
                                            <div class="form__label">
                                                <label>E-mail</label>
                                            </div>
                                            <input type="text" name="email" value="{{$user->email}}">
                                        </fieldset>
                                        <fieldset>
                                            <div class="form__label">
                                                <label>Текущий пароль</label>
                                            </div>
                                            <input type="password" name="password">
                                            <div class="form__label-pwd js-pwd hide"></div>
                                        </fieldset>
                                        <fieldset>
                                            <div class="form__label">
                                                <label>Новый пароль</label>
                                            </div>
                                            <input type="password" name="password-new">
                                            <div class="form__label-pwd js-pwd hide"></div><span>Минимум 6 символов</span>
                                        </fieldset>
                                        <fieldset>
                                            <div class="form__label">
                                                <label>Повторите пароль:</label>
                                            </div>
                                            <input type="password" name="password-repeat">
                                            <div class="form__label-pwd js-pwd hide"></div>
                                        </fieldset>
                                        <div class="btn__wrapper"><button class="btn btn-primary" type="submit">Сохранить</button><a class="btn btn-secondary" href="/cabinet/client/profile">Отменить</a></div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab__pane lk__tab-pane" id="t2">
                        <div class="body__wrapper-form">
                            <div class="row">
                                <div class="col-7">

                                        <fieldset>
                                            <div class="form__label">
                                                <label>ИНН</label>
                                            </div>
                                            <input type="text" name="inn" value="{{$user->inn}}" disabled="disabled" >
                                        </fieldset>
                                        <fieldset>
                                            <div class="form__label">
                                                <label>Наименование организации (сокращенное)</label>
                                            </div>
                                            <input type="text" name="company_name_short" value="{{$contract['company_name_short']}}">
                                        </fieldset>
                                        <div class="row">
                                            <div class="col-6">
                                                <fieldset>
                                                    <div class="form__label">
                                                        <label>ОГРН</label>
                                                    </div>
                                                    <input type="text" name="ogrn" value="{{$contract['ogrn']}}">
                                                </fieldset>
                                            </div>
                                            <div class="col-6">
                                                <fieldset>
                                                    <div class="form__label">
                                                        <label>КПП</label>
                                                    </div>
                                                    <input type="text" name="kpp" value="{{$contract['kpp']}}">
                                                </fieldset>
                                            </div>
                                            <div class="col-6">
                                                <fieldset>
                                                    <div class="form__label">
                                                        <label>Дата регистрации</label>
                                                    </div>
                                                    <input type="date" name="register_at" value="{{explode(' ',$contract['register_at'])[0]}}">
                                                </fieldset>
                                            </div>
                                        </div>
                                        <fieldset>
                                            <div class="form__label">
                                                <label>Фактический адрес</label>
                                            </div>
                                            <input type="text" name="fact_address" value="{{$contract['fact_address']}}">
                                            <label class="checkbox">
                                                <input type="checkbox" name="fact_check">
                                                <p>Совпадает с юридическим</p>
                                            </label>
                                        </fieldset>
                                        <fieldset>
                                            <div class="form__label">
                                                <label>Почтовый адрес</label>
                                            </div>
                                            <input type="text" name="post_address" value="{{$contract['post_address']}}">
                                            <label class="checkbox">
                                                <input type="checkbox" name="post_check">
                                                <p>Совпадает с юридическим</p>
                                            </label>
                                        </fieldset>
                                        <h3>Подписант 1</h3>
                                        <fieldset>
                                            <div class="form__label">
                                                <label>ФИО</label>
                                            </div>
                                            <input type="text" name="signer1_fio" value="{{$contract['signer1_fio']}}">
                                        </fieldset>
                                        <div class="row">
                                            <div class="col-6">
                                                <fieldset>
                                                    <div class="form__label">
                                                        <label>Должность</label>
                                                    </div>
                                                    <input type="text" name="signer1_position" value="{{$contract['signer1_position']}}">
                                                </fieldset>
                                            </div>
                                            <div class="col-6">
                                                <fieldset>
                                                    <div class="form__label">
                                                        <label>Действует на основании</label>
                                                    </div>
                                                    <input type="text" name="signer1_foundation" value="{{$contract['signer1_foundation']}}">
                                                </fieldset>
                                            </div>
                                        </div>
                                        <h3>Подписант 2</h3>
                                        <fieldset>
                                            <div class="form__label">
                                                <label>ФИО</label>
                                            </div>
                                            <input type="text" name="signer2_fio" value="{{$contract['signer2_fio']}}">
                                        </fieldset>
                                        <div class="row">
                                            <div class="col-6">
                                                <fieldset>
                                                    <div class="form__label">
                                                        <label>Должность</label>
                                                    </div>
                                                    <input type="text" name="signer2_position" value="{{$contract['signer2_position']}}">
                                                </fieldset>
                                            </div>
                                            <div class="col-6">
                                                <fieldset>
                                                    <div class="form__label">
                                                        <label>Действует на основании</label>
                                                    </div>
                                                    <input type="text" name="signer2_foundation" value="{{$contract['signer2_foundation']}}">
                                                </fieldset>
                                            </div>
                                        </div>
{{--                                        <h3>Загрузить документ (при наличии)</h3>--}}
{{--                                        <fieldset>--}}
{{--                                            <input type="file">--}}
{{--                                        </fieldset>--}}
                                        <div class="btn__wrapper"><button class="btn btn-primary" type="submit">Сохранить</button><a class="btn btn-secondary" href="/cabinet/client/profile">Отменить</a></div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab__pane lk__tab-pane" id="t3">
                        <div class="body__wrapper-form">
                            <div class="row">
                                <div class="col-7">

                                        <fieldset>
                                            <div class="form__label">
                                                <label>Имя контактного лица</label>
                                            </div>
                                            <input type="text" name="partner_fio" value="{{$contract['partner_fio']}}">
                                        </fieldset>
                                        <fieldset>
                                            <div class="form__label">
                                                <label>Номер телефона</label>
                                            </div>
                                            <input type="text" name="partner_phone" value="{{$contract['partner_phone']}}">
                                        </fieldset>
                                        <fieldset>
                                            <div class="form__label mb-3">
                                                <label>Персональный менеджер</label>
                                            </div>
                                            <p>Иванов Иван Иванович <br>+7 (999) 876-54-32</p>
                                        </fieldset>
                                        <div class="btn__wrapper"><button class="btn btn-primary" type="submit">Сохранить</button><a class="btn btn-secondary" href="/cabinet/client/profile">Отменить</a></div>

                                </div>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-3">
            @include('cabinets/client/chat/chat')
        </div>
    </div>
</div>

@include('cabinets/client/footer')

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="HandheldFriendly" content="true">
    <meta http-equiv="cleartype" content="on">
    <meta http-equiv="msthemecompatible" content="no">
    <meta name="format-detection" content="telephone=no">
    <link rel="shortcut icon" type="image/x-icon" href="cabinets/client/favicon.ico">
    <title>William Kidd - Поставщик углеводородов</title>
    <link href="{{ asset('cabinets/client/vendors/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('cabinets/client/vendors/malihu-scrollbar/jquery.mCustomScrollbar.css') }}" rel="stylesheet">
    <link href="{{ asset('cabinets/client/vendors/jquery-ui/themes/base/jquery-ui.min.css') }}" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet">

    <link href="{{ asset('assets/css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('cabinets/client/assets/css/reg_partner_form.css') }}" rel="stylesheet">
    <script type="text/javascript" src="{{ asset('vendors/jquery/dist/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendors/jquery-ui/jquery-ui.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendors/malihu-scrollbar/jquery.mCustomScrollbar.concat.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('cabinets/client/assets/js/reg_partner_form.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/app.js') }}"></script>

    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
</head>
<body class="lk__wrapper">

<header class="lk__header">
    <div class="container-fluid">
        <div class="row align-items-stretch">
            <div class="col-9">
                <div class="row align-items-center">
                    <div class="col-7">
                        <div class="d-flex align-items-center">
                            <div class="logo"><img src="{{ asset('cabinets/client/assets/images/logo.svg') }}"></div>
                            <ul class="nav nav-bold">
                                <li><a href="/cabinet/client/orders">Заказы</a></li>
                                <li><a href="#">Документы</a></li>
                                <li><a href="/cabinet/client/profile">Профиль</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-5 text-right">
                        <div class="d-flex align-items-center justify-content-end">
                            <ul class="nav">
                                <li><a href="#">Торговля</a></li>
                                <li><a href="#">Финансирование</a></li>
                                <li><a href="#">Инвестирование</a></li>
                            </ul>
                        </div><a class="nav-menu d-none" href="#"><img src="{{ asset('cabinets/client/assets/images/nav.svg') }}"></a>
                    </div>
                </div>
            </div>
            <div class="col-3">
                <div class="lk__header-chat">
                    <div><span>чат с менеджером <img src="{{ asset('cabinets/client/assets/images/ib.svg') }}"></span>
                        <p style="display: none;">Иванов Иван Иванович</p>
                    </div><a class="nav-menu" href="#"><img src="{{ asset('cabinets/client/assets/images/nav.svg') }}"></a>
                </div>
            </div>
        </div>
    </div>
</header>
<div class="lk__wrapper-body">

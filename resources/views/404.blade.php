@include('header')
<div class="error__wrapper">
    <div class="container">
        <div class="row">
            <div class="col-xl-10 mx-auto">
                <div class="d-flex align-items-center"><img src="assets/images/404.png">
                    <div>
                        <h1>404</h1><strong>Кажется, что-то пошло не так</strong>
                        <p>Мы разберемся с этим недоразумением, а пока предлагаем вам вернуться на главную страницу.</p><a class="btn btn-primary" href="/">На главную</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('footer')

@include('header')
<div class="contacts__wrapper">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1>Контакты</h1>
            </div>
            <div class="col-xl-6 col-lg-4">
                <div class="contacts__wrapper-items"><strong>Адрес</strong>
                    <p>{!! $settings['user.contacts.address'] !!}</p>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4">
                <div class="contacts__wrapper-items"><strong>Режим работы</strong>
                    <p>{!! $settings['user.contacts.worktime'] !!}</p>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4">
                <div class="contacts__wrapper-items"><strong>Свяжитесь с нами</strong><a href="tel:+{{ preg_replace('![^0-9]+!', '', $settings['user.contacts.phone']) }}">{!! $settings['user.contacts.phone'] !!}</a><a href="mailto:{!! $settings['user.contacts.email'] !!}">{!! $settings['user.contacts.email'] !!}</a></div>
            </div>
            <div class="col-12">
                <div id="map" style="width: 100%; height: 440px;">
                    <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A4b2b115010c9632537a46194aaf722196d25759a3cf3b63d743d79b1bd69cad3&amp;width=100%&amp;height=440&amp;lang=ru_RU&amp;scroll=true"></script>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="contacts__wrapper-question">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <h2>Остались вопросы? <br/>Воспользуйтесь электронной формой. <br/>Мы ответим вам в первый рабочий день. </h2>
                <form id="feedback" class="callback" method="POST" action="/mail/send/feedback">
                    @method('POST')
                    @csrf
                    <div class="row">
                        <div class="col-sm-6">
                            <label>
                                <input type="text" name="name"><span>Имя </span>
                            </label>
                        </div>
                        <div class="col-sm-6">
                            <label>
                                <input type="text" name="phone"><span>Телефон для связи</span>
                            </label>
                        </div>
                        <div class="col-sm-6 hidden">
                            <label>
                                <input type="text" name="check"><span>Телефон для связи</span>
                            </label>
                        </div>
                        <div class="col-12">
                            <label>
                                <textarea name="message"></textarea><span>Сообщение</span>
                            </label>
                        </div>
                    </div>
                    <div class="row align-items-center">
                        <div class="col-sm-7">
                            <p>Нажимая кнопку «отправить», я подтверждаю согласие c <a href="/terms-of-use">Пользовательским соглашением</a> и <a href="/privacy-policy">Политикой конфиденциальности</a></p>
                        </div>
                        <div class="col-sm-5 text-right"><button type="submit" class="btn btn-primary">+ Отправить</button></div>
                    </div>
                </form>
            </div>
        </div>
    </div><img class="d-none d-lg-inline-block" src="assets/images/contacts.jpg">
</div>
@include('footer')

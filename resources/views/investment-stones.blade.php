@include('header-invert')
<div class="page__wrapper">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6">
                <h1>Инвестиционные камни</h1>
                <p>Сотрудничаем с лидером алмазодобывающей отрасли мира</p>
            </div>
            <div class="col-xl-4 col-lg-6 ml-auto text-right">
                <div class="page__wrapper-jewel"><img src="assets/images/jewel.svg">
                    <div><strong>№0120001889 </strong>
                        <p>Номер в Российской пробирной палате</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><img class="cover" src="assets/images/jewel.jpg">
<div class="block">
    <div class="container">
        <div class="row">
            <div class="col-lg-5">
                <h3>Является поставщиком алмазодобывающей компании «АЛРОСА»</h3>
                <p class="m d-inline-block d-lg-none" style="margin-top: -45px;">Оформление сделки может быть организовано удаленно в. отделениях АЛРОСА в Гонконге или в зоне freeport в Женеве. <br><br> Другой вариант оформления – приобретение бриллиантов через отделение АЛРОСА в Дубай. В случае оформления сделки за предела ми России Вам будут предложены удобные варианты хранения и страхования.</p>
            </div>
            <div class="col-lg-6 ml-auto">
                <div class="image"><img src="assets/images/wm4.jpg"><span><img src="assets/images/alr.png"></span></div>
            </div>
        </div>
    </div>
</div>
<div class="subblock d-none d-lg-inline-block">
    <div class="container">
        <div class="row">
            <div class="col-xl-5 col-6">
                <p class="m" style="margin-top: -45px;">Оформление сделки может быть организовано удаленно в. отделениях АЛРОСА в Гонконге или в зоне freeport в Женеве. <br><br> Другой вариант оформления – приобретение бриллиантов через отделение АЛРОСА в Дубай. В случае оформления сделки за предела ми России Вам будут предложены удобные варианты хранения и страхования.</p>
            </div>
        </div>
    </div>
</div>
<div class="page__wrapper-calc">
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <div class="sticky">
                    <h3>Калькулятор бриллиантов</h3>
                    <p>Рассчитайте цену бриллианта. Выберите нужные характеристики камня и нажмите кнопку «Рассчитать»</p>
                    <ul class="d-none d-lg-inline-block">
                        <li><span>карат</span><span class="js-field-carrat">0,08 - 4,3</span></li>
                        <li><span>цвет</span><span class="js-field-color">D</span></li>
                        <li><span>чистота</span><span class="js-field-clear">IF</span></li>
                    </ul><a class="btn btn-primary js-jewel" href="#" data-toggle="modal" data-target="calc">+ рассчитать</a>
                </div>
            </div>
            <div class="col-lg-8 ml-auto">
                <form class="form__jewel">
                    <div class="row">
                        <div class="col-12">
                            <h3 class="d-inline-block d-lg-none">Калькулятор бриллиантов</h3><a class="close-j d-inline-block d-lg-none" href="#" data-close>&#215;</a>
                            <fieldset>
                                <div class="title"><span>01.</span>
                                    <p>выберите Форму бриллианта</p>
                                </div>
                                <div class="form-row mb-0">
                                    <div class="form-block form-block-wide">
                                        <label>
                                            <input type="radio" name="form" value="круглый">
                                            <div><img src="assets/images/fr1.svg">
                                                <p>круглый</p>
                                            </div>
                                        </label>
                                        <label>
                                            <input type="radio" name="form" value="изумруд" checked>
                                            <div><img src="assets/images/fr2.svg">
                                                <p>изумруд</p>
                                            </div>
                                        </label>
                                        <label>
                                            <input type="radio" name="form" value="овал">
                                            <div><img src="assets/images/fr3.svg">
                                                <p>овал</p>
                                            </div>
                                        </label>
                                        <label>
                                            <input type="radio" name="form" value="груша">
                                            <div><img src="assets/images/fr4.svg">
                                                <p>груша</p>
                                            </div>
                                        </label>
                                        <label>
                                            <input type="radio" name="form" value="маркиз">
                                            <div><img src="assets/images/fr5.svg">
                                                <p>маркиз</p>
                                            </div>
                                        </label>
                                        <label>
                                            <input type="radio" name="form" value="принцесса">
                                            <div><img src="assets/images/fr6.svg">
                                                <p>принцесса</p>
                                            </div>
                                        </label>
                                        <label>
                                            <input type="radio" name="form" value="квадратный изумруд">
                                            <div><img src="assets/images/fr7.svg">
                                                <p>квадратный изумруд</p>
                                            </div>
                                        </label>
                                        <label>
                                            <input type="radio" name="form" value="сердце">
                                            <div><img src="assets/images/fr8.svg">
                                                <p>сердце</p>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                        <div class="col-md-6">
                            <fieldset>
                                <div class="title"><span>02.</span>
                                    <p>ценовой диапазон</p>
                                </div>
                                <div class="slider">
                                    <p>₽</p>
                                    <div class="price_dia">
                                        <div class="jq-slider" data-min="9000" data-max="18413000" data-step="1000"></div>
                                        <div class="min"></div>
                                        <div class="max"></div>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                        <div class="col-md-6">
                            <fieldset>
                                <div class="title"><span>03.</span>
                                    <p>вес</p>
                                </div>
                                <div class="slider">
                                    <p>КАР</p>
                                    <div class="carat">
                                        <div class="jq-slider js-slider-carrat" data-min="0.08" data-max="4.30" data-step="0.01"></div>
                                        <div class="min"></div>
                                        <div class="max"></div>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                        <div class="col-12">
                            <fieldset>
                                <div class="title"><span>04.</span>
                                    <p>цвет</p>
                                </div>
                                <div class="form-row">
                                    <div class="form-block">
                                        <label>
                                            <input type="radio" name="color" data-name="D" checked value="D">
                                            <div title="Совершенно бесцветный"><img src="assets/images/jf.svg">
                                                <p>D</p>
                                            </div>
                                        </label>
                                        <label>
                                            <input type="radio" name="color" data-name="E" value="E">
                                            <div title="Практически бесцветный"><img src="assets/images/jf.svg">
                                                <p>E</p>
                                            </div>
                                        </label>
                                        <label>
                                            <input type="radio" name="color" data-name="F" value="F">
                                            <div title="Практически бесцветный"><img src="assets/images/jf.svg">
                                                <p>F</p>
                                            </div>
                                        </label><span>бесцветные</span>
                                    </div>
                                    <div class="form-block">
                                        <label>
                                            <input type="radio" name="color" data-name="G" value="G">
                                            <div title="Почти бесцветный"><img src="assets/images/jf.svg">
                                                <p>G</p>
                                            </div>
                                        </label>
                                        <label>
                                            <input type="radio" name="color" data-name="J" value="J">
                                            <div title="Почти бесцветный с небольшим желтым оттенком"><img src="assets/images/jf.svg">
                                                <p>J</p>
                                            </div>
                                        </label>
                                        <label>
                                            <input type="radio" name="color" data-name="H" value="H">
                                            <div title="Почти бесцветный"><img src="assets/images/jf.svg">
                                                <p>H</p>
                                            </div>
                                        </label>
                                        <label>
                                            <input type="radio" name="color" data-name="I" value="I">
                                            <div title="Почти бесцветный с небольшим желтым оттенком"><img src="assets/images/jf.svg">
                                                <p>I</p>
                                            </div>
                                        </label><span>почти бесцветные</span>
                                    </div>
                                    <div class="form-block">
                                        <label>
                                            <input type="radio" name="color" data-name="K" value="K">
                                            <div title="Небольшой заметный желтый оттенок"><img src="assets/images/jf.svg">
                                                <p>K</p>
                                            </div>
                                        </label>
                                        <label>
                                            <input type="radio" name="color" data-name="L" value="L">
                                            <div title="Небольшой заметный желтый оттенок"><img src="assets/images/jf.svg">
                                                <p>L</p>
                                            </div>
                                        </label>
                                        <label>
                                            <input type="radio" name="color" data-name="M" value="M">
                                            <div title="Очень легкий желтый оттенок"><img src="assets/images/jf.svg">
                                                <p>M</p>
                                            </div>
                                        </label><span>легкий оттенок</span>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                        <div class="col-12">
                            <fieldset>
                                <div class="title"><span>05.</span>
                                    <p>чистота</p>
                                </div>
                                <div class="form-row">
                                    <div class="form-block">
                                        <label>
                                            <input type="radio" name="clear" data-name="IF" checked value="IF">
                                            <div title="Нет никаких включений, поверхностные пятна видны квалифицированному оценщику при 10-кратном увеличении"><img src="assets/images/jf.svg">
                                                <p>IF</p>
                                            </div>
                                        </label><span>чистый</span>
                                    </div>
                                    <div class="form-block">
                                        <label>
                                            <input type="radio" name="clear" data-name="VVS1" value="VVS1">
                                            <div title="Мельчайшие включения, которые варьируются от трудноразличимых до очень трудноразличимых, видны опытному оценщику при 10-кратном увеличении"><img src="assets/images/jf.svg">
                                                <p>VVS1</p>
                                            </div>
                                        </label>
                                        <label>
                                            <input type="radio" name="clear" data-name="VVS2" value="VVS2">
                                            <div title="Мельчайшие включения, которые варьируются от трудноразличимых до очень трудноразличимых, видны опытному оценщику при 10-кратном увеличении"><img src="assets/images/jf.svg">
                                                <p>VVS2</p>
                                            </div>
                                        </label><span>очень-очень <br/>мелкие <br/>включения</span>
                                    </div>
                                    <div class="form-block">
                                        <label>
                                            <input type="radio" name="clear" data-name="VS1" value="VS1">
                                            <div title="Очень незначительные включения, которые варьируются от трудноразличимых до различимых довольно легко"><img src="assets/images/jf.svg">
                                                <p>VS1</p>
                                            </div>
                                        </label>
                                        <label>
                                            <input type="radio" name="clear" data-name="VS2" value="VS2">
                                            <div title="Очень незначительные включения, которые варьируются от трудноразличимых до различимых довольно легко"><img src="assets/images/jf.svg">
                                                <p>VS2</p>
                                            </div>
                                        </label><span>очень <br/>незначительные <br/>включения</span>
                                    </div>
                                    <div class="form-block">
                                        <label>
                                            <input type="radio" name="clear" data-name="SI1" value="SI1">
                                            <div title="Заметные включения, варьирующиеся от простых до различимых очень легко при 10-кратном увеличении"><img src="assets/images/jf.svg">
                                                <p>SI1</p>
                                            </div>
                                        </label>
                                        <label>
                                            <input type="radio" name="clear" data-name="SI2" value="SI2">
                                            <div title="Заметные включения, варьирующиеся от простых до различимых очень легко при 10-кратном увеличении"><img src="assets/images/jf.svg">
                                                <p>SI2</p>
                                            </div>
                                        </label>
                                        <label>
                                            <input type="radio" name="clear" data-name="SI3" value="SI3">
                                            <div title="Заметные включения, варьирующиеся от простых до различимых очень легко при 10-кратном увеличении"><img src="assets/images/jf.svg">
                                                <p>SI3</p>
                                            </div>
                                        </label><span>мелкие заметные <br/>включения</span>
                                    </div>
                                    <div class="form-block">
                                        <label>
                                            <input type="radio" name="clear" data-name="I1" value="I1">
                                            <div title="Включения очевидны при 10-кратном увеличении и могут повлиять на прозрачность и яркость"><img src="assets/images/jf.svg">
                                                <p>I1</p>
                                            </div>
                                        </label>
                                        <label>
                                            <input type="radio" name="clear" data-name="I2" value="I2">
                                            <div title="Включения очевидны при 10-кратном увеличении и могут повлиять на прозрачность и яркость"><img src="assets/images/jf.svg">
                                                <p>I2</p>
                                            </div>
                                        </label><span>заметные <br/>включения</span>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                        <div class="col-12">
                            <fieldset>
                                <div class="title"><span>06.</span>
                                    <p>Огранка</p>
                                </div>
                                <div class="form-row mb-0">
                                    <div class="form-block form-block-wide">
                                        <label>
                                            <input type="radio" name="shape" value="EXCELLENT">
                                            <div  class="activate_stones" ><img src="assets/images/js.svg">
                                                <p>EXCELLENT</p>
                                            </div>
                                        </label>
                                        <label>
                                            <input type="radio" name="shape"  value="VERY GOOD" checked>
                                            <div><img src="assets/images/js.svg">
                                                <p>VERY GOOD</p>
                                            </div>
                                        </label>
                                        <label>
                                            <input type="radio" name="shape" value="GOOD">
                                            <div><img src="assets/images/js.svg">
                                                <p>GOOD</p>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                        <div class="col-md-6">
                            <fieldset>
                                <div class="title"><span>07.</span>
                                    <p>ДИАМЕТР/ДЛИНА</p>
                                </div>
                                <div class="slider">
                                    <p>ММ</p>
                                    <div class="diameter">
                                        <div class="jq-slider" data-min="2.85" data-max="11.20" data-step="0.01"></div>
                                        <div class="min"> </div>
                                        <div class="max"></div>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                        <div class="col-md-6">
                            <fieldset>
                                <div class="title"><span>08.</span>
                                    <p>ширина</p>
                                </div>
                                <div class="slider">
                                    <p>ММ</p>
                                    <div class="wight">
                                        <div class="jq-slider" data-min="2.43" data-max="8.04" data-step="0.01"></div>
                                        <div class="min"> </div>
                                        <div class="max"></div>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                        <div class="col-12">
                            <fieldset>
                                <div class="title"><span>09.</span>
                                    <p>цена за карат</p>
                                </div>
                                <div class="slider">
                                    <p>₽</p>
                                    <div class="price">
                                        <div class="jq-slider" data-min="59500" data-max="4282100" data-step="500"></div>
                                        <div class="min"> </div>
                                        <div class="max"></div>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                        <div class="col-12">
                            <fieldset class="mb-0">
                                <div class="title"><span>10.</span>
                                    <p>другие параметры</p>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4">
                                        <label><span>флуоресценция <i class="tt" title="Видимый свет, излучаемый некоторыми алмазами при воздействии ультрафиолетовых (УФ) лучей.">?</i></span>
                                            <select name="flur">
                                                <option value="NONE">NONE</option>
                                                <option value="FAINT">FAINT</option>
                                                <option value="MEDIUM">MEDIUM</option>
                                                <option value="STRONG">STRONG</option>
                                                <option value="VERY STRONG">VERY STRONG</option>
                                            </select>
                                        </label>
                                    </div>
                                    <div class="col-lg-4">
                                        <label><span>год добычи</span>
                                            <select name="year">
                                                <option value="2013">2013</option>
                                                <option value="2014">2014</option>
                                                <option value="2015">2015</option>
                                                <option value="2016">2016</option>
                                                <option value="2017">2017</option>
                                                <option value="2018">2018</option>
                                                <option value="2019">2019</option>
                                                <option value="2020">2020</option>
                                            </select>
                                        </label>
                                    </div>
                                    <div class="col-lg-4">
                                        <label><span>месторождение</span>
                                            <select name="location">
                                                <option value="Якутия">Якутия</option>
                                                <option value="Архангельск">Архангельск</option>
                                            </select>
                                        </label>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                        <div class="person d-inline-block d-lg-none">
                            <div class="col-12">
                                <div class="title mb-3">Персональные данные</div>
                            </div>
                            <div class="col-12">
                                <label class="mb-2"><span>ФИО</span>
                                    <input type="text">
                                </label>
                                <label class="mb-2"><span>E-mail</span>
                                    <input type="text">
                                </label>
                                <label class="mb-2"><span>Телефон</span>
                                    <input type="text">
                                </label><a class="w-100 btn btn-primary mb-3 mt-3" href="#">Отправить</a><a class="w-100 btn btn-secondary hide-form" href="#" data-close>Скрыть</a>
                            </div>
                        </div>
                    </div>
                </form><a class="form-spoiler d-none d-lg-inline-block" href="#"></a>
            </div>
        </div>
    </div>
</div>
<div class="page__wrapper-alrosa">
    <div class="container">
        <div class="row align-items-stretch">
            <div class="col-lg-4 bdl"><span>АЛРОСА</span>
                <h3>Сотрудничаем с лидером алмазодобывающей отрасли мира</h3>
                <p>Мы можем организовать доставку бриллиантов или ювелирных изделий в любую удобную для Вас точку мира</p><a download href="/storage/files/Alrosa_diamond_exclusive_prs-2020%20v3.pdf"><img src="assets/images/alrosa.jpg"></a>
            </div>
            <div class="col-lg-7 ml-auto">
                <h4>Почему стоит инвестировать в бриллианты?</h4>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="a-item"><img src="assets/images/ar1.png">
                            <p>мультивалютность</p><span>Цена номинируется в долларах <br/>США, но реализация возможна <br/>в различных валютах.</span>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="a-item"><img src="assets/images/ar2.png">
                            <p>Низкая волатильность</p><span>и их слабая корреляция <br/>с другими формам инвестиций.</span>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="a-item mb-0"><img src="assets/images/ar3.png">
                            <p>вневременной актив</p><span>Невозобновляемый природный <br/>актив, пользующийся <br/>спросом во все времена</span>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="a-item mb-0"><img src="assets/images/ar4.png">
                            <p>Легкая передача</p><span>Отсутствие регистрации <br/>владения, соответственно, <br/>легкая передача актива</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="page__wrapper-form">
    <div class="container">
        <div class="row">
            <div class="col-lg-5">
                <h3>Для получения детальной информации воспользуйтесь формой обратной связи </h3><span class="d-none d-lg-inline-block">Телефон для связи</span>
                <p class="d-none d-lg-inline-block">+7 495 120-73-37</p>
            </div>
            <div class="col-lg-6 ml-auto">
                <form class="callback" id="form_callback">
                    @method('POST')
                    @csrf
                    <div class="row">
                        <div class="col-sm-6">
                            <label>
                                <input type="text" name="name"><span>Имя </span>
                            </label>
                        </div>
                        <div class="col-sm-6">
                            <label>
                                <input type="text" name="phone"><span>Телефон для связи</span>
                            </label>
                        </div>
                        <div class="col-sm-6 hidden">
                            <label>
                                <input type="text" name="check"><span>Телефон для связи</span>
                            </label>
                        </div>
                        <div class="col-12">
                            <label>
                                <select  name="product"  class="gray_list">
                                    <option value="Вид продукта">Вид продукта</option>
                                    @foreach($list as $item)
                                        <option value="{!! $item->value !!}">{!! $item->name !!}</option>
                                    @endforeach
                                </select>
                            </label>
                        </div>
                    </div>
                    <div class="row align-items-center">
                        <div class="col-sm-7">
                            <p>Нажимая кнопку «отправить», я подтверждаю согласие c <a href="/terms-of-use">Пользовательским соглашением</a> и <a href="/privacy-policy">Политикой конфиденциальности</a></p>
                        </div>
                        <div class="col-sm-5 text-right"><button class="btn btn-primary">+ Отправить</button></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@include('footer')

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="HandheldFriendly" content="true">
    <meta http-equiv="cleartype" content="on">
    <meta http-equiv="msthemecompatible" content="no">
    <meta name="format-detection" content="telephone=no">
    <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico">
    <title>William Kidd - Поставщик углеводородов</title>
    <link href="{{ asset('vendors/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendors/malihu-scrollbar/jquery.mCustomScrollbar.css') }}" rel="stylesheet">
    <link href="{{ asset('vendors/jquery-ui/themes/base/jquery-ui.min.css') }}" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet">
    <link href="{{ asset('assets/css/app.css') }}" rel="stylesheet">
    <script type="text/javascript" src="{{ asset('vendors/jquery/dist/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendors/jquery-ui/jquery-ui.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendors/malihu-scrollbar/jquery.mCustomScrollbar.concat.min.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script type="text/javascript" src="{{ asset('assets/js/jquery.maskedinput.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/app.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/main.js') }}"></script>

</head>
<body class="invert">
<div class="mobile-menu"><a class="btn btn-secondary" href="/cabinet/client/login">Кабинет покупателя</a>
    {!! menu('top-left', 'top-mobile-menu') !!}
    {!! menu('top-right', 'top-mobile-menu') !!}
</div>
<header class="screen_second">
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="col-7">
                <div class="d-flex align-items-center">
                    <a class="logo" href="/"><img src="/assets/images/logo-light.svg"></a>
                    {!! menu('top-left', 'top-left-menu') !!}
                </div>
            </div>
            <div class="col-5 text-right">
                <div class="d-flex align-items-center justify-content-end">
                    {!! menu('top-right', 'top-right-menu') !!}
                    <a class="btn btn-secondary" href="/cabinet/client/login">Кабинет покупателя</a>
                </div><a class="nav-menu d-none" href="#"><img src="/assets/images/nav.svg"></a>
            </div>
        </div>
    </div>
</header>

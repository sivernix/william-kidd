@include('header')
<div class="page__wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <h1>Мы знаем индустрию, <br/>а нас знают крупнейшие игроки рынка</h1>
            </div>
            <div class="col-xl-5 col-lg-6 ml-auto">
                <div class="row">
                    <div class="col-md-6">
                        <div class="page__num"><strong>85</strong>
                            <div>
                                <p>регионов доставки<br/>нефтепродуктов</p><span>Прямые договоры <br/>поставки с НПЗ</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6"><a class="page__wrapper-video  js-jewel" data-target="video" href="#"><img src="assets/images/vidoe.jpg">
                            </a></div>
                </div>
            </div>
        </div>
    </div>
</div><img class="cover" src="assets/images/about.jpg">
<div class="block">
    <div class="container">
        <div class="row">
            <div class="col-lg-5">
                <h3>С 2008 занимаемся торговлей нефтепродуктами, инвестициями в драгоценные камни и финансированием</h3>
                <p class="d-inline-block d-lg-none">Накопленный опыт позволяет нам быть оперативными в принятии решений</p>
            </div>
            <div class="col-lg-6 ml-auto">
                <div class="image"><img src="assets/images/wm.jpg"><span><img src="assets/images/logo.svg"></span></div>
            </div>
        </div>
    </div>
</div>
<div class="subblock d-none d-lg-inline-block">
    <div class="container">
        <div class="row">
            <div class="col-xl-4 col-6">
                <p>Накопленный опыт позволяет нам быть оперативными в принятии решений</p>
            </div>
        </div>
    </div>
</div>
<div class="sell">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6">
                <h2>Продажа нефтепродуктов <br/>и инвестиционных камней</h2>
                <div class="d-flex align-items-center justify-content-start btn-flex"><a class="btn btn-primary" href="/fuel">+ Подробности</a><a class="more" href="#request">оставить заявку</a></div>
            </div>
            <div class="col-xl-4 col-lg-5 ml-auto"><strong>Доставляем по всей России</strong>
                <div class="d-flex align-items-center justify-content-start"><span>85</span>
                    <p>Регионов доставки <br/>от Калининграда <br/>до Сахалина</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="page__wrapper-provider">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-5 mr-auto">
                <p>Нашими поставщиками являются крупнейшие российские нефтяные компании и их дочерние структуры, а также алмазодобывающая компания «АЛРОСА»</p>
            </div>
            <div class="col-lg-6">
                <div class="provider-line">
                    <div class="provider-items"><img src="/assets/images/p1.png"></div>
                    <div class="provider-items"><img src="/assets/images/p2.png"></div>
                    <div class="provider-items"><img src="/assets/images/p3.png"></div>
                </div>
                <div class="provider-line">
                    <div class="provider-items"><img src="/assets/images/p4.png"></div>
                    <div class="provider-items"><img src="/assets/images/p5.png"></div>
                    <div class="provider-items"><img src="/assets/images/p6.png"></div>
                </div>
            </div>
        </div>
    </div>
</div><img class="cover" src="/assets/images/about2.jpg">
<div class="page__wrapper-descriptor">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-5"><span>Торговля. Финансирование. Инвестирование.</span>
                <h3>William Kidd – поставщик нефтепродуктов высокого качества по всей России от ведущих производителей</h3><a class="page__wrapper-video js-jewel" data-toggle="modal" data-target="video" href="#"><img src="assets/images/vidoe.jpg"></a>
            </div>
            <div class="col-lg-6 ml-auto">
                <p>Уильям Кидд — это команда квалифицированных и опытных специалистов во всех областях нашей деятельности.</p>
                <p>Наши партнеры — крупные нефтедобывающие и нефтеперерабатывающие компании России, биржевые торговые площадки, организации финансового сектора, торговые и транспортные компании, бюджетные организации.</p><a class="btn btn-primary" href="#request">+ ОСТАВИТЬ ЗАЯВКУ</a>
            </div>
        </div>
    </div>
</div>
<div class="page__wrapper-props">
    <div class="container">
        <div class="row">
            <div class="col-xl-6 col-lg-5">
                <h3>Реквизиты компании</h3><a download href="/storage/{!! json_decode($settings['user.about.banks'])[0]->download_link !!}">Банки-партнеры</a><br><a download href="/storage/{!! json_decode($settings['user.about.allrek'])[0]->download_link !!}">Все реквизиты</a>
            </div>
            <div class="col-xl-6 col-lg-7">
                <div class="props-list">
                    <div class="props-item"><span>Название на русс. языке</span>
                        <p>{!! $settings['user.about.namerus'] !!}</p>
                    </div>
                    <div class="props-item"><span>Юридический адрес</span>
                        <p>{!! $settings['user.about.uaddress'] !!}</p>
                    </div>
                    <div class="props-item"><span>Контактный телефон</span>
                        <p>{!! $settings['user.about.contactphone'] !!}</p>
                    </div>
                    <div class="props-item"><span>ИНН</span>
                        <p>{!! $settings['user.about.inn'] !!}</p>
                    </div>
                    <div class="props-item"><span>КПП</span>
                        <p>{!! $settings['user.about.kpp'] !!}</p>
                    </div>
                    <div class="props-item"><span>ОГРН</span>
                        <p>{!! $settings['user.about.ogrn'] !!}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="page__wrapper-form" id="request">
    <div class="container">
        <div class="row">
            <div class="col-lg-5">
                <h3>Для получения детальной информации воспользуйтесь формой обратной связи </h3><span class="d-none d-lg-inline-block">Телефон для связи</span>
                <p class="d-none d-lg-inline-block">{!! $settings['user.about.contactphone'] !!}</p>
            </div>
            <div class="col-lg-6 ml-auto">
                <form class="callback" id="form_callback">
                    @method('POST')
                    @csrf
                    <div class="row">
                        <div class="col-sm-6">
                            <label>
                                <input type="text" name="name"><span>Имя </span>
                            </label>
                        </div>
                        <div class="col-sm-6">
                            <label>
                                <input type="text" name="phone"><span>Телефон для связи</span>
                            </label>
                        </div>
                        <div class="col-sm-6 hidden">
                            <label>
                                <input type="text" name="check"><span></span>
                            </label>
                        </div>
                        <div class="col-12">
                            <label>
                                <select name="product" class="gray_list">
                                    <option value="Вид продукта">Вид продукта</option>
                                    @foreach($list as $item)
                                        <option value="{!! $item->value !!}">{!! $item->name !!}</option>
                                    @endforeach
                                </select>
                            </label>
                        </div>
                    </div>
                    <div class="row align-items-center">
                        <div class="col-sm-7">
                            <p>Нажимая кнопку «отправить», я подтверждаю согласие c <a href="/terms-of-use">Пользовательским соглашением</a> и <a href="/privacy-policy">Политикой конфиденциальности</a>
                            </p>
                        </div>
                        <div class="col-sm-5 text-right"><button class="btn btn-primary">+ Отправить</button></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@include('footer')

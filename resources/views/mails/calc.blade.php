Здравствуйте,
<p>Поступила заявка на индивидуальный расчет топлива</p>

<p><u>Данные запроса:</u></p>

<div>
    <p><b>Имя:</b>&nbsp;{{ $calc->name }}</p>
    <p><b>Телефон:</b>&nbsp;{{ $calc->phone }}</p>
    <p><b>Email:</b>&nbsp;{{ $calc->email }}</p>
    <p><b>ИНН:</b> {{$calc->inn}}</p>
    <p><b>Вид топлива:</b>&nbsp;{{$calc->type}}</p>
    <p><b>Лотов:</b>&nbsp;{{$calc->number}}</p>
    <p><b>Тонн:</b>&nbsp;{{$calc->size}}</p>
    <p><b>Станция назначения:</b>&nbsp;{{$calc->station}}</p>
    <p><b>Период отгрузки:</b>&nbsp;{{$calc->period }}</p>
    <p><b>Предоплата:</b>&nbsp;{{$calc->prepayment}}</p>
    <p><b>Срок оплаты:</b>&nbsp;{{$calc->dueDate}}</p>
    <p><b>Отсрочка:</b>&nbsp;{{$calc->gracePeriod}}</p>
    <p><b>Оплата по факту прибытия:</b>&nbsp;{{$calc->fact}}</p>
    <p><b>Комментарий:</b>&nbsp;{{$calc->comment }}</p>
</div>

Письмо создано автоматически

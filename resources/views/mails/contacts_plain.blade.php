Здравствуйте,
<p>Поступила заявка с формы обратной связи</p>

<p><u>Данные контакта:</u></p>

<div>
    <p><b>Имя:</b>&nbsp;{{ $contact->name }}</p>
    <p><b>Телефон:</b>&nbsp;{{ $contact->phone }}</p>
    <p><b>Сообщение:</b>&nbsp;{{ $contact->message }}</p>
</div>

Спасибо

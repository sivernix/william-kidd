Здравствуйте,
<p>Поступила заявка с формы калькулятора бриллиантов</p>

<p><u>Данные запроса информации о продукте:</u></p>

<div>
    <p><b>Имя:</b>&nbsp;{{ $stones->name }}</p>
    <p><b>Телефон:</b>&nbsp;{{ $stones->phone }}</p>
    <p><b>E-mail:</b>&nbsp;{{ $stones->email }}</p>
</div>

<div>
    <p><b>Форма:</b>&nbsp;{{ $stones->form }}</p>
    <p><b>Цвет:</b>&nbsp;{{ $stones->color }}</p>
    <p><b>Чистота:</b>&nbsp;{{ $stones->clear }}</p>
    <p><b>Огранка:</b>&nbsp;{{ $stones->shape }}</p>
    <p><b>Флуоресценция:</b>&nbsp;{{ $stones->flur }}</p>
    <p><b>Год:</b>&nbsp;{{ $stones->year }}</p>
    <p><b>Месторождение:</b>&nbsp;{{ $stones->location }}</p>
    <p><b>Ценовой диапазон:</b>&nbsp;{{ $stones->price_dia }}</p>
    <p><b>Вес:</b>&nbsp;{{ $stones->carat }}</p>
    <p><b>Диаметр/длинна:</b>&nbsp;{{ $stones->diameter }}</p>
    <p><b>Ширина:</b>&nbsp;{{ $stones->wight }}</p>
    <p><b>Цена за карат:</b>&nbsp;{{ $stones->price }}</p>
</div>

Письмо создано автоматически

@include('header')
<div class="page__wrapper">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6">
                <h1>Финансирование <br/>торговых контрактов</h1>
                <p>Поддержка торговых операций и контрактов поставки</p>
            </div>
            <div class="col-xl-5 col-lg-6 ml-auto">
                <div class="row align-items-center">
                    <div class="col-md-6">
                        <div class="page__num"><strong>1777</strong>
                            <div>
                                <p>произведено <br/>поставок </p><span>за последний год</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6"><a class="page__wrapper-video js-jewel" data-target="video" href="#"><img src="assets/images/vidoe.jpg">
                            </a></div>
                </div>
            </div>
        </div>
    </div>
</div><img class="cover" src="assets/images/finance.jpg">
<div class="block">
    <div class="container">
        <div class="row">
            <div class="col-lg-5">
                <h3>Предоставляем возможность существенно нарастить объем торговых операций</h3>
                <p class="m d-inline-block d-lg-none" style="margin-top: -45px;">Мы занимаемся финансированием торговых контрактов на закупку нефтепродуктов, представленных в котировках на СПбМТСБ, в том числе мазута, дизельного топлива, авиакеросина и др.</p><a class="more d-inline-block d-lg-none" href="#request">оставить заявку</a>
            </div>
            <div class="col-lg-6 ml-auto">
                <div class="image"><img src="assets/images/wm2.jpg"><span><img src="assets/images/rdk.png"></span></div>
            </div>
        </div>
    </div>
</div>
<div class="subblock d-none d-lg-inline-block">
    <div class="container">
        <div class="row">
            <div class="col-xl-5 col-6">
                <p class="m" style="margin-top: -45px;">Мы занимаемся финансированием торговых контрактов на закупку нефтепродуктов, представленных в котировках на СПбМТСБ, в том числе мазута, дизельного топлива, авиакеросина и др.</p><a class="more" href="#request">оставить заявку</a>
            </div>
        </div>
    </div>
</div>
<div class="partners">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="d-flex align-items-center justify-content-start"><img src="assets/images/pt1.png">
                    <div>
                        <h3>Основной партнер</h3>
                        <p>ООО КБ «ВНЕШФИНБАНК» предлагает все необходимые банковские услуги, что способствует повышению оперативности при реализации проектов.</p>
                    </div>
                </div>
            </div>
            <div class="col-xl-5 col-lg-6 ml-auto">
                <h4>Другие партнеры</h4>
                <div class="d-flex align-items-center justify-content-start"><img src="assets/images/pt2.png"><img src="assets/images/pt3.png"></div>
            </div>
        </div>
    </div>
</div><img class="cover" src="assets/images/finance2.jpg">
<div class="page__wrapper-form" id="request">
    <div class="container">
        <div class="row">
            <div class="col-lg-5">
                <h3>Для получения детальной информации воспользуйтесь формой обратной связи </h3><span class="d-none d-lg-inline-block">Телефон для связи</span>
                <p class="d-none d-lg-inline-block">+7 495 120-73-37</p>
            </div>
            <div class="col-lg-6 ml-auto">
                <form class="callback" id="form_callback">
                    @method('POST')
                    @csrf
                    <div class="row">
                        <div class="col-sm-6">
                            <label>
                                <input type="text" name="name"><span>Имя </span>
                            </label>
                        </div>
                        <div class="col-sm-6">
                            <label>
                                <input type="text" name="phone"><span>Телефон для связи</span>
                            </label>
                        </div>
                        <div class="col-sm-6 hidden">
                            <label>
                                <input type="text" name="check"><span>Телефон для связи</span>
                            </label>
                        </div>
                        <div class="col-12">
                            <label>
                                <select name="product"class="gray_list">
                                    <option value="Вид продукта">Вид продукта</option>
                                    @foreach($list as $item)
                                        <option value="{!! $item->value !!}">{!! $item->name !!}</option>
                                    @endforeach
                                </select>
                            </label>
                        </div>
                    </div>
                    <div class="row align-items-center">
                        <div class="col-sm-7">
                            <p>Нажимая кнопку «отправить», я подтверждаю согласие c <a href="/terms-of-use">Пользовательским соглашением</a> и <a href="/privacy-policy">Политикой конфиденциальности</a></p>
                        </div>
                        <div class="col-sm-5 text-right"><button class="btn btn-primary">+ Отправить</button></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@include('footer')

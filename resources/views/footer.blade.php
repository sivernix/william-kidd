<footer class="screen_second">
    <div class="container">
        <div class="row">
            <div class="col-lg-4"><a class="logo" href="#"><img src="/assets/images/logo.svg"></a>
                <p class="d-none d-lg-inline-block">{!! setting('site.preview_text_logo') !!}</p>
                <div class="social d-none d-lg-inline-block">
                    <p>Мы в соц. сетях</p>
                    <ul>
                        <li><a target="_blank" href="{!! setting('site.vk_link') !!}"><img src="/assets/images/vk.svg"></a></li>
                        <li><a target="_blank" href="{!! setting('site.fb_link') !!}"><img src="/assets/images/fb.svg"></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-8">
                <div class="row">
                    <div class="col-4 d-none d-lg-inline-block"><strong>Компания</strong>
                        {!!  menu('bottom-left', 'bottom-menu') !!}
                    </div>
                    <div class="col-4 d-none d-lg-inline-block"><strong>Пользователям</strong>
                        {!!  menu('bottom-right', 'bottom-menu') !!}
                    </div>
                    <div class="col-lg-4 footer__contact"><strong>Как с нами связаться</strong>
                        <p>{!! setting('site.address_footer') !!}</p><a href="mailto:{!! setting('site.email_address') !!}">{!! setting('site.email_address') !!}</a></br>
                        <a href="tel:{!! str_replace([' ', '(', ')', '-'], '', setting('site.phone_number')) !!}">{!! setting('site.phone_number') !!}</a>
                        <div class="social d-inline-block d-lg-none">
                            <ul>
                                <li><a href="{!! setting('site.vk_link') !!}"><img src="/assets/images/vk.svg"></a></li>
                                <li><a href="{!! setting('site.fb_link') !!}"><img src="/assets/images/fb.svg"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bottom">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-3">
                    <p>© William Kidd LLC, 2008-2021</p>
                </div>
                <div class="col-lg-9">
                    <div class="row">
                        <div class="col-lg-4"><a href="/terms-of-use">Пользовательское соглашение</a></div>
                        <div class="col-lg-4"><a href="/privacy-policy">Политика конфиденциальности</a></div>
                        <div class="col-lg-4 text-right"><a target="_blank" href="https://affetta.ru/">Создание сайта - Affetta</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<div class="modal modal-side" id="export">
    <div class="modal-body"><a class="close" href="#" data-dismiss>&#215;</a>
        <form>
            <h2>Выгрузка сводной таблицы профилей</h2>
            <fieldset>
                <div class="row">
                    <div class="col-12">
                        <h4>Сформировать за период <i title="gjlcrfprf">?</i></h4>
                    </div>
                    <div class="col-6">
                        <label>
                            <input type="date">
                        </label>
                    </div>
                    <div class="col-6">
                        <label>
                            <input type="date">
                        </label>
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <div class="row">
                    <div class="col-12">
                        <h4>Данные для выгрузки</h4>
                    </div>
                    <div class="col-6">
                        <label>
                            <input type="checkbox" disabled checked>
                            <p>ИНН</p>
                        </label>
                        <label>
                            <input type="checkbox" disabled checked>
                            <p>Наименование</p>
                        </label>
                        <label>
                            <input type="checkbox" disabled checked>
                            <p>E-mail</p>
                        </label>
                        <label>
                            <input type="checkbox">
                            <p>ОГРН</p>
                        </label>
                        <label>
                            <input type="checkbox">
                            <p>КПП</p>
                        </label>
                        <label>
                            <input type="checkbox">
                            <p>Дата регистрации</p>
                        </label>
                        <label>
                            <input type="checkbox">
                            <p>Юридический адрес</p>
                        </label>
                        <label>
                            <input type="checkbox">
                            <p>Фактический адрес</p>
                        </label>
                        <label>
                            <input type="checkbox">
                            <p>Почтовый адрес</p>
                        </label>
                        <label>
                            <input type="checkbox">
                            <p>ЕИО</p>
                        </label>
                    </div>
                    <div class="col-6">
                        <label>
                            <input type="checkbox">
                            <p>ОКПО</p>
                        </label>
                        <label>
                            <input type="checkbox">
                            <p>Среднесписочная численность</p>
                        </label>
                        <label>
                            <input type="checkbox">
                            <p>Подписанты</p>
                        </label>
                        <label>
                            <input type="checkbox">
                            <p>Банк</p>
                        </label>
                        <label>
                            <input type="checkbox">
                            <p>БИК банка</p>
                        </label>
                        <label>
                            <input type="checkbox">
                            <p>Номер к/с</p>
                        </label>
                        <label>
                            <input type="checkbox">
                            <p>Номер р/с</p>
                        </label>
                        <label>
                            <input type="checkbox">
                            <p>Контактный номер телефона</p>
                        </label>
                        <label>
                            <input type="checkbox">
                            <p>Имя контактного лица</p>
                        </label>
                        <label>
                            <input type="checkbox">
                            <p>Персональный менеджер</p>
                        </label>
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <div class="row">
                    <div class="col-12">
                        <label><span>Статус договора</span>
                            <select>
                                <option>Выгрузить все</option>
                            </select>
                        </label>
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <div class="row">
                    <div class="col-12">
                        <label><span>Статус изменений</span>
                            <select>
                                <option>Выгрузить все</option>
                            </select>
                        </label>
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <div class="row">
                    <div class="col-12"><a class="btn btn-primary" href="#">скачать</a><a class="btn btn-secondary" href="#" data-dismiss>Отменить</a></div>
                </div>
            </fieldset>
        </form>
    </div>
</div>
<div class="modal modal-popup" id="closed">
    <div class="modal-container">
        <div class="modal-body"><a class="close" href="#" data-dismiss>&#215;</a>
            <div class="title">
                <h3>Аннулирование договора</h3>
            </div>
            <div class="form">
                <fieldset>
                    <label><span>Укажите причину</span>
                        <textarea placeholder="Например: преращена работа с данной организацией"></textarea>
                    </label>
                </fieldset>
                <fieldset><a class="btn btn-primary" href="#">аннулировать</a><a class="btn btn-secondary" href="#" data-dismiss>отменить</a></fieldset>
            </div>
        </div>
    </div>
</div>
<div class="modal modal-popup" id="sure">
    <div class="modal-container">
        <div class="modal-body"><a class="close" href="#" data-dismiss>&#215;</a>
            <div class="title">
                <h3>Уверены, что хотите покинуть страницу?</h3>
            </div>
            <div class="form">
                <fieldset><a class="btn btn-primary" href="#">Да</a><a class="btn btn-secondary" href="#" data-dismiss>отменить</a></fieldset>
            </div>
        </div>
    </div>
</div>
<div class="modal modal-popup" id="calc">
    <div class="modal-overlay"></div>
    <div class="modal-popup-body"><a class="close" href="#" data-dismiss>&#215;</a>
        <div class="modal-popup-title">
            <h3>Укажите данные для получения расчёта</h3>
            <p>Наш менеджер скоро свяжется с вами</p>
        </div>
        <form>
            @method('POST')
            @csrf
            <label><span>ФИО</span>
                <input type="text" name="name">
            </label>
            <label><span>E-mail</span>
                <input type="text" name="email">
            </label>
            <label><span>Телефон</span>
                <input type="text" name="phone">
            </label><button type="submit" class="btn btn-primary">Отправить</button>
        </form>
    </div>
</div>
<div class="modal modal-popup" id="error">
    <div class="modal-overlay"></div>
    <div class="modal-popup-body"><a class="close" href="#" data-dismiss>&#215;</a>
        <div class="modal-popup-title">
            <h3>Произошла ошибка!</h3>
            <p>Попробуйте повторить ввод через несколько минут</p>
        </div>
        <form><a class="btn btn-primary" href="#">Вернуться</a></form>
    </div>
</div>
<div class="modal modal-popup" id="success">
    <div class="modal-overlay"></div>
    <div class="modal-popup-body"><a class="close" href="#" data-dismiss>&#215;</a>
        <div class="modal-popup-title">
            <h3>Заявка успешно отправлена</h3>
            <p>Наш менеджер скоро свяжется с вами</p>
        </div>
        <form><a class="btn btn-primary" href="#">Вернуться</a></form>
    </div>
</div>
<div class="modal modal-popup" id="video">
    <div class="modal-overlay"></div>
    <div class="modal-popup-body">
        <iframe width="470" height="322" src="https://www.youtube.com/embed/M9T7UC2F4x0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
</div>
</body>
</html>

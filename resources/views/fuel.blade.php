@include('header')
<div class="page__wrapper">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6">
                <h1>Оптовая торговля <br/>и поставка топлива</h1>
                <p>С нами вы можете заключать договор онлайн и получать полный <br/>доступ к закупке нефтепродуктов</p>
            </div>
            <div class="col-xl-5 col-lg-6 ml-auto">
                <div class="row align-items-center">
                    <div class="col-md-6">
                        <div class="page__num"><strong>85</strong>
                            <div>
                                <p>регионов доставки <br/>нефтепродуктов</p><span>Прямые договоры <br/>поставки с НПЗ</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6"><a class="page__wrapper-video js-jewel"  data-target="video" href="#"><img src="assets/images/vidoe.jpg">
                            </a></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="page__filter">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <form id="calculatorMini">
                    <p>Рассчитайте <br/>cтоимость топлива</p>
                    <label>
                        <select placeholder="Вид топлива" name="type">
                            @foreach($fuelType as $item)
                                <option value="{!! $item['code'] !!}">{!! $item['name'] !!}</option>
                            @endforeach
                        </select>
                    </label>
                    <label>
{{--                        <select placeholder="Станция назначения" name="station">--}}
{{--                            <option value="Станция 1">Станция 1</option>--}}
{{--                        </select>--}}
                        <input type="text" placeholder="Станция назначения" name="station">
                    </label>
                    <label>
                        <input type="text" placeholder="Объем, тонн" name="size">
                    </label><button class="btn btn-primary calculation">+ Рассчитать</button>
                </form>
            </div>
        </div>
    </div>
</div><img class="cover" src="assets/images/fuel.jpg">
<div class="block">
    <div class="container">
        <div class="row">
            <div class="col-lg-5">
                <h3>«William Kidd» – поставщик нефтепродуктов высокого качества по всей России</h3>
                <p class="m d-inline-block d-lg-none" style="margin-top: -45px;">Членство на бирже СПбМТСБ, а также длительные партнерские отношения с ресурсодержателями и транспортными компаниями позволяют нам вести деятельность по поставке всех видов нефтепродуктов по всей территории России.</p><a class="more d-inline-block d-lg-none" href="#request">оставить заявку</a>
            </div>
            <div class="col-lg-6 ml-auto">
                <div class="image"><img src="assets/images/wm3.jpg"><span><img src="assets/images/rdk.png"></span></div>
            </div>
        </div>
    </div>
</div>
<div class="subblock d-none d-lg-inline-block">
    <div class="container">
        <div class="row">
            <div class="col-xl-5 col-6">
                <p class="m" style="margin-top: -45px;">Членство на бирже СПбМТСБ, а также длительные партнерские отношения с ресурсодержателями и транспортными компаниями позволяют нам вести деятельность по поставке всех видов нефтепродуктов по всей территории России.</p><a class="more" href="#request">оставить заявку</a>
            </div>
        </div>
    </div>
</div>
<div class="page__count"><img class="d-none d-lg-inline-block" src="assets/images/count.jpg">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 ml-auto">
                <div class="page__count-header"><span>William Kidd в цифрах</span></div>
                <div class="row">
                    <div class="col-12">
                        <h3>К началу 2017 года мы поставили 448&nbsp;020 тонн или 7467 цистерн нефтепродуктов.</h3>
                    </div>
                    <div class="col-md-4">
                        <div class="page__count-item"><strong>448 020</strong>
                            <p>тонн нефтепродуктов поставлено всего</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="page__count-item"><strong>1000 <span>литров</span></strong>
                            <p>минимальная партия для&nbsp;заказа</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="page__count-item"><strong>1777</strong>
                            <p>произведено поставок за&nbsp;последний год</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="page__wrapper-provider">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-5 mr-auto">
                <p>Нашими поставщиками являются крупнейшие российские нефтяные компании и их дочерние структуры.</p><a class="more" href="#request">оставить заявку</a>
            </div>
            <div class="col-lg-6">
                <div class="provider-line">
                    <div class="provider-items"><img src="/assets/images/p1.png"></div>
                    <div class="provider-items"><img src="/assets/images/p2.png"></div>
                    <div class="provider-items"><img src="/assets/images/p3.png"></div>
                </div>
                <div class="provider-line">
                    <div class="provider-items"><img src="/assets/images/p4.png"></div>
                    <div class="provider-items"><img src="/assets/images/p5.png"></div>
                    <div class="provider-items"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="page__wrapper-form" id="request">
    <div class="container">
        <div class="row">
            <div class="col-lg-5">
                <h3>Для получения детальной информации воспользуйтесь формой обратной связи </h3><span class="d-none d-lg-inline-block">Телефон для связи</span>
                <p class="d-none d-lg-inline-block">+7 495 120-73-37</p>
            </div>
            <div class="col-lg-6 ml-auto">
                <form class="callback" id="form_callback">
                    @method('POST')
                    @csrf
                    <div class="row">
                        <div class="col-sm-6">
                            <label>
                                <input type="text" name="name"><span>Имя </span>
                            </label>
                        </div>
                        <div class="col-sm-6">
                            <label>
                                <input type="text" name="phone"><span>Телефон для связи</span>
                            </label>
                        </div>
                        <div class="col-12">
                            <label>
                                <select name="product"  class="gray_list">
                                    <option value="Вид продукта">Вид продукта</option>
                                    @foreach($list as $item)
                                        <option value="{!! $item->value !!}">{!! $item->name !!}</option>
                                    @endforeach
                                </select>
                            </label>
                        </div>
                    </div>
                    <div class="row align-items-center">
                        <div class="col-sm-7">
                            <p>Нажимая кнопку «отправить», я подтверждаю согласие c <a href="/terms-of-use">Пользовательским соглашением</a> и <a href="/privacy-policy">Политикой конфиденциальности</p>
                        </div>
                        <div class="col-sm-5 text-right"><button type="submit" class="btn btn-primary">+ Отправить</button></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@include('footer')

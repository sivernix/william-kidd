@include('header')
<div class="page__wrapper page-border">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-12">
                <h1>Индивидуальный расчет cтоимости топлива</h1>
            </div>
            <div class="col-12">
                <form class="form__calc" id="calculator">
                    @method('POST')
                    @csrf
                    <div class="row">
                        <div class="col-lg-6">
                            <fieldset><strong>Условия доставки</strong>
                                <label><span>Вид топлива</span>
                                    <select name="type" id="typeFuel">
                                        @foreach($typeList as $item)
                                            <option value="{!! $item['code'] !!}" @if($item['code'] == $requestParam['type']) selected @endif size="{!! $item['size'] !!}">{!! $item['name'] !!}</option>
                                        @endforeach
                                    </select>
                                </label>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label><span>Лотов</span>
                                            <input type="number" name="lots" value="{!! $requestParam['lots'] !!}">
                                        </label>
                                    </div>
                                    <div class="col-sm-6">
                                        <label><span>Тонн <i>?</i></span>
                                            <input type="number" name="size"  disabled>
                                        </label>
                                    </div>
                                </div>
                                <label><span>Станция назначения</span>
{{--                                    <select name="station">--}}
{{--                                        <option value="Асино, Западно-Сибирская ЖД">Асино, Западно-Сибирская ЖД</option>--}}
{{--                                    </select>--}}
                                    <input type="text"  name="station" value="{!! $requestParam['station']  !!}">
                                </label>
                                <label><span>Период отгрузки</span>
                                    <select name="period">
                                        <option value="Иное" @if("Иное" == $requestParam['period']) selected @endif>Иное</option>
                                        <option value="до 30 дней" @if("до 30 дней" == $requestParam['period']) selected @endif>до 30 дней</option>
                                    </select>
                                </label>
                            </fieldset>
                            <fieldset class="mb-0"><strong>Условия оплаты</strong>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label><span>Предоплата</span>
                                            <select name="prepayment">
                                                <option value="100%" @if("100%" == $requestParam['prepayment']) selected @endif>100%</option>
                                                <option value="90%" @if("90%" == $requestParam['prepayment']) selected @endif>90%</option>
                                                <option value="80%" @if("80%" == $requestParam['prepayment']) selected @endif>80%</option>
                                                <option value="70%" @if("70%" == $requestParam['prepayment']) selected @endif>70%</option>
                                                <option value="60%" @if("60%" == $requestParam['prepayment']) selected @endif>60%</option>
                                                <option value="50%" @if("50%" == $requestParam['prepayment']) selected @endif>50%</option>
                                                <option value="40%" @if("40%" == $requestParam['prepayment']) selected @endif>40%</option>
                                                <option value="30%" @if("30%" == $requestParam['prepayment']) selected @endif>30%</option>
                                                <option value="20%" @if("20%" == $requestParam['prepayment']) selected @endif>20%</option>
                                                <option value="10%" @if("10%" == $requestParam['prepayment']) selected @endif>10%</option>
                                                <option value="0%" @if("0%" == $requestParam['prepayment']) selected @endif>0%</option>
                                            </select>
                                        </label>
                                    </div>
                                    <div class="col-sm-6">
                                        <label><span>Срок оплаты</span>
                                            <select name="dueDate">
                                                <option value="Иное" @if("Иное" == $requestParam['dueDate']) selected @endif>Иное</option>
                                                <option value="2 дня" @if("2 дня" == $requestParam['dueDate']) selected @endif>2 дня</option>
                                            </select>
                                        </label>
                                    </div>
                                </div>
                                <label class="g_period @if("on" == $requestParam['fact']) disabled @endif"><span>Отсрочка <i>?</i></span>
                                    <input type="number" name="gracePeriod"  @if("on" == $requestParam['fact']) disabled @endif>
{{--                                    <span class="error">Укажите период отсрочки</span>--}}
                                </label>
                                <label>
                                    <input type="checkbox" name="fact" @if("on" == $requestParam['fact']) checked @endif>
                                    <p>Оплата по факту прибытия</p>
                                </label>
                            </fieldset>
                        </div>
                        <div class="col-lg-6"><strong>Личные данные</strong>
                            <label><span>ИНН</span>
                                <input placeholder="" type="number" name="inn">
                            </label>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label><span>E-mail</span>
                                        <input type="text" placeholder="" name="email">
                                    </label>
                                </div>
                                <div class="col-sm-6">
                                    <label><span>Телефон*</span>
                                        <input type="text" placeholder="" name="phone">
                                    </label>
                                </div>
                            </div>
                            <input type="hidden" placeholder="" name="check">
                            <label><span>Комментарий</span>
                                <textarea name="comment"></textarea>
                            </label><em>*Поля, обязательные для заполнения</em>
                            <div class="btn__wrapper"><a class="btn btn-secondary" href="/fuel">Отмена</a><button class="btn btn-primary">+ Рассчитать</button></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@include('footer')

@include('header')
<div class="page__wrapper page-border">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-12">
                <h1>Рассчитайте cтоимость топлива</h1>
            </div>
            <div class="col-12">
                <form class="form__calc" id="calculator-easy">
                    @method('POST')
                    @csrf
                    <div class="row">
                        <div class="col-lg-6"><strong>Условия доставки</strong>
                            <label><span>Вид топлива</span>
                                <select name="type" id="typeFuel">
                                    @foreach($typeList as $item)
                                        <option value="{!! $item['code'] !!}" @if($item['code'] == $requestParam['type']) selected @endif size="{!! $item['size'] !!}">{!! $item['name'] !!}</option>
                                    @endforeach
                                </select>
                            </label>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label><span>Лотов</span>
                                        <input type="number" name="lots">
                                    </label>
                                </div>
                                <div class="col-sm-6">
                                    <label><span>Тонн <i>?</i></span>
                                        <input type="number" name="size" disabled>
                                    </label>
                                </div>
                            </div>
                            <label><span>Станция назначения</span>
{{--                                <select>--}}
{{--                                    <option value="Асино, Западно-Сибирская ЖД">Асино, Западно-Сибирская ЖД</option>--}}
{{--                                </select>--}}
                                <input type="text"  name="station" value="{!! $requestParam['station']  !!}">
                            </label>
                            <label><span>Период отгрузки</span>
                                <select name="period">
                                    <option value="Иное">Иное</option>
                                    <option value="до 30 дней">до 30 дней</option>
                                </select>
                            </label>
                        </div>
                        <div class="col-lg-6"><strong>Условия оплаты</strong>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label><span>Предоплата</span>
                                        <select name="prepayment">
                                            <option value="100%">100%</option>
                                            <option value="90%">90%</option>
                                            <option value="80%">80%</option>
                                            <option value="70%">70%</option>
                                            <option value="60%">60%</option>
                                            <option value="50%">50%</option>
                                            <option value="40%">40%</option>
                                            <option value="30%">30%</option>
                                            <option value="20%">20%</option>
                                            <option value="10%">10%</option>
                                            <option value="0%">0%</option>
                                        </select>
                                    </label>
                                </div>
                                <div class="col-sm-6">
                                    <label><span>Срок оплаты</span>
                                        <select name="dueDate">
                                            <option value="Иное">Иное</option>
                                            <option value="2 дня">2 дня</option>
                                        </select>
                                    </label>
                                </div>
                            </div>
                            <label class="g_period"><span>Отсрочка <i>?</i></span>
{{--                                <select disabled>--}}
{{--                                    <option value="0 дней">0 дней</option>--}}
{{--                                </select>--}}
                                <input type="number" name="gracePeriod">
                            </label>
                            <label>
                                <input type="checkbox" name="fact">
                                <p>Оплата по факту прибытия</p>
                            </label>
                            <blockquote>В случае отсрочки платежа необходимо предоставление банковской гарантии. <a herf="/banks" target="_blank">Список банков-партнеров</a></blockquote><button class="btn btn-primary" data-target="calc">+ Рассчитать</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@include('footer')

@include('header')
<div class="news__wrapper-page">
    <div class="container">
        <div class="row">
            <div class="col-xl-8 mx-auto">
                <div class="news__wrapper-tags"><span>{!!  $category !!}</span><span>{!! $news[0]->date !!}</span></div>
                <h1>{!! $news[0]->title !!}</h1>
                {!! html_entity_decode($news[0]->content) !!}
                <div class="news__wrapper-similar">
                    <h3>Другие статьи</h3>
                    @foreach($randomNews as $item)
                        <a href="/news/{{$item->id}}"><img src="/assets/images/article.jpg">
                            <p>{{$item->title}}</p>
                        </a>
                    @endforeach 
                    <a class="more" href="/news">смотреть все новости</a>
                </div>
            </div>
        </div>
    </div>
</div>

@include('footer')

@include('header')
<div class="header__page-img" style="background: url('assets/images/news.jpg') center / cover no-repeat">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1>Новости компании
                    @if (is_numeric($requestParam['group']))
                       - {!! $category[$requestParam['group']] !!}
                    @endif
                </h1>
            </div>
        </div>
    </div>
</div>
<div class="news__wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <form action="/news/" method="GET" role="search">
                    {{ csrf_field() }}
                    <input type="text"  name="q" placeholder="Поиск по новостям">
                </form>
                <ul class="news__tag d-lg-none d-flex">
                    <li><a
                            @if (!is_numeric($requestParam['group']))
                                class="active"
                            @endif
                            href="/news/?group=all">Все</a></li>
                    @foreach($category as $index => $item)
                        <li><a @if ($requestParam['group'] == $index) class="active" @endif href="/news/?group={{$index}}">{{$item}}</a></li>
                    @endforeach
                </ul>
                @if($errorMsg)
                    {{$errorMsg}}
                @endif
                @foreach($news as $index => $item)
                    <a class="news__item" href="/news/{{$item->id}}">
                        <div class="news__item-title"><span>{{$category[$item->group]}}</span><span>{{$item->date}}</span></div>
                        <p>{{$item->title}}</p>
                    </a>
                @endforeach
                {{$news->links()}}
            </div>
            <div class="col-lg-4">
                <h3 class="d-none d-lg-inline-block">Рубрики</h3>
                <ul class="news__tag d-none d-lg-flex">
                    <li><a
                            @if ($requestParam['group'] == 'all')
                            class="active"
                            @endif
                            href="/news/?group=all">Все</a></li>
                    @foreach($category as $index => $item)
                        <li><a
                                @if ($requestParam['group'] == $index)
                                class="active"
                                @endif
                                href="/news/?group={{$index}}">{{$item}}</a></li>
                    @endforeach
                </ul>
                <h3>Популярное</h3>
                <div class="news__popular">
                    @foreach($newsTop as $index => $item)
                        @if($item->top)
                            <a class="news__popular-item" href="/news/{{$item->id}}">{{$item->title}}</a>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>

@include('footer')

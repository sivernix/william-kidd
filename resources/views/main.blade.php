@include('header')
<div class="header__page screen_second">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6"><img src="/storage/{!! $settings['user.main.image'] !!} "></div>
                {!! $settings['user.main.text'] !!}
        </div>
    </div>
</div>
<div class="stones screen_second">
    <div class="container">
        <div class="row"><a class="col-lg-4 text-center" href="{!! $settings['user.main.linkg1'] !!}"><img src="/storage/{!! $settings['user.main.imageg1'] !!}"><strong>{!! $settings['user.main.titleg1'] !!}</strong>
                {!! $settings['user.main.subtitleg1'] !!}</a><a class="col-lg-4 text-center" href="{!! $settings['user.main.linkg2'] !!}"><img src="/storage/{!! $settings['user.main.imageg2'] !!}"><strong>{!! $settings['user.main.titleg2'] !!}</strong>
                {!! $settings['user.main.subtitleg2'] !!}</a><a class="col-lg-4 text-center" href="{!! $settings['user.main.linkg3'] !!}"><img src="/storage/{!! $settings['user.main.image3'] !!}"><strong>{!! $settings['user.main.titleg3'] !!}</strong>
                {!! $settings['user.main.subtitleg3'] !!}</a></div>
    </div>
</div>
@include('footer')

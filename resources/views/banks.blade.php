@include('header')
<body>
<div class="banks__wrapper">
    <div class="container">
        <div class="row">
            <div class="col-xl-10">
                <h1>Банки-партнеры</h1>
                @foreach($banks as $item)
                    <div class="banks__wrapper-items"><img src="/storage/{!! $item->image !!}">
                        <div><strong>{!! $item->title !!}</strong>
                            <p>ИНН: {!! $item->inn !!}</p>
                            <p>Сайт: <a target="_blank" href="{!! $item->site !!}">{!! $item->site !!}</a></p>
                        </div>
                        <div>
                            <p>Телефон: {!! $item->phone !!}</p>
                            <p>E-mail: <a href="{!! $item->email !!}">{!! $item->email !!}</a></p>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@include('footer')

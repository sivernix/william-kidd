const mysql = require("mysql");
const connection = mysql.createConnection({
    host: "127.0.0.1",
    port: "3306",
    database: "william",
    user: "root",
    password: "1234"
});

const express = require('express');
const app = express();
const http = require('http');
const server = http.createServer(app);
const {Server} = require("socket.io");
const io = new Server(server, {
    cors: {
        origin: '*',
    }
});

function empty(vrbl) {
    return typeof(vrbl) === 'undefined' || vrbl.length === 0;
}

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');
});

const chat = {
    socketsToUsersCode: {},
    rooms: {},
    getAllRooms: () => {

        return new Promise((resolve, reject) => {
            const sql = 'SELECT * FROM chat WHERE `client1` LIKE ? OR `client2` LIKE ?';
            let rooms = {};

            connection.query({sql, values: ['%_client', '%_client']}, function (err, results) {
                if (err) console.log(err);

                if (results.length) {

                    for(let i in results) {
                        let roomId = '',
                            member = ''

                        if (results[i].client1.indexOf('_client') > -1) {
                            roomId = results[i].client1;
                            member = results[i].client2;
                        } else if (results[i].client2.indexOf('_client') > -1) {
                            roomId = results[i].client2;
                            member = results[i].client1;
                        }

                        if (!empty(roomId) && typeof(rooms[roomId]) == 'undefined') {
                            rooms[roomId] = [];
                        }

                        if (!empty(roomId) && rooms[roomId].indexOf(member) === -1) {
                            rooms[roomId].push(member);
                        }

                        //идентификатор комнаты, это также и идентификатор партнёра, добавим его как участника тоже
                        if (!empty(roomId) && rooms[roomId].indexOf(roomId) === -1) {
                            rooms[roomId].push(roomId);
                        }
                    }

                    resolve(rooms);

                } else {
                    resolve(false);
                }

            });
        });

    },
    setSocket: (socketId, ucode) => {
        if (typeof (chat.socketsToUsersCode[ucode]) === 'undefined') {
            chat.socketsToUsersCode[ucode] = [];
        }

        if (chat.socketsToUsersCode[ucode].indexOf(socketId) === -1) {
            chat.socketsToUsersCode[ucode].push(socketId);
        }

        console.log(chat.socketsToUsersCode[ucode]);
    },
    removeSocketUser: (socketId) => {
        for (let i in chat.socketsToUsersCode) {
            let index = chat.socketsToUsersCode[i].indexOf(socketId);
            if (index > -1) {
                chat.socketsToUsersCode[i].splice(index, 1);
                console.log(chat.socketsToUsersCode[i]);
            }
        }
    },
    getSocketsByUserCode: (ucode) => {
        if (typeof (chat.socketsToUsersCode[ucode]) === 'undefined') {
            chat.socketsToUsersCode[ucode] = [];
        }

        //возвращаем список сокетов для этого пользователя,
        // т.е. чат может быть открыт в разных вкладках, надо сообщение во все вкладки доставить
        return chat.socketsToUsersCode[ucode];

    },
    getMessageListByUserCode: function (userCode) {

        const sql = 'SELECT * FROM chat WHERE client1 = ? OR client2 = ?';

        connection.query({sql, values: ['%_client', '%_client']}, function (err, results) {
            if (err) console.log(err);
            console.log(results);
        });

    },
    setMessageToUserCode: function (userCodeFrom, userCodeTo, message) {
        /*
                const sql = 'UPDATE chat SET message = ? WHERE (client1 = ? AND client2 = ?) OR (client1 = ? AND client2 = ?)';
                const sql = 'INSERT INTO chat (message) VALUES (?) WHERE (client1 = ? AND client2 = ?) OR (client1 = ? AND client2 = ?)';

                connection.query({ sql, values: [message, userCodeFrom, userCodeTo, userCodeTo, userCodeFrom] }, function(err, results) {
                    if(err) console.log(err);
                    console.log(results);
                });*/

        const sql = 'INSERT INTO chat (message, client1, client2, created_at, updated_at) VALUES (?,?,?,now(),now())';

        connection.query({sql, values: [message, userCodeFrom, userCodeTo, Date.now(), Date.now()]}, function (err, results) {
            if (err) console.log(err);
            console.log(results);
        });

    },
    getDialogPartnerForUserCode: function (userCode) {
        return new Promise((resolve, reject) => {
            const sql = 'SELECT * FROM chat WHERE `client1` = ? OR `client2` = ?';

            connection.query({sql, values: [userCode, userCode]}, function (err, results) {
                if (err) console.log(err);
                if (results.length) {
                    if (results[0].client1 === userCode) {
                        resolve(results[0].client2);
                    } else {
                        resolve(results[0].client1);
                    }
                } else {
                    resolve(false);
                }

            });
        });

    },
}

function sendHeartbeat(){
    setTimeout(sendHeartbeat, 8000);
    io.sockets.emit('ping', { beat : 1 });
}

io.sockets.on('connection', function (socket) {
    socket.on('pong', function(data){
        console.log("Pong received from client");
    });
});

setTimeout(sendHeartbeat, 8000);


io.on('connection', (socket) => {
    var handshakeData = socket.request;
    let client_ucode;

    if (handshakeData) {
        client_ucode = handshakeData._query['ucode'];


        if (socket.id && client_ucode) {
            chat.setSocket(socket.id, client_ucode);
        }
    }

    socket.on('disconnect', function () {
        console.log('Got disconnect!');
        chat.removeSocketUser(socket.id);
    });


    //при подключении нового пользователя, актуализировать комнаты
    chat.getAllRooms().then(res => {
        chat.rooms = res;
    });


    console.log("client_ucode:", client_ucode);
    console.log('a user connected');
    socket.on('chat message', (data) => {
        if (typeof (data.curChat) !== 'undefined') {
            console.log(data.curChat);
            for(let i in chat.rooms[data.curChat]) {
                let clientCode = chat.rooms[data.curChat][i];
                if (clientCode) {
                    let SocketsByUserCode = chat.getSocketsByUserCode(clientCode);

                    if (SocketsByUserCode.length) {

                        console.log(SocketsByUserCode);
                        SocketsByUserCode.forEach((socketId) => {
                            //socket.emit('chat message', {message: data.message, socketId: socket.id, ucode: data.ucode});
                            io.to(socketId).emit('chat message', {
                                message: data.message,
                                socketId: socket.id,
                                ucode: data.ucode
                            });
                        })
                    }

                    if (data.ucode !== clientCode) {
                        //chat.setMessageToUserCode(data.ucode, clientCode, data.message);
                    }
                }
            }

            //кто-то в комнате прислал сообщение, сопоставим сокет с кодом пользователя и отправим всем, кто в этой же комнате
            if (data.socketId && data.ucode) {
                chat.setSocket(data.socketId, data.ucode);
            }


            //io.sockets.emit('chat message', {message: data.message, socketId: socket.id});


        }
    });
});


server.listen(3000, () => {
    console.log('listening on *:3000');
});

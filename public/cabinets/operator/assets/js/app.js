'use strict';

$(function () {

  $('.notify-bar').on('click', 'a', function () {
    $('.notify-bar').hide('drop', {
      direction: 'up'
    }, 300);
    return false;
  });

  $('.js-filter').on('click', function () {
    $(this).next('.filter__wrapper').toggle('drop', {
      direction: 'up'
    }, 200);
    return false;
  });

  $(document).on('keyup', function (e) {
    if (e.keyCode == 27) {
      $('.header__page').find('.filter__wrapper').hide('drop', {
        direction: 'up'
      }, 200);
    }
  });

  $('.js-scroller').mCustomScrollbar({
    mouseWheel: {
      scrollAmount: 200
    },
    scrollInertia: 500
  });

  $('.tab-link').on('click', 'a', function () {
    var _tg = $(this).attr('href');

    $(this).closest('ul').find('li').removeClass('active');
    $(this).closest('li').addClass('active');
    $('.tab-pane').removeClass('active');
    $('.tab-pane' + _tg).addClass('active');

    return false;
  });

  $('.body__page-spoiler').on('click', '.title', function () {
    $(this).closest('.body__page-spoiler').toggleClass('closed');
    return false;
  });

  $('[data-toggle="modal"]').on('click', function () {
    var _tg = $(this).data('target');
    console.log(_tg);

    $('.modal#' + _tg).show();
    return false;
  });

  $('[data-dismiss]').on('click', function () {
    $(this).closest('.modal').hide();
    return false;
  });

  $('.modal-side').on('click', function (e) {
    if (e.target !== e.currentTarget) return;
    $(this).hide();
  });

  $(document).on('keyup', function (e) {
    if (e.keyCode == 27) {
      $('.modal').hide();
    }
  });

  $('.user').on('click', function () {
    $(this).toggleClass('active');
    return false;
  });

  $('.js-pwd').on('click', function () {
    $(this).toggleClass('hide').next('input').attr('type', function (_, attr) {
      return attr == 'password' ? 'text' : 'password';
    });
    return false;
  });
});
//# sourceMappingURL=app.js.map

$(() => {

    let validate_functions = {
        'partner-reg-confirm': function () {
            let fields = [
                    'company_name_short',
                    'company_name_full',
                    'ogrn',
                    'kpp',
                    'register_at',
                    'ur_address',
                    'main_fio',
                    'position',
                    'foundation',
                    'partner_fio',
                    'partner_phone',
                    'partner_email',
                ],
                inputs = $('input'),
                hasErrors = false;

            for (let inpName in fields) {
                let input = inputs.filter('[name="' + fields[inpName] + '"]');
                if (input.val() === '') {
                    hasErrors = true;
                    input.addClass('error');
                } else {
                    input.removeClass('error');
                }
            }

            if (hasErrors) {
                scrollToElement(inputs.filter('.error')[0]);
            }

            return !hasErrors;
        },
        'partner-reg-profile': function () {
            let fields = [
                    'okpo',
                    'average_amount',
                    'fact_address',
                    'post_address',
                    'signer1_fio',
                    'signer1_position',
                    'signer1_foundation',
                    'signer2_fio',
                    'signer2_position',
                    'signer2_foundation',
                    'bank_bik',
                    'bank_account',
                ],
                inputs = $('input'),
                hasErrors = false;

            for (let inpName in fields) {
                let input = inputs.filter('[name="' + fields[inpName] + '"]');
                if (input.val() === '') {
                    hasErrors = true;
                    input.addClass('error');
                } else {
                    input.removeClass('error');
                }
            }

            if (hasErrors) {
                scrollToElement(inputs.filter('.error')[0]);
            }

            return !hasErrors;
        },
        'partner-reg-doc': function () {
            let fields = [
                    'file_charter',
                    'file_reg_cert',
                    'file_decision_create',
                    'file_order_head',
                    'file_order_accountant',
                ],
                inputs = $('label:visible > input[type="file"]'),
                hasErrors = false;

            for (let inpName in fields) {
                let input = inputs.filter('[name="' + fields[inpName] + '"]');
                if (input.val() === '') {
                    hasErrors = true;
                    input.parent().parent().addClass('error');
                } else {
                    input.parent().parent().removeClass('error');
                }
            }

            if (hasErrors) {
                scrollToElement($('.list__doc-item.error')[0]);
            }


            return !hasErrors;
        },
    };

    function scrollToElement(elem) {
        $('html, body').animate({
            scrollTop: $(elem).offset().top - 150
        }, 500);
    }


    $(document).on('click', '.js-btn-send-form', function () {
        let form = $('#' + $(this).data('formid'));

        if (form.length) {
            form.trigger('submit');
        }
    });

    $(document).on('click', '.js-file_reset', function () {
        $(this).parent().removeClass('loaded');
        $(this).hide();
        $(this).parent().find('label').show();
    });

    $(document).on('submit', 'form', function () {
        if ($(this).data('validate')) {
            return validate_functions[$(this).data('validate')]();
        }

        return true;
    });

    $(document).on('change', 'input[type="file"]', function () {
        if ($(this).val()) {
            $(this).parent().parent().addClass('loaded').removeClass('error');
            $(this).parent().parent().find('.js-file_reset').show();
            $(this).parent().parent().find('label').hide();
        }
    });

})

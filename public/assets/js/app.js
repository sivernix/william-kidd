'use strict';

$(function () {

  $('.form__calc select').select2({
    minimumResultsForSearch: Infinity
  });

  $('.form__jewel select').select2({
    minimumResultsForSearch: Infinity
  });

  $('.lk__wrapper select').select2({
    minimumResultsForSearch: Infinity
  });

  $('.jq-slider').each(function () {
    var _min = $(this).data('min');
    var _max = $(this).data('max');
    var _step = $(this).data('step') || 10;

    var _this = $(this);

    $(this).siblings('.min').html(_min.toLocaleString('ru')).position({
      my: 'left top',
      at: 'left bottom',
      of: _this
    });
    $(this).siblings('.max').html(_max.toLocaleString('ru')).position({
      my: 'right top',
      at: 'right bottom',
      of: _this
    });

    $(this).slider({
      range: true,
      min: _min,
      max: _max,
      step: _step,
      values: [_min, _max],
      slide: function slide(event, ui) {
        var delay = function delay() {
          var label = ui.handleIndex == 0 ? '.min' : '.max';
          _this.siblings(label).html(ui.value.toLocaleString('ru')).position({
            my: 'center top',
            at: 'center bottom',
            of: ui.handle
          });
        };
        setTimeout(delay, 5);
        if ($(this).hasClass('js-slider-carrat')) {
          $('.js-field-carrat').text(ui.values[0] + '-' + ui.values[1]);
        }
      }
    });
  });

  $('.form-spoiler').on('click', function () {
    $(this).prev('.form__jewel').toggleClass('opened');
    return false;
  });

  $('.form-block p, .tt, [title]:not(.select2-selection__rendered)').tooltip();

  $('.select2-selection__rendered').attr('title', '');

  $('.js-scroller').mCustomScrollbar({
    mouseWheel: {
      scrollAmount: 200
    },
    scrollInertia: 500
  });

  $('.tab__link').on('click', 'a', function () {
    $('.tab__link').find('a').removeClass('active');
    $(this).addClass('active');
    $('.tab__pane').removeClass('show');
    $('.tab__pane' + $(this).attr('href')).addClass('show');

    return false;
  });

  $('[data-toggle="modal"]').on('click', function () {
    var _target = $(this).data('target');

    $('.modal#' + _target).show();
    return false;
  });

  $(document).on('keyup', function (e) {
    if (e.keyCode == 27) {
      $('.modal').hide();
    }
  });

  $('[data-dismiss], .close').on('click', function () {
    $(this).closest('.modal').hide();
    return false;
  });

  $('.modal-overlay').on('click', function (e) {
    $(this).closest('.modal').hide();
  });

  $('.nav-menu').on('click', function () {
    $('.mobile-menu').toggle('slide', { direction: 'right' }, 300).toggleClass('open');
    return false;
  });

  if ($(window).width() < 992) {
    $('.js-jewel').on('click', function () {
      $('.form__jewel').toggleClass('open');
      return false;
    });
  }

  $('.form__jewel').on('click', '[data-close]', function () {
    $('.form__jewel').toggleClass('open');
    return false;
  });

  $('[name="color"]').on('change', function () {
    $('.js-field-color').text($(this).data('name'));
  });

  $('[name="clear"]').on('change', function () {
    $('.js-field-clear').text($(this).data('name'));
  });

  $('.js-pwd').on('click', function () {
    $(this).toggleClass('hide').prev('input').attr('type', function (_, attr) {
      return attr == 'password' ? 'text' : 'password';
    });
    return false;
  });
});
//# sourceMappingURL=app.js.map

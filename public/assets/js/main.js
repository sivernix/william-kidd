function setCookie(name,value,days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}

function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

$(document).ready(function() {

    $( ".nav-menu.d-none" ).click(function() {
        if($('.nav-menu.d-none:visible').length)
            $('.nav-menu.d-none img').hide();
        else
            $('.nav-menu.d-none img').show();
    });


    $('#success .btn').click(function () {
        $('#success').hide();
    });

    $("form input[name='phone']").mask("+7 (999) 999-9999");

    //Плавная прокрутка по якорю
    const anchors = document.querySelectorAll('a[href*="#"]')

    for (let anchor of anchors) {
        anchor.addEventListener('click', function (e) {
            e.preventDefault()

            const blockID = anchor.getAttribute('href').substr(1)

            document.getElementById(blockID).scrollIntoView({
                behavior: 'smooth',
                block: 'start'
            })
        })
    }

    //Отправка формы со страницы обратной связи
    $("#calculatorMini").submit(function(e) {
        $(this).serialize();
        //prevent Default functionality
        e.preventDefault();
        document.location.href = 'fuel/calculation/?' + $(this).serialize()
    });

    //Отправка формы со страницы обратной связи
    $("#feedback").submit(function(e) {

        //prevent Default functionality
        e.preventDefault();

        //get the action-url of the form
        var actionurl = '/mail/send/feedback';

        //do your own request an handle the results
        var error = true;

        if ( $("input[name='phone']").val().length >= 5 ) {
            error = false;
        } else {
            error = true;
            $("input[name='phone']").css('border-color', '#f00');
        }

        $("input[name='phone']").click(function(){
            $(this).css('border-color', '#000');
        });

        if(error == false) {

            //console.log(data);

            $.ajax({
                url: actionurl,
                type: 'post',
                dataType: 'html',
                data: $( this ).serialize(),
                success: function(res) {
                    $('#success').show();
                    $("input[name='phone']").val("");
                    $("input[name='name']").val("");
                    $("input[name='message']").text("");
                }
            });

        }

    });

    // Отправка формы со страницы - О нас
    $("#form_callback").submit(function(e) {

        //prevent Default functionality
        e.preventDefault();

        //get the action-url of the form
        var actionurl = '/mail/send/info';

        //do your own request an handle the results
        var error = true;

        if ( $("input[name='phone']").val().length >= 5 ) {
            error = false;
        } else {
            error = true;
            $("input[name='phone']").css('border-color', '#f00');
        }

        $("input[name='phone']").click(function(){
            $(this).css('border-color', '#000');
        });

        if(error == false) {

            //console.log(data);

            $.ajax({
                url: actionurl,
                type: 'post',
                dataType: 'html',
                data: $( this ).serialize(),
                success: function(res) {
                    $('#success').show();
                    $("input[name='phone']").val("");
                    $("input[name='name']").val("");
                }
            });

        }

    });

    var disable = true;

    if($('input[name="fact"]:checked').length) {
        disable = false;
        $('input[name="gracePeriod"]').val(0)
    }

    $('input[name="fact"]').click(function () {

        if ( disable === true ) {
            $('input[name="gracePeriod"]').prop('disabled', disable);
            $('.g_period').addClass('disabled');
            $('input[name="gracePeriod"]').val(0)
            disable = false;
        } else if ( disable === false ) {
            $('input[name="gracePeriod"]').prop('disabled', disable);
            $('.g_period').removeClass('disabled');
            $('input[name="gracePeriod"]').val('');
            disable = true;
        }
    });

    var size = $('#typeFuel').find(':selected').attr('size');
    size = parseInt(size);
    var lots = $("input[name='lots']").val();
    if(lots > 0) {
        $( "input[name='size']" ).val(size*lots);
    } else {
        $( "input[name='size']" ).val('');
    }

    $( "input[name='lots']" ).keyup(function() {
        var size = $('#typeFuel').find(':selected').attr('size');
        size = parseInt(size);
        var lots = $(this).val();
        if(lots > 0) {
            $( "input[name='size']" ).val(size*lots);
        } else {
            $( "input[name='size']" ).val('');
        }
    });

    $( "input[name='lots']" ).click(function() {
        var size = $('#typeFuel').find(':selected').attr('size');
        size = parseInt(size);
        var lots = $(this).val();
        if(lots > 0) {
            $( "input[name='size']" ).val(size*lots);
        } else {
            $( "input[name='size']" ).val('');
        }
    });

    $("#calculator-easy").submit(function(e) {

        //prevent Default functionality
        e.preventDefault();
        var lots = $( "input[name='lots']" ).val();

        var serializeData = $( this ).serialize();

        if(lots > 10) {
            document.location.href = '/fuel/calculation/?' + $(this).serialize();
        }
        //get the action-url of the form
        var actionurl = '/mail/send/calculator';

        //do your own request an handle the results
        var error = true;

        if ( $("input[name='phone']").val().length >= 5 ) {
            error = false;
        } else {
            error = true;
            $("input[name='phone']").css('border-color', '#f00');
        }

        $("input[name='phone']").click(function(){
            $(this).css('border-color', '#000');
        });

        if(error == false) {

            //console.log(data);

            $.ajax({
                url: actionurl,
                type: 'post',
                dataType: 'html',
                data: serializeData,
                success: function(res) {
                    $('#success').show();
                    $("input[name='phone']").val("");
                    $("input[name='name']").val("");
                    $("input[name='type']").val("");
                    $("input[name='number']").val("");
                    $("input[name='size']").val("");
                    $("input[name='station']").val("");
                    $("input[name='prepayment']").val("");
                    $("input[name='dueDate']").val("");
                    $("input[name='gracePeriod']").val("");
                    $("input[name='fact']").val("");
                    $("input[name='inn']").val("");
                    $("input[name='comment']").text("");
                    $("input[name='period']").val("");
                    $("input[name='email']").val("");
                }
            });

        }

    });

    $("#calculator").submit(function(e) {

        //prevent Default functionality
        e.preventDefault();

        //get the action-url of the form
        var actionurl = '/mail/send/calculator';

        //do your own request an handle the results
        var error = true;

        if ( $("input[name='phone']").val().length >= 5 ) {
            error = false;
        } else {
            error = true;
            $("input[name='phone']").css('border-color', '#f00');
        }

        $("input[name='phone']").click(function(){
            $(this).css('border-color', '#000');
        });

        if(error == false) {

            //console.log(data);

            $.ajax({
                url: actionurl,
                type: 'post',
                dataType: 'html',
                data: $( this ).serialize(),
                success: function(res) {
                    $('#success').show();
                    $("input[name='phone']").val("");
                    $("input[name='name']").val("");
                    $("input[name='type']").val("");
                    $("input[name='number']").val("");
                    $("input[name='size']").val("");
                    $("input[name='station']").val("");
                    $("input[name='prepayment']").val("");
                    $("input[name='dueDate']").val("");
                    $("input[name='gracePeriod']").val("");
                    $("input[name='fact']").val("");
                    $("input[name='inn']").val("");
                    $("input[name='comment']").text("");
                    $("input[name='period']").val("");
                    $("input[name='email']").val("");
                }
            });

        }

    });


    // Отправка формы на расчет камней
    $('.activate_stones').click();


    $("#calc form").submit(function(e) {

        var options = $(".form__jewel").serialize();


        var data = {
            'price_dia' : $('.price_dia .min').text() + ' - ' + $('.price_dia .max').text(),
            'carat' : $('.carat .min').text() + ' - ' + $('.carat .max').text(),
            'diameter' : $('.diameter .min').text() + ' - ' + $('.diameter .max').text(),
            'wight' : $('.wight .min').text() + ' - ' + $('.wight .max').text(),
            'price' : $('.price .min').text() + ' - ' + $('.price .max').text(),
        }

        options = options + '&' + $.param(data);
        console.log(options);
        //prevent Default functionality
        e.preventDefault();

        //get the action-url of the form
        var actionurl = '/mail/send/calc';

        //do your own request an handle the results
        var error = true;

        if ( $("#calc input[name='phone']").val().length >= 5 ) {
            error = false;
        } else {
            error = true;
            $("#calc input[name='phone']").css('border-color', '#f00');
        }

        $("#calc input[name='phone']").click(function(){
            $(this).css('border-color', '#000');
        });

        if(error == false) {

            //console.log(data);
            options = options + '&' + $("#calc form").serialize();

            $.ajax({
                url: actionurl,
                type: 'post',
                dataType: 'html',
                data: options,
                success: function(res) {
                    $('#success').show();
                    $("input[name='phone']").val("");
                    $("input[name='name']").val("");
                    $("input[name='email']").val("");
                }
            });

        }
    });

});

// $(document).ready(function(){
//     $( ".form__jewel input" ).each(function(id)
//     {
//         if($(this).attr('data-name') != '') {
//             $(this).val($(this).attr('data-name'))
//         }
//     });
// });


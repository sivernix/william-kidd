<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Pub\Pages\NewsController;
use App\Http\Controllers\Pub\Pages\ContactsController;
use App\Http\Controllers\Pub\Pages\MainController;
use App\Http\Controllers\Pub\Pages\BanksController;
use App\Http\Controllers\Pub\Pages\AboutController;
use App\Http\Controllers\Pub\Pages\FinancingController;
use App\Http\Controllers\Pub\Pages\InvestmentStonesController;
use App\Http\Controllers\Pub\Pages\FuelController;
use App\Http\Controllers\Pub\Pages\CalcController;
use App\Http\Controllers\Pub\Pages\TestController;
use App\Http\Controllers\LogoutController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/about', [AboutController::class, 'index']);
Route::post('/mail/send/info', [AboutController::class, 'sendProductInfo']);

Route::get('/news', [NewsController::class, 'index']);
Route::get('/news/{id}', [NewsController::class, 'detail']);

Route::get('/banks', [BanksController::class, 'index']);

Route::get('/contacts', [ContactsController::class, 'index']);
Route::post('/mail/send/feedback', [ContactsController::class, 'sendFeedback']);

Route::get('/fuel', [FuelController::class, 'index']);
Route::post('/mail/send/info', [FuelController::class, 'sendProductInfo']);

Route::get('/fuel/calculation', [CalcController::class, 'index']);
//Route::get('/fuel/easy-calculation', [CalcController::class, 'indexEasy']);
Route::post('/mail/send/calculator', [CalcController::class, 'sendCalcInfo']);

Route::get('/investment-stones', [InvestmentStonesController::class, 'index']);
Route::post('/mail/send/calc', [InvestmentStonesController::class, 'calcSend']);

Route::get('/financing', [FinancingController::class, 'index']);

Route::get('/', [MainController::class, 'index'])->name('main');

Route::get('/terms-of-use', function () {
    return view('terms-of-use');
});

Route::get('/privacy-policy', function () {
    return view('privacy-policy');
});

Route::get('/trading', function () {
    return view('trading');
});

Route::get('/biography', function () {
    return view('bio');
});

Route::get('/phpinfo', function () {
    phpinfo();
});

Route::get('/test', [TestController::class, 'index']);

Route::group(['prefix' => 'cabinet/operator'], function () {

    Route::get('/documents', function () {return view('cabinets.operator.documents');})->name('cabinet.od')->middleware(['auth']);
    Route::get('/contract/{id}', [App\Http\Controllers\Client\ClientContractController::class,'getContracts'])->name('cabinet.oc')->middleware(['auth']);
    Route::get('/contract', [App\Http\Controllers\Client\ClientContractController::class,'index'])->name('cabinet.oci')->middleware(['auth']);
    Route::post('/contract/set-status', [App\Http\Controllers\Client\ClientContractController::class,'setContractStatus'])->name('cabinet.setstat')->middleware(['auth']);
    Route::get('/order', function () {return view('cabinets.operator.order');})->name('cabinet.oo')->middleware(['auth']);
    Route::get('/profile', [App\Http\Controllers\Client\ClientProfileController::class,'getProfiles'])->name('cabinet.op')->middleware(['auth']);
    Route::get('/profile/{id}', [App\Http\Controllers\Client\ClientProfileController::class,'getProfile'])->name('cabinet.opg')->middleware(['auth']);
    Route::get('/chat/', [App\Http\Controllers\ChatOperator::class,'index'])->name('cabinet.och')->middleware(['auth']);
    Route::get('/chat/{id}', [App\Http\Controllers\ChatOperator::class,'index'])->name('cabinet.och')->middleware(['auth']);
    Route::get('/order/new', function () {return view('cabinets.operator.order-new');})->name('cabinet.on')->middleware(['auth']);
    Route::get('/history', function () {return view('cabinets.operator.history');})->name('cabinet.oh')->middleware(['auth']);
});

Route::group(['middleware' => ['auth']], function() {
    /**
     * Logout Route
     */
    Route::get('/logout', 'App\Http\Controllers\LogoutController@perform')->name('logout.perform');
});

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();

    Route::get('main-list','App\Http\Controllers\Voyager\VoyagerMainListController@index')->middleware('admin.user');
    Route::put('main-list','App\Http\Controllers\Voyager\VoyagerMainListController@update')->middleware('admin.user')->name('main.update');

    Route::get('contacts','App\Http\Controllers\Voyager\VoyagerContactsController@index')->middleware('admin.user');
    Route::put('contacts','App\Http\Controllers\Voyager\VoyagerContactsController@update')->middleware('admin.user')->name('contacts.update');

    Route::get('about','App\Http\Controllers\Voyager\VoyagerAboutController@index')->middleware('admin.user');
    Route::put('about','App\Http\Controllers\Voyager\VoyagerAboutController@update')->middleware('admin.user')->name('about.update');
});

Route::view('/cabinet/operator/password', 'cabinets.operator.password')->middleware(['auth']);
Route::view('/cabinet/client/password', 'cabinets.client.password')->middleware(['auth']);

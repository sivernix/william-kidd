<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LogoutController;

Route::group(['prefix' => 'cabinet/client'], function () {
    Route::get('/registration', [App\Http\Controllers\Client\ClientRegisterController::class, 'index']);
    Route::post('/registration', [App\Http\Controllers\Client\ClientRegisterController::class, 'stepOne'])->name('reg-one');
    Route::post('/registration/get/dadata-info/{inn}', [App\Http\Controllers\DadataController::class, 'getInfo'])->name('get-dadata');
    Route::get('/registration-next', function () {return view('cabinets.client.registration-next');});
    Route::get('/registration-password', [App\Http\Controllers\Client\ClientVerificationController::class, 'index']);
    Route::post('/registration-password', [App\Http\Controllers\Client\ClientVerificationController::class, 'setup']);
    Route::get('/documents', function () {return view('cabinets.client.documents');})->middleware(['auth:client']);
    Route::get('/contract', function () {return view('cabinets.client.contract');})->middleware(['auth:client']);
    Route::get('/orders', function () {return view('cabinets.client.orders');})->name('client.orders')->middleware(['auth:client']);
    Route::get('/order', function () {return view('cabinets.client.order');})->middleware(['auth:client']);
    Route::get('/order/new', function () {return view('cabinets.client.order-new');})->middleware(['auth:client']);
    Route::get('/order/{id}', function () {return view('cabinets.client.order');})->middleware(['auth:client']);
    Route::get('/profile', [App\Http\Controllers\Client\ClientProfileController::class, 'index'])->middleware(['auth:client']);
    Route::post('/profile', [App\Http\Controllers\Client\ClientProfileController::class, 'setup'])->name('client.profile')->middleware(['auth:client']);
    Route::get('/history', function () {return view('cabinets.client.history');})->middleware(['auth:client']);
    Route::get('/partner-reg', [App\Http\Controllers\Client\ClientContractController::class,'checkStatusContract'])->name('client.partner-reg')->middleware(['auth:client']);
    Route::get('/partner-reg-confirm', 'App\Http\Controllers\Pub\Pages\PartnerRegController@getFormData')->name('partner-reg-confirm')->middleware(['auth:client']);
    Route::post('/partner-reg-confirm', 'App\Http\Controllers\Pub\Pages\PartnerRegController@partnerReg')->middleware(['auth:client']);
    Route::get('/partner-reg-profile',  [App\Http\Controllers\Pub\Pages\PartnerRegController::class,'getFormData'])->name('partner-reg-profile')->middleware(['auth:client']);
    Route::post('/partner-reg-profile', [App\Http\Controllers\Pub\Pages\PartnerRegController::class,'partnerReg'])->middleware(['auth:client']);
    Route::get('/partner-reg-profile-error', function () {return view('cabinets.client.partner-reg-profile-error');})->middleware(['auth:client']);
    Route::get('/partner-reg-doc', 'App\Http\Controllers\Pub\Pages\PartnerRegController@getFormData')->name('partner-reg-doc')->middleware(['auth:client']);
    Route::post('/partner-reg-doc', 'App\Http\Controllers\Pub\Pages\PartnerRegController@partnerReg')->middleware(['auth:client']);
    Route::get('/partner-reg-doc-error', function () {return view('cabinets.client.partner-reg-doc-error');})->middleware(['auth:client']);
    Route::get('/partner-reg-check', [App\Http\Controllers\Pub\Pages\PartnerRegController::class,'getFormData'])->name('partner-reg-check')->middleware(['auth:client']);
    Route::get('/partner-reg-complete', [App\Http\Controllers\Client\ClientContractController::class,'completedByClient'])->name('partner-reg-complete')->middleware(['auth:client']);
    Route::get('/partner-reg-complete-error', function () {return view('cabinets.client.partner-reg-complete-error');})->middleware(['auth:client']);

    Route::get('/login', 'App\Http\Controllers\Client\ClientAuthController@getLogin')->name('login.client');
    Route::post('/login', 'App\Http\Controllers\Client\ClientAuthController@postLogin');
    Route::get('/logout', 'App\Http\Controllers\Client\ClientAuthController@postLogout');
});

